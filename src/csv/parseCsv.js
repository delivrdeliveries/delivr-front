

const breakLines = text => text.split('\n')

const readFileText = file => new Promise((accept, reject) => {
    const fileReader = new FileReader()
    fileReader.onloadend = () => accept(fileReader.result)
    fileReader.onerror = reject

    fileReader.readAsText(file, "UTF-8")
})

const getHeader = lines => lines[0].split(';').map(h => h.replace(/[^0-9a-zA-Z_]/g, ''))

const buildLineObject = (header, line) => {
    const items = line.split(';')
    const lineObject = {}

    var i = 0
    for (const field of header) {
        lineObject[field] = items[i]
        i++
    }

    return lineObject
}

export default async file => {
    const text = await readFileText(file)
    const body = breakLines(text)
    const header = getHeader(body)
    body.shift()

    return body.map(line => buildLineObject(header, line))
}