import requests from '../requests';
import endpoints from '../config/endpoints';

export const getTopics = async (deliveryId) => {
    const resp = await requests.get(`/topics/${deliveryId}`, {}, endpoints.MESSAGES)
    return resp.data
}

export const postTopic = async (deliveryId, topic) => {
    const resp = await requests.post(`/topics/${deliveryId}`, topic, endpoints.MESSAGES)
    return resp.data
}

export const getMessages = async (deliveryId, topicId, skip = 0) => {
    const resp = await requests.get(`/topics/${deliveryId}/${topicId}/messages`, {skip}, endpoints.MESSAGES)
    return resp.data
}