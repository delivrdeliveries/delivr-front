import {
  SET_MESSAGES_TOPICS,
  SET_MESSAGES
} from './constants'

const initialState = {
  messages: [],
  topics: [],
}

const setMessages = (state, action) => ({
  ...state,
  messages: action.payload
})

const setTopics = (state, action) => ({
  ...state,
  topics: action.payload
})

const topicsReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_MESSAGES_TOPICS:
        return setTopics(state, action)
      case SET_MESSAGES:
        return setMessages(state, action)
      default:
        return state
  }
}

export default topicsReducer