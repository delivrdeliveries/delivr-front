import {
    SET_MESSAGES,
    SET_MESSAGES_TOPICS
} from './constants'

import { getMessages, getTopics, postTopic } from './endpoints'

export const setMessages = messages =>({
    type: SET_MESSAGES,
    payload: messages
})

export const setTopics = topics =>({
    type: SET_MESSAGES_TOPICS,
    payload: topics
})

export const loadTopicsAsync = (deliveryLocator, onSuccess) => {
    return dispatch => {
        getTopics(deliveryLocator).then(topics => {
            dispatch(setTopics(topics))
            onSuccess(topics)
        });
    }
}

export const createTopic = (deliveryLocator, topic, onSuccess) => {
    return dispatch => {
        postTopic(deliveryLocator, topic).then(onSuccess)
    }
}


export const loadMessagesAsync = (deliveryLocator, topicId, onSuccess, skip = 0) => {
    return dispatch => {
        getMessages(deliveryLocator, topicId, skip).then(messages => {
            dispatch(setMessages(messages))
            onSuccess(messages)
        });
    }
}