import io from 'socket.io-client'
import endpoints from '../config/endpoints';

export function enterMessageChat (onConnect, onEvent, onDisconnect) {
    return io(endpoints.MESSAGES_SOCKET)
}