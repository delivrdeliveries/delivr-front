import {SET_USER_LOCAL_STORAGE} from './constants'
import User from './user';

const setUserLocalStorage = user => ({
    type: SET_USER_LOCAL_STORAGE,
    payload: user
})


export const loadUserFromLocalStorage = (onSuccess) => {
    return dispatch => {
        const user = getUserFromLocalStorage()
        dispatch(setUserLocalStorage(user))
        if (onSuccess) {
            onSuccess(user)
        }
    }
}

export const getUserFromLocalStorage = () => {
    const storage = window.localStorage
    const userInfos = storage.getItem('user')

    if (userInfos) {
        return User.fromJSON(userInfos)
    }
}

export const saveUserToLocalStorage = (user) => {
    return dispatch => {
        const storage = window.localStorage
        dispatch(setUserLocalStorage(user))
        storage.setItem('user', user.toJSON())
    }
}

export const removeUserFromLocalStorage = () => {
    return dispatch => {
        const storage = window.localStorage
        storage.removeItem('user')
        dispatch(setUserLocalStorage(undefined))
    }
}   