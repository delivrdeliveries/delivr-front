import {SET_USER_LOCAL_STORAGE} from './constants'

const initialState = { 
  userLocalStorage: undefined
}

const setUserLocalStorage = (state, action) => ({
  ...state,
  userLocalStorage: action.payload
})

const userReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_USER_LOCAL_STORAGE:
          return setUserLocalStorage(state, action)
      default:
          return state
  }
}

export default userReducer