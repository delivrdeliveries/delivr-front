export default class User {

    _firebaseId  = undefined
    _companyId = undefined
    _companyType = undefined

    constructor ({firebaseId, companyId, companyType}) {
        this._firebaseId = firebaseId
        this._companyId = companyId
        this._companyType = companyType
    }

    toJSON () {
        return JSON.stringify({
            firebaseId: this._firebaseId,
            companyId: this._companyId,
            companyType: this._companyType
        }) 
    }

    static fromJSON(json) {
        return new User(JSON.parse(json))
    }

    get firebaseId () {
        return this._firebaseId
    }

    get companyId () {
        return this._companyId
    }

    isShippingCompany () {
        return this._companyType === 'ShippingCompany'
    }

    isSupplierCompany () {
        return this._companyType === 'SupplierCompany'
    }

}