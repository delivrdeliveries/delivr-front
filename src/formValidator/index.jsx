
export default class FormValidator {
    _fields = {}

    constructor (fields) {
        fields = fields.map(field => new FieldValidator(field.name, field.validators))

        for (const field of fields) {
            this._fields[field.getName()] = field
        }

        this.isFieldInvalid = this.isFieldInvalid.bind(this)
        this.isFormInvalid = this.isFormInvalid.bind(this)
    }

    isFieldInvalid (name, value) {
        return this._fields[name].isInvalid(value)
    }

    isFormInvalid () {
        for (const fieldName in this._fields) {
            if (this._fields[fieldName]._isLastValidationInvalid()) {
                return true
            }
        }

        return false
    }

}

export class FieldValidator {

    _name = undefined
    _validators = undefined
    _isInvalid = true

    constructor(name, validators, value) {
        this._name = name
        this._validators = validators

        if (value) {
            this.isInvalid(value)
        }
    }

    getName() {
        return this._name
    }

    _isLastValidationInvalid () {
        return this._isInvalid
    }

    isInvalid (value) {
        for (const validator of this._validators) {
            if (validator(value)) {
                this._isInvalid = true
                return true
            }
        }

        this._isInvalid = false
        return false
    }

}

export const isEmpty = (value) => {
    return !value || !String(value).trim().length
}

export const isCPF = (value) => {
    var sum  = 0
    var i = 0
    var rest

    if (isEmpty(value) || value === "00000000000") return true
     
    for (i=1; i<=9; i++) sum = sum + parseInt(value.substring(i-1, i), 10) * (11 - i)
    rest = (sum * 10) % 11
   
    if ((rest === 10) || (rest === 11))  rest = 0
    if (rest !== parseInt(value.substring(9, 10), 10) ) return true
   
    sum = 0
    for (i = 1; i <= 10; i++) sum = sum + parseInt(value.substring(i-1, i), 10) * (12 - i)
    rest = (sum * 10) % 11
   
    if ((rest === 10) || (rest === 11))  rest = 0
    if (rest !== parseInt(value.substring(10, 11), 10) ) return true
    return false
}

export const isEmail = (value) => {
    return isEmpty(value) || !value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b/)
}

export const isCNPJ = (value) => {
    if (isEmpty(value)) return true
    
    value = value.replace(/[^\d]+/g,'')
     
    if (value.length !== 14)
        return true
 
    if (value === "00000000000000" || 
        value === "11111111111111" || 
        value === "22222222222222" || 
        value === "33333333333333" || 
        value === "44444444444444" || 
        value === "55555555555555" || 
        value === "66666666666666" || 
        value === "77777777777777" || 
        value === "88888888888888" || 
        value === "99999999999999")
        return true
         
    var length = value.length - 2
    var numbers = value.substring(0,length)
    var digits = value.substring(length)
    var sum = 0
    var pos = length - 7
    var i
    var results

    for (i = length; i >= 1; i--) {
      sum += numbers.charAt(length - i) * pos--
      if (pos < 2)
            pos = 9
    }
    results = String(sum % 11 < 2 ? 0 : 11 - sum % 11)
    if (results !== digits.charAt(0))
        return true
         
    length = length + 1
    numbers = value.substring(0,length)
    sum = 0
    pos = length - 7
    for (i = length; i >= 1; i--) {
      sum += numbers.charAt(length - i) * pos--
      if (pos < 2)
            pos = 9
    }
    results = String(sum % 11 < 2 ? 0 : 11 - sum % 11)
    if (results !== digits.charAt(1))
          return true
           
    return false
}

export const isCEP = (value) => {
    if (isEmpty(value)) {
        return true
    }

    value = value.replace(/[ˆ\d]/g, '')
    return value.length === 8
}