import requests from '../requests'

export const getDeliveryProfiles = companyId => {
    return requests.get(`/shipping_companies/${companyId}/delivery_profiles`).then(
        req => req.data
    )
}

export const postDeliveryProfile = (companyId, profile) => {
    return requests.post(`/shipping_companies/${companyId}/delivery_profiles`, profile).then(
        req => req.data
    )
}

export const putDeliveryProfile = (companyId, profileId, profile) => {
    return requests.put(`/shipping_companies/${companyId}/delivery_profiles/${profileId}`, profile).then(
        req => req.data
    )
}

export const getDeliveryProfile = (companyId, profileId) => {
    return requests.get(`/shipping_companies/${companyId}/delivery_profiles/${profileId}`).then(
        req => req.data
    )
}