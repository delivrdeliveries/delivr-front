import {SET_DELIVERY_PROFILE, SET_DELIVERY_PROFILES} from './constants'
import { getDeliveryProfile, getDeliveryProfiles, postDeliveryProfile, putDeliveryProfile } from './endpoints'

export const setDeliveryProfile = profile =>({
    type: SET_DELIVERY_PROFILE,
    payload: profile
})

export const setDeliveryProfiles = profiles =>({
    type: SET_DELIVERY_PROFILES,
    payload: profiles
})

export const loadDeliveryProfilesAsync = (companyId, onSuccess) => {
    return dispatch => {
        getDeliveryProfiles(companyId).then(profiles => {
                dispatch(setDeliveryProfiles(profiles))
                if (onSuccess) {
                    onSuccess(profiles)
                }
            }
        )
    }
}

export const loadDeliveryProfileAsync = (companyId, profileId, onSuccess) => {
    return dispatch => {
        getDeliveryProfile(companyId, profileId).then(profile => {
                dispatch(setDeliveryProfile(profile))
                if (onSuccess) {
                    onSuccess(profile)
                }
            }
        )
    }
}

export const createDeliveryProfileAsync = (companyId, profile, onSuccess, onError) => {
    return dispatch => {
        postDeliveryProfile(companyId, profile).then(onSuccess).catch(onError)
    }
}

export const updateDeliveryProfileAsync = (companyId, profileId, profile, onSuccess, onError) => {
    return dispatch => {
        putDeliveryProfile(companyId, profileId, profile).then(onSuccess).catch(onError)
    }
}