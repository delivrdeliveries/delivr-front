import { SET_DELIVERY_PROFILES, SET_DELIVERY_PROFILE } from './constants'

const initialState = {
  deliveryProfiles: [],
  deliveryProfile: {}
}

const setDeliveryProfiles = (state, action) => ({
  ...state,
  deliveryProfiles: action.payload
})

const setDeliveryProfile = (state, action) => ({
  ...state,
  deliveryProfile: action.payload
})

const deliveryProfileReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_DELIVERY_PROFILE:
        return setDeliveryProfile(state, action)
      case SET_DELIVERY_PROFILES:
        return setDeliveryProfiles(state, action)
      default:
        return state
  }
}

export default deliveryProfileReducer