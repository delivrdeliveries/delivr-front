export default ({street, number, complement, city}) => {
    let strAddr = `${street}, ${number}`

    if (complement)
        strAddr += ` ${complement}`

    strAddr += `. ${city.name}, ${city.state} - ${city.country}`

    return strAddr
}