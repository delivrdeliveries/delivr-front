const publicRoutes = [
    '/',
    '/fornecedora/criar',
    '/transportadora/criar',
    '/publico/entrega/:locator',
    '/publico/entrega'
]

export const isPublicRoute = route => {
    return publicRoutes.filter(publicRoute => {

        if (publicRoute.includes(':')) {

            const param = publicRoute.match('.*(:[a-zA-Z]*)')[1]
            const regex = new RegExp(publicRoute.replace(param, '.*'))
            return route.match(regex) !== null

        } else {

            return publicRoute === route
            
        }

    }).length > 0
}