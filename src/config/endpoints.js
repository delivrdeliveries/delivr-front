const endpoints = {
    DELIVERIES: 'http://localhost:7000',
    MESSAGES: 'http://localhost:9000',
    MESSAGES_SOCKET: 'http://localhost:9090',
    REPORTS: 'http://localhost:9898',
    PAYMENTS: 'http://localhost:3100',
}

export default endpoints