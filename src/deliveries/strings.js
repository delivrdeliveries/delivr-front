import states from './status'
import events from './events'

export const getStateLabel = state => {
    const labels = {}
    labels[states.DELIVERED] = 'Entregue'
    labels[states.FINISHED] = 'Entrega Confirmada'
    labels[states.ON_CARRIAGE] = 'Em Transporte'
    labels[states.IN_OCCURRENCE] = 'Em Ocorrência'
    labels[states.AWAITTING_SHIPMENT] = 'Disponível para Retirada'
    labels[states.CANCELED] = 'Cancelada'
    return labels[state]
}

export const getEventLabel = event => {
    const labels = {}
    labels[events.CANCELED] = 'Cancelada'
    labels[events.CREATED] = 'Criada'
    labels[events.DELIVERED] = 'Entregue'
    labels[events.FINISHED] = 'Finalizada'
    labels[events.LOCATION_UPDATED] = 'Localização Atualizada'
    labels[events.OCCURRENCE] = 'Em Ocorrência'
    labels[events.SHIPPED] = 'Coletado'
    return labels[event]
}

export const generateLocator = id => parseInt(id, 10).toString(16).toUpperCase()