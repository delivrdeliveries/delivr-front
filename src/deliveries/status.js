const status = {
    DELIVERED: 'delivered',
    FINISHED:   'finished',
    CANCELED:   'canceled',
    ON_CARRIAGE: 'on_carriage',
    IN_OCCURRENCE: 'in_occurrence',
    AWAITTING_SHIPMENT: 'awaiting_shipment'
}

export const allStates = [
    status.DELIVERED,
    status.FINISHED,
    status.IN_OCCURRENCE,
    status.ON_CARRIAGE,
    status.AWAITTING_SHIPMENT,
    status.CANCELED
]

export default status