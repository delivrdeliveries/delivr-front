import {  SET_DELIVERY } from './constants'

const initialState = {
  deliveries: [],
}

const setDelivery = (state, action) => ({
  ...state,
  delivery: action.payload
})

const delirveryReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_DELIVERY:
        return setDelivery(state, action)
      default:
        return state
  }
}

export default delirveryReducer