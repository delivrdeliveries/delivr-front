const events = {
    CREATED: 'created',
    SHIPPED: 'shipped',
    LOCATION_UPDATED: 'location_updated',
    OCCURRENCE: 'occurrence',
    DELIVERED: 'delivered',
    FINISHED: 'finished',
    CANCELED: 'canceled'
}

export default events