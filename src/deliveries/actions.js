import {SET_DELIVERY} from './constants'
import { getDelivery, postDelivery, postEvent, postRate } from './endpoints'
import { getStateLabel } from './strings'
import events from './events'

export const setDelivery = delivery =>({
    type: SET_DELIVERY,
    payload: delivery
})


export const postDeliveryAsync = (delivery, onSuccess, onError) => {
    return dispatch => {
         postDelivery(delivery).then(delivery => {
             delivery.statusLabel = getStateLabel(delivery.status)
             
             if (onSuccess) {
                 onSuccess(delivery)
             }

             dispatch(setDelivery(delivery))
         }).catch(errors => {
            if (onError) {
                onError(errors)
            }
         })
     }
}

export const postRateAsync = (delivery, params, onSuccess) => {
    return dispatch => {
         postRate(delivery, params).then(() => {
            dispatch(loadDeliveryAsync(delivery.locator))
            if (onSuccess) {
                onSuccess()
            }
         })
     }
}

const postEventAsync = (delivery, params, onSuccess) => {
    return dispatch => {
         postEvent(delivery, params).then(() => {
            dispatch(loadDeliveryAsync(delivery.locator))
            if (onSuccess) {
                onSuccess()
            }
         })
     }
}

export const postDeliveryShippedAsync = (delivery, location, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        event: events.SHIPPED
    }, onSuccess)
}

export const postDeliveryContinuedAsync = (delivery, location, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        description: 'Entrega colocada novamente para rodar',
        event: events.SHIPPED
    }, onSuccess)
}

export const postUpdateLocationAsync = (delivery, location, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        description: 'Localição Atualizada'
    }, onSuccess)
}

export const postOnOccurrenceAsync = (delivery, location, description, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        description,
        event: events.OCCURRENCE
    }, onSuccess)
}

export const postCanceledAsync = (delivery, location, description, onSuccess) => {
    console.info('post canceled async fired')
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        description,
        event: events.CANCELED
    }, onSuccess)
}

export const postDeliverDeliveryAsync = (delivery, location, description, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        description,
        event: events.DELIVERED
    }, onSuccess)
}

export const postFinishDeliveryAsync = (delivery, location, onSuccess) => {
    return postEventAsync(delivery, {
        latitude: location.latitude,
        longitude: location.longitude,
        event: events.FINISHED
    }, onSuccess)
}

export const loadDeliveryAsync = (locator, onSuccess, onError) => {
    return dispatch => {
        dispatch(setDelivery(undefined))

        getDelivery(locator).then(delivery => {
            if (delivery)
                delivery.statusLabel = getStateLabel(delivery.status)

            dispatch(setDelivery(delivery))
            return delivery
        }).then(onSuccess).catch(onError)
    }
}

