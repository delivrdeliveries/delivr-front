import requests from '../requests'

export const postDelivery = delivery => {
    return requests.post('/deliveries/', delivery)
        .then(resp => resp.data)
}

export const postRate = (delivery, rate) => {
    return requests.post('/deliveries/' + delivery.locator + '/rate', rate)
        .then(resp => resp.data)
}


export const postEvent = (delivery, params) => {
    return requests.post('/deliveries/' + delivery.locator + '/events', params)
        .then(resp => resp.data)
}

export const getDelivery = (locator) => {
    return requests.get('/deliveries/' + locator).then(
        ({data: delivery}) => {
            delivery.events = delivery.events.sort(e => e.created_at).reverse()
            return delivery
        }
    )
}