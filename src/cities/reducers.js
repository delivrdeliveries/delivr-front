import { SET_CITIES } from './constants'

const initialState = {
  cities: []
}

const setCities = (state, action) => ({
  ...state,
  cities: action.payload
})

const citiesReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_CITIES:
        return setCities(state, action)
      default:
        return state
  }
}

export default citiesReducer