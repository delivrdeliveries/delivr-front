import requests from '../requests'

export const getCities = () => {
    return requests.get('/cities').then(
        req => req.data
    )
}