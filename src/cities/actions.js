import {SET_CITIES} from './constants'
import { getCities } from './endpoints'

export const setCities = cities =>({
    type: SET_CITIES,
    payload: cities
})

export const loadCitiesAsync = (onSuccess) => {
    return dispatch => {
        getCities().then(
            cities => {
                dispatch(setCities(cities))
                if (onSuccess) {
                    onSuccess(cities)
                }
            }
        )
    }
}