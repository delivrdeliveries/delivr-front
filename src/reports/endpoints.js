import requests from "../requests";
import endpoints from "../config/endpoints";

export function getDeliveriesPerMonthReport(supplierCompany, year) {
    return requests.get(`/${supplierCompany}/deliveries/per-month`, { year }, endpoints.REPORTS)
}

export function getDeliveriesPerShippingCompany(supplierCompany, startAt, endAt) {
    return requests.get(`/${supplierCompany}/deliveries/per-shipping-company`, { startAt, endAt }, endpoints.REPORTS)
}

export function downloadCsvDeliveriesPerShippingCompany(supplierCompany, startAt, endAt) {
    return requests.download(`/${supplierCompany}/deliveries/per-shipping-company.csv`, { startAt, endAt }, 'per-shipping-company.csv', endpoints.REPORTS)
}