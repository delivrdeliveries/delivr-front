import moment from 'moment'

const months = {
    1: 'Janeiro',
    2: 'Fevereiro',
    3: 'Março',
    4: 'Abril',
    5: 'Maio',
    6: 'Junho',
    7: 'Julho',
    8: 'Agosto',
    9: 'Setembro',
    10: 'Outubro',
    11: 'Novembro',
    12: 'Dezembro'
}

export const formatDate = date => {
    if (!date) {
        return
    } else if (date instanceof moment) {
        return date.format('DD/MM/YYYY')
    } else {
        return formatDate(moment(date))
    }
}

export const formatDateTime = dateTime => {
    if (!dateTime) {
        return
    } else if (dateTime instanceof moment) {
        return dateTime.format('DD/MM/YYYY HH:mm:ss')
    } else {
        return formatDateTime(moment(dateTime))
    }
}

export const formatCompetency = competency => {
    competency = parseInt(competency, 10)
    const year = Math.floor(competency / 100)
    const month = competency % 100
    return `${months[month]}/${year}`
}