import { SET_AGENTS, SET_AGENT, SET_SHIPPING_COMPANIES, SET_SHIPPING_COMPANY_DELIVERIES } from './constants'

import { getAgents, getAgent, getShippingCompaniesSuggestions, postShippingCompany, putShippingCompany, getShippingCompanyDeliveries, postAgent, putAgent } from './endpoints'
import { getStateLabel } from '../deliveries/strings';

export const setAgent = agent => ({
    type: SET_AGENT,
    payload: agent
})

export const setAgents = agents => ({
    type: SET_AGENTS,
    payload: agents
})

export const setShippingCompanies = companies => ({
    type: SET_SHIPPING_COMPANIES,
    payload: companies
})

export const setShippingCompanyDeliveries = deliveries => ({
    type: SET_SHIPPING_COMPANY_DELIVERIES,
    payload: deliveries
})

export const postShippingCompanyAsync = (company, onSuccess, onError) => {
    return dispatch => {
        postShippingCompany(company).then(onSuccess).catch(onError)
    }
}

export const putShippingCompanyAsync = (id, company, onSuccess, onError) => {
    return dispatch => {
        putShippingCompany(id, company).then(onSuccess).catch(onError)
    }
}

export const loadAgents = (shippingCompanyId, onSuccess) => {
    return dispatch => {
        getAgents(shippingCompanyId).then(agents => {
            dispatch(setAgents(agents))
        }).then(onSuccess)
    }
}

export const loadAgent = (shippingCompanyId, agentId, onSuccess) => {
    return dispatch => {
        getAgent(shippingCompanyId, agentId).then(agent => {
            dispatch(setAgent(agent))
            return agent
        }).then(onSuccess)
    }
}

export const createAgent = (shippingCompanyId, agent, onSuccess) => {
    return dispatch => {
        postAgent(shippingCompanyId, agent).then(onSuccess)
    }
}

export const editAgent = (shippingCompanyId, agentId, agent, onSuccess) => {
    return dispatch => {
        putAgent(shippingCompanyId, agentId, agent).then(onSuccess)
    }
}

export const loadShippingCompaniesSuggestionsAsync = (delivery, onSuccess, onError) => {
    return dispatch => {
        getShippingCompaniesSuggestions(delivery)
            .then(companies => dispatch(setShippingCompanies(companies.sort(c => parseFloat(c.estimated_price)))))
            .then(onSuccess)
            .catch(onError)
    }
}

export const loadShippingCompanyDeliveries = (companyId, onSuccess, { status, locator, page }) => {
    return dispatch => {
        getShippingCompanyDeliveries(companyId, { status, locator, page }).then(deliveries => {

            dispatch(setShippingCompanyDeliveries(deliveries.map(delivery => {
                delivery.statusLabel = getStateLabel(delivery.status)
                return delivery
            })))

            return deliveries
        }).then(onSuccess)
    }
}