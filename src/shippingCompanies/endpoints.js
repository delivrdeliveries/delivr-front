import requests from '../requests'

export const getShippingCompaniesSuggestions = (delivery) => {
    return requests.get('/shipping_companies/suggestions', delivery).then(resp => resp.data)
}

export const postShippingCompany = async company => {
    const resp = await requests.post('/shipping_companies', company)
    return resp.data
}

export const putShippingCompany = async (id, company) => {
    const resp = await requests.put(`/shipping_companies/${id}`, company)
    return resp.data
}

export const getShippingCompanyDeliveries = (id, params) => {
    return requests.get(`/shipping_companies/${id}/deliveries`, params).then(
        resp => resp.data
    )
}

export const getAgents = companyId => {
    return requests.get(`/shipping_companies/${companyId}/agents`).then(
        resp => resp.data
    )
}

export const getAgent = (companyId, agentId) => {
    return requests.get(`/shipping_companies/${companyId}/agents/${agentId}`).then(
        resp => resp.data
    )
}

export const postAgent = (companyId, agent) => {
    return requests.post(`/shipping_companies/${companyId}/agents`, agent).then(
        resp => resp.data
    )
}

export const putAgent = (companyId, agentId, agent) => {
    return requests.put(`/shipping_companies/${companyId}/agents/${agentId}`, agent).then(
        resp => resp.data
    )
}