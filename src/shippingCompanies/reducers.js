import { SET_AGENT, SET_AGENTS, SET_SHIPPING_COMPANIES, SET_SHIPPING_COMPANY_DELIVERIES } from './constants'

const initialState = {
  agents: [],
  shippingCompanies: [],
  agent: {},
  loadingAgents: false,
  deliveries: [],
}

const setAgent = (state, action) => ({
  ...state,
  agent: action.payload
})

const setAgents = (state, action) => ({
  ...state,
  agents: action.payload
})

const setShippingCompanies = (state, action) => ({
  ...state,
  shippingCompanies: action.payload
})

const setDeliveries = (state, action) => ({
  ...state,
  deliveries: action.payload
})

const topicsReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_AGENT:
        return setAgent(state, action)
      case SET_AGENTS:
        return setAgents(state, action)
      case SET_SHIPPING_COMPANIES:
        return setShippingCompanies(state, action)
      case SET_SHIPPING_COMPANY_DELIVERIES:
      return setDeliveries(state, action)
      default:
        return state
  }
}

export default topicsReducer