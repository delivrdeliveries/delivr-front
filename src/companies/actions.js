import { SET_COMPANY } from './constants'
import { getCompanyByUser } from './endpoints'

export const setCompany = company =>({
    type: SET_COMPANY,
    payload: company
})

export const loadCompanyByUserAsync = (user, onSuccess) => {
    return dispatch => {
        getCompanyByUser(user).then(
            company => {
                dispatch(setCompany(company))
                if (onSuccess) {
                    onSuccess(company)
                }
            }
        )
    }
}