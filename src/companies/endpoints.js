import requests from '../requests'

export const getCompanyByUser = (userId) => {
    return requests.get('/companies/' + userId).then(
        req => req.data
    )
}