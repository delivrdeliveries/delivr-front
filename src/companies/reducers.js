import { SET_COMPANY } from './constants'

const initialState = {
  company: {}
}

const setCompany = (state, action) => ({
  ...state,
  company: action.payload
})

const companiesReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_COMPANY:
        return setCompany(state, action)
      default:
        return state
  }
}

export default companiesReducer