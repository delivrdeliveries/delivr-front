import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducers from './reducers'
import { routerReducer, routerMiddleware } from 'react-router-redux'

import { reactReduxFirebase } from 'react-redux-firebase'
import firebase from 'firebase'
import firebaseConfig from '../config/firebaseConfig'

import createHistory from 'history/createBrowserHistory'

export const history = createHistory()
const middleware = routerMiddleware(history)

firebase.initializeApp(firebaseConfig)

const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, {
    userProfile: 'users',
    enableLogging: false,
    logErrors: false,
  })
)(createStore)

const store = createStoreWithFirebase(
  combineReducers({
    ...reducers,
    routerReducer
  }), 
  composeWithDevTools(applyMiddleware(thunk)),
  applyMiddleware(middleware)
)

export default store