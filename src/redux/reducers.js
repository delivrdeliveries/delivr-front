import { firebaseReducer } from 'react-redux-firebase'

import userReducer from '../user/reducers'
import messageReducer from '../messages/reducers'
import deliveryReducer from '../deliveries/reducers'
import shippingCompanyReducer from '../shippingCompanies/reducers'
import supplierCompanyReducer from '../supplierCompanies/reducers'
import cityReducer from '../cities/reducers'
import productTypeReducer from '../productTypes/reducers'
import deliveryRestrictionReducer from '../deliveryRestrictions/reducers'
import deliveryProfileReducer from '../deliveryProfiles/reducers'
import companyReducer from '../companies/reducers'

export default { 
    userReducer, 
    messageReducer,
    deliveryReducer, 
    companyReducer,
    shippingCompanyReducer,
    supplierCompanyReducer,
    cityReducer,
    firebaseReducer,
    productTypeReducer,
    deliveryRestrictionReducer,
    deliveryProfileReducer
}