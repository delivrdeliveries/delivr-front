import requests from '../requests'

export const getDeliveryRestrictions = () => {
    return requests.get('/delivery_restrictions').then(
        req => req.data
    )
}