import {SET_DELIVERY_RESTRICTIONS} from './constants'
import { getDeliveryRestrictions } from './endpoints'

export const setDeliveryRestrictions = deliveryRestrictions =>({
    type: SET_DELIVERY_RESTRICTIONS,
    payload: deliveryRestrictions
})

export const loadDeliveryRestrictionsAsync = (onSuccess) => {
    return dispatch => {
        getDeliveryRestrictions().then(deliveryRestrictions => {
            dispatch(setDeliveryRestrictions(deliveryRestrictions))
            if (onSuccess) {
                onSuccess(deliveryRestrictions)
            }
        })
    }
}