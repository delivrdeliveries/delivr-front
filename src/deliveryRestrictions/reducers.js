import { SET_DELIVERY_RESTRICTIONS } from './constants'

const initialState = {
  deliveryRestrictions: []
}

const setDeliveryRestrictions = (state, action) => ({
  ...state,
  deliveryRestrictions: action.payload
})

const deliveryRestrictionsReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_DELIVERY_RESTRICTIONS:
        return setDeliveryRestrictions(state, action)
      default:
        return state
  }
}

export default deliveryRestrictionsReducer