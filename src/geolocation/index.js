import Axios from "axios"

export const getGeolocation = () => new Promise((resolve, reject) => {

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            location => resolve(location.coords),
            ()=> resolve({latitude: -25.465487, longitude: -49.2181885}),
            {
                maximumAge: 60 * 60 * 1000,
                timeout: 2 * 1000
            }
        )
    } else {
        reject(new Error('Browser não suporta geolocation'))
    }
})

export const getAddressByCep = async cep => {
    const {data} = await Axios.get(`https://viacep.com.br/ws/${cep}/json/unicode/`)
    return data
}