import requests from "../requests";
import endpoints from "../config/endpoints";

async function doGetInPayments(path, params = {}) {
    const { data } = await requests.get(path, params, endpoints.PAYMENTS)
    return data
}

export async function getInvoices(supplierCompanyId) {
    return doGetInPayments(`/invoices/${supplierCompanyId}`)
}

export async function getInvoiceDeliveries(supplierCompanyId, competency) {
    return doGetInPayments(`/invoices/${supplierCompanyId}/${competency}/deliveries`)
}

export async function getReceipts(shippingCompanyId) {
    return doGetInPayments(`/receipts/${shippingCompanyId}`)
}

export async function getReceiptDeliveries(shippingCompanyId, competency) {
    return doGetInPayments(`/receipts/${shippingCompanyId}/${competency}/deliveries`)
}