import axios from 'axios'
import endpoints from '../config/endpoints'
import fileDownload from 'js-file-download'

class requests {

    handleErrors (error) {
        if (error.response && error.response.status === 400) {
            
            const errors = error.response.data
            const messages = []
            for (const key in errors) {
                const fieldName = key.split('_').map(s =>  s.charAt(0).toUpperCase() + s.slice(1)).join(' ')
                
                if (errors[key] instanceof Array) {
                    messages.push(`${fieldName} ${errors[key].join(', ')}`)
                } else {
                    messages.push(`${fieldName} ${errors[key]}`)
                }
            }

            alert(messages.join('\n\n\n'))

            throw error.response.data
        } else {
            throw error
        }
    }

    get (path, params, domain = endpoints.DELIVERIES) {
        return axios.get(domain + path, {params}).catch(this.handleErrors)
    }

    post (path, body, domain = endpoints.DELIVERIES) {
        return axios.post(domain + path, body).catch(this.handleErrors)
    }

    put (path, body, domain = endpoints.DELIVERIES) {
        return axios.put(domain + path, body).catch(this.handleErrors)
    }

    delete (path, domain = endpoints.DELIVERIES) {
        return axios.delete(domain + path).catch(this.handleErrors)
    }

    async download (path, params, filename, domain = endpoints.DELIVERIES) {
        const resp = await this.get(path, params, domain)
        fileDownload(resp.data, filename)
    }

}

export default  new requests()