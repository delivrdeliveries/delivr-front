import formatter from 'currency-formatter'

export const formatCurrency = currency => {
    currency = parseFloat(currency)
    return formatter.format(currency, {locale: 'pt-BR'})
}

export const formatPaymentsCurrency = currency => formatCurrency(currency / 100)

export default formatCurrency