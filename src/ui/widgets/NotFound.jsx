import React, {Component} from 'react'

import TitleBack from './TitleBack'

import {
    Title,
    Paragraph
} from '../components/Texts'


export default class NotFound extends Component {

    goBack () {
        window.history.back()
    }

    render () {
        return (
            <div>
                <Title> Não encontramos :( <TitleBack backTo={this.props.backTo} /> </Title>
                
                <Paragraph>
                    {this.props.reason || 'Desculpe mas não pudemos localizar o que você procura'}
                </Paragraph>
            </div>
        )
    }

}