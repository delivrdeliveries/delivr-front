import React, {Component} from 'react'
import { WhiteCard } from '../../components/Containers';
import { Row, Col } from 'react-grid-system';
import { FieldContainer, Label, TextField, MaskField } from '../../components/Forms';
import FormValidator, { isEmpty, isCPF, isEmail } from '../../../formValidator';

export default class UserForm extends Component {

    state = {
        user: {
            name: undefined,
            document: undefined,
            email: undefined,
            password: undefined,
            passwordConfirmation: null
        }
    }

    constructor (props) {
        super(props)

        this.userUpdated = this.userUpdated.bind(this)
        this.mustMatchPassword = this.differentPasswords.bind(this)

        this.validator = new FormValidator([
            {name: 'name', validators: [ isEmpty ]},
            {name: 'document', validators: [ isCPF ]},
            {name: 'email', validators: [ isEmail ]},
            {name: 'password', validators: [ isEmpty ]},
        ])
    }

    userUpdated (event) {
        const { name, value } = event.target
        const { user } = this.state
        user[name] = value
        this.setState({user: this.formatUser(user)})
        this.propagateUser(user)
    }

    propagateUser(user) {
        if (this.props.onUpdateUser) {
            this.props.onUpdateUser(user, this.isFormValid())
        }
    }

    isFormValid() {
        return !this.validator.isFormInvalid() && !this.differentPasswords()
    }

    formatUser (user) {
        if (user.document) {
            user.document = user.document.replace(/[^0-9]/g, '')
        }
        return user
    }

    differentPasswords() {
        return this.state.user.password !== this.state.user.passwordConfirmation
    }

    render () {
        const {user} = this.state
        const {isFieldInvalid} = this.validator
        return (
            <WhiteCard>
                <Row>
                    <Col>
                        <FieldContainer>
                            <Label htmlFor="name">Nome</Label>
                            <TextField id="name"
                                        name="name" 
                                        block 
                                        onChange={this.userUpdated}
                                        invalid={isFieldInvalid('name', user.name)} />
                        </FieldContainer>
                    </Col>

                    <Col>
                        <FieldContainer>
                            <Label htmlFor="document">CPF</Label>
                            <MaskField mask={[/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'-',/\d/,/\d/,]} 
                                id="document" 
                                name="document" 
                                block 
                                onChange={this.userUpdated}
                                invalid={isFieldInvalid('document', user.document)} />
                        </FieldContainer>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <FieldContainer>
                            <Label htmlFor="email">E-mail</Label>
                            <TextField id="email" 
                                    name="email"
                                    type="email "
                                    block
                                    onChange={this.userUpdated} 
                                    invalid={isFieldInvalid('email', user.email)} />
                        </FieldContainer>
                    </Col>
                </Row>

                    <Row>
                    <Col>
                        <FieldContainer>
                            <Label htmlFor="password">Senha</Label>
                            <TextField id="password" 
                                        name="password" 
                                        type="password" 
                                        block 
                                        onChange={this.userUpdated} 
                                        invalid={isFieldInvalid('password', user.password)} />
                        </FieldContainer>

                            <FieldContainer>
                            <Label htmlFor="passwordConfirmation">Confirme sua Senha</Label>
                            <TextField id="passwordConfirmation" invalid={this.differentPasswords()} onChange={this.userUpdated} name="passwordConfirmation" type="password" block />
                        </FieldContainer>
                    </Col>
                </Row>
            </WhiteCard>
        )
    }

}