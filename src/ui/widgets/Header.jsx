import React, {Component} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'
import SettingsIcon from 'react-icons/lib/md/settings'
import ExitIcon from 'react-icons/lib/md/exit-to-app'

import {
    HeaderContainer,
    List,
    ListItem,
    StyledNavLink,
    CircleName,
    CircleContainer,
    FloatingMenu,
    FloatingMenuItem,
    FloatingPointer,
    FloatingNavLink,
    Isotype,
    Logo
} from './Header.css'
import { withFirebase } from 'react-redux-firebase';
import { removeUserFromLocalStorage, loadUserFromLocalStorage } from '../../user/actions';
import Loader from './Loader';

const iconStyle = {
    position: 'relative',
    bottom: '2px',
    fontSize: '1em'
}

class Header extends Component {

    state = {
        user: undefined,
        loading: true
    }

    constructor (props) {
        super(props)

        this.logout = this.logout.bind(this)
    }

    componentDidMount () {
      const firebase = this.props.firebase

      this.props.loadUserFromLocalStorage()
      firebase.auth().onAuthStateChanged(
        user => {
            this.setState({ user, loading: false })
        },
        () => this.setState({ user: null, loading: false })
      )
    }

    async logout () {
      const { firebase }  = this.props
      await firebase.logout()
      this.props.removeUserFromLocalStorage()
    }

    render () {
        const {user} = this.state
        const {userLocalStorage} = this.props

        if (user) {
            return (
                <HeaderContainer>
                    
                    <NavLink to="/home"><Isotype /></NavLink>
    
                    <List>
                        {(() => {
                            if (userLocalStorage) {
                                if (userLocalStorage.isShippingCompany()) {
                                    return <ListItem><StyledNavLink to="/transportadora/entregas">Entregas</StyledNavLink></ListItem>       
                                } else {
                                    return [
                                    <ListItem key='0'><StyledNavLink to="/fornecedora/entregas">Entregas</StyledNavLink></ListItem>,
                                    <ListItem key='1'><StyledNavLink to="/relatorios">Relatórios</StyledNavLink></ListItem>
                                    ]
                                }
                            }
                        })()}
                    </List>
    
                    <CircleContainer>
                        <CircleName>{user.email.charAt(0).toUpperCase()}</CircleName>
    
                        <FloatingPointer/>
                        <FloatingMenu>
    
                            <FloatingMenuItem>
                                <FloatingNavLink to="/configuracoes">
                                    <SettingsIcon style={iconStyle}/> Configurações
                                </FloatingNavLink>
                            </FloatingMenuItem>
    
                            <FloatingMenuItem onClick={this.logout}>
                                <FloatingNavLink to="/home">
                                    <ExitIcon style={iconStyle} /> Sair
                                </FloatingNavLink>
                            </FloatingMenuItem>
    
                        </FloatingMenu>
                    </CircleContainer>
                </HeaderContainer>
            )
        } else {
            return (
                <HeaderContainer>
                    
                    <NavLink to="/"><Logo /></NavLink>
                
                    <List>
                        <ListItem><StyledNavLink to="/publico/entrega/">Localizar Entrega</StyledNavLink></ListItem>       
                    </List>

                    <Loader loading={this.state.loading} />
                </HeaderContainer>
            )
        }
            
    }
}

const mapStateToProps = state => ({
    userLocalStorage: state.userReducer.userLocalStorage
})

const mapDispatchToProps = dispatch => ({
    removeUserFromLocalStorage: () => dispatch(removeUserFromLocalStorage()),
    loadUserFromLocalStorage: () => dispatch(loadUserFromLocalStorage())
})
export default withFirebase(connect(mapStateToProps, mapDispatchToProps)(Header))