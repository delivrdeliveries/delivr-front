import React, {Component} from 'react'

import DeliveryIcon from 'react-icons/lib/fa/truck'
import ShippingCompanyIcon from 'react-icons/lib/fa/archive'

import { Menu, MenuItem } from '../../components/SideMenu'

export default class ReportsMenu extends Component {

    render() {
        const {active} = this.props
        return (
            <Menu>
                <MenuItem to="/relatorios/entregas-por-mes" selected={active === 'DeliveryReport'} >
                    <DeliveryIcon /> Entregas por mês
                </MenuItem>

                <MenuItem to="/relatorios/entregas-por-transportadoras" selected={active === 'ShippingCompanyReport'}>
                    <ShippingCompanyIcon /> Entregas por transportadoras
                </MenuItem>
            </Menu>
        )
    }

}