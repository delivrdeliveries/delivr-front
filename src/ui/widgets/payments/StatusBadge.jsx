import React from 'react'
import styled from 'styled-components'

const Badge = styled.span`
    font-size: 0.7em;
    border-radius: 10px;
    padding: 5px 10px;
    font-weight: 600;
`

const NotPaidBadge = Badge.extend`
    background: ${p => p.theme.warningOpaque }
`

const PaidBadge = Badge.extend`
    background: ${p => p.theme.mainLight }
`
const ProblemBadge = Badge.extend`
    background: ${p => p.theme.dangerOpaque }
`

const badges = {
    'open': <NotPaidBadge>Aberta</NotPaidBadge>,
    'awaiting_payment': <PaidBadge>Disponível</PaidBadge>,
    'paid': <PaidBadge>Pago</PaidBadge>,
    'canceled': <ProblemBadge>Cancelado</ProblemBadge>,
    'refunded': <ProblemBadge>Reembolsado</ProblemBadge>
}

export default (props) => {
    return badges[props.status]
}