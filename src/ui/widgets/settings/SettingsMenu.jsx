import React, { Component } from 'react'
import { connect } from 'react-redux'

import PersonalInfoIcon from 'react-icons/lib/md/account-circle'
import UpdatePasswordIcon from 'react-icons/lib/md/edit'
import CompanyInfoIcon from 'react-icons/lib/fa/info'
import SupplierAddressesIcon from 'react-icons/lib/fa/map-marker'
import BillIcon from 'react-icons/lib/fa/bars'

import { Menu, MenuItem } from '../../components/SideMenu'

class SettingsMenu extends Component {

    render() {
        const { active } = this.props
        const { user } = this.props

        return (
            <Menu>
                <MenuItem to="/configuracoes/informacoes-pessoais" selected={active === 'PersonalInfo'}>
                    <PersonalInfoIcon /> Informações Pessoais
                </MenuItem>

                <MenuItem to="/configuracoes/alterar-senha" selected={active === 'UpdatePassword'}>
                    <UpdatePasswordIcon /> Alterar Senha
                </MenuItem>

                <MenuItem to="/configuracoes/informacoes-empresa" selected={active === 'CompanyInfo'}>
                    <CompanyInfoIcon /> Informações da Empresa
                </MenuItem>

                {(() => {
                    if (user && user.isShippingCompany()) {
                        return (
                            <MenuItem key='profile' to="/configuracoes/perfis-de-entrega" selected={active === 'DeliveryProfiles'}>
                                <SupplierAddressesIcon /> Perfis de Entrega
                            </MenuItem> ,
                            <MenuItem key='receipt' to="/configuracoes/pagamentos" selected={active === 'Receipts'}>
                                <BillIcon /> Pagamentos
                        </MenuItem>
                        )
                    }
                })()}

                {(() => {
                    if (user && user.isSupplierCompany()) {
                        return [
                            <MenuItem key='address' to="/configuracoes/locais-retirada" selected={active === 'SupplierAddresses'}>
                                <SupplierAddressesIcon /> Locais de Retirada
                            </MenuItem>,
                            <MenuItem key='invoice' to="/configuracoes/faturas" selected={active === 'Invoices'}>
                                <BillIcon /> Faturas
                            </MenuItem>
                        ]
                    }
                })()}
            </Menu>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage,
})

const mapDispatchToProps = dispatch => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(SettingsMenu)