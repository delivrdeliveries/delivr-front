import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Col } from 'react-grid-system'
import { ButtonPrimary } from '../../../components/Buttons'

import FinishedIcon from 'react-icons/lib/fa/check-circle'

import { postFinishDeliveryAsync } from '../../../../deliveries/actions'
import { getGeolocation } from '../../../../geolocation';
import Loader from '../../Loader';
import { getUserFromLocalStorage } from '../../../../user/actions';

class DeliveredAction extends Component {

    state = {
        loading: false
    }

    constructor(props) {
        super(props)

        this.finishDelivery = this.finishDelivery.bind(this)
    }

    finishDelivery() {
        this.setState({ loading: true })
        getGeolocation()
            .then(coords => this.props.postFinishDelivery(this.props.delivery, coords, () => this.setState({ loading: false })))
            .catch(error => {
                window.alert(error.message)
                this.setState({ loading: false })
            })
    }

    render() {
        const user = getUserFromLocalStorage()
        if (user.isSupplierCompany()) {
            return (
                <Col md={4} offset={{ md: 8 }}>
                    <Loader loading={this.state.loading} />
                    <ButtonPrimary block onClick={this.finishDelivery}> <FinishedIcon /> Confirmar Entrega </ButtonPrimary>
                </Col>
            )
        } 

        return <div></div>
    }

}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    postFinishDelivery: (delivery, location, onSuccess) => dispatch(postFinishDeliveryAsync(delivery, location, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveredAction)