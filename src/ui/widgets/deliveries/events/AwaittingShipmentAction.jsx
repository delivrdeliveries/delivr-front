import React, {Component} from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'

import { Col } from 'react-grid-system'
import { ButtonWarning } from '../../../components/Buttons'
import OnCarriageIcon from 'react-icons/lib/fa/truck'

import { postDeliveryShippedAsync } from '../../../../deliveries/actions'
import { getGeolocation } from '../../../../geolocation';
import Loader from '../../Loader';

const OnCarriageStyledIcon = styled(OnCarriageIcon)`
    transform: scaleX(-1);
`

class AwaittingShipmentAction extends Component {

    state = {
        loading: false
    }


    constructor (props) {
        super(props)

        this.shipDelivery = this.shipDelivery.bind(this)
    }

    shipDelivery() {
        this.setState({loading: true})

        getGeolocation()
            .then(coords => {
                this.props.postDeliveryShipped(this.props.delivery, coords, () => this.setState({loading: false}))
            })
            .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    }

    render () {
       return (
            <Col md={4} offset={{md: 8}}>
                <Loader loading={this.state.loading} />
                <ButtonWarning block onClick={this.shipDelivery}> <OnCarriageStyledIcon /> Retirado pela Transportadora </ButtonWarning>
            </Col>
       )
    }

}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    postDeliveryShipped: (delivery, location, onSuccess) => dispatch(postDeliveryShippedAsync(delivery, location, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AwaittingShipmentAction)