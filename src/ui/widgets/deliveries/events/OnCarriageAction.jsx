import React, {Component} from 'react'
import {connect} from 'react-redux'

import { Col } from 'react-grid-system'
import { ButtonWarning, ButtonDanger, ButtonPrimary } from '../../../components/Buttons'
import { WhiteCard } from '../../../components/Containers'
import { Label, FieldContainer, TextArea, SubmitContainer } from '../../../components/Forms'
import Modal from '../../../components/Modal'

import { postUpdateLocationAsync, postOnOccurrenceAsync, postDeliverDeliveryAsync } from '../../../../deliveries/actions'

import UpdateLocaleIcon from 'react-icons/lib/fa/map-marker'
import InOccurrenceIcon from 'react-icons/lib/fa/exclamation-triangle'
import DeliveredIcon from 'react-icons/lib/fa/archive'
import { getGeolocation } from '../../../../geolocation';
import Loader from '../../Loader';

class OnCarriageAction extends Component {

    state = {
        loading: false,
        showFinishModal: false,
        showOccurrenceModal: false,
        onOccurrence: {
            description: ''
        },

        deliverDelivery: {
            description: ''
        }
    }

    constructor (props) {
        super(props)
        
        this.updateLocation = this.updateLocation.bind(this)
        this.onOccurrence = this.onOccurrence.bind(this)
        this.deliverDelivery = this.deliverDelivery.bind(this)
        this.closeFinishModal = this.closeFinishModal.bind(this)
        this.openFinishModal = this.openFinishModal.bind(this)
        this.closeOccurrenceModal = this.closeOccurrenceModal.bind(this)
        this.openOccurrenceModal = this.openOccurrenceModal.bind(this)
        this.updateOcurrenceDescription = this.updateOcurrenceDescription.bind(this)
        this.updateDeliverDeliveryDescription = this.updateDeliverDeliveryDescription.bind(this)
    }

    updateLocation () {
        this.setState({loading: true})
        getGeolocation()
            .then(coords => this.props.postUpdateLocation(this.props.delivery, coords, () => this.setState({loading: false})))
            .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    }

    onOccurrence () {
        const description =  this.state.onOccurrence.description.trim()

        if (!description || description.trim().length === 0) {
            alert('Necessário descrever o que aconteceu')
            return
        }

        this.setState({loading: true})

        getGeolocation()
            .then(coords => this.props.postOnOccurrence(this.props.delivery, coords, description, () => this.setState({loading: false})))
            .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    
    }

    deliverDelivery () {
        this.setState({loading: true})

        const description =  this.state.deliverDelivery.description.trim()

        getGeolocation()
            .then(coords => this.props.postDeliverDelivery(this.props.delivery, coords, description, () => this.setState({loading: false})))
            .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    }

    closeFinishModal () {
        this.setState({ ...this.state, showFinishModal: false})
    }

    openFinishModal () {
        this.setState({ ...this.state, showFinishModal: true})
    }

    closeOccurrenceModal () {
        this.setState({ ...this.state, showOccurrenceModal: false})
    }

    openOccurrenceModal () {
        this.setState({ ...this.state, showOccurrenceModal: true})
    }

    updateOcurrenceDescription (event) {
        this.setState({
            ...this.state,
            onOccurrence: {
                description: event.target.value
            }
        })
    }

    updateDeliverDeliveryDescription (event) {
        this.setState({
            ...this.state,
            deliverDelivery: {
                description: event.target.value
            }
        })
    }

    render () {
        if (this.state.showFinishModal && !this.state.loading) {
            return (
                <Modal title={`Finalizar Entrega #${this.props.delivery.locator}`} onExit={this.closeFinishModal}>

                    <WhiteCard>
                         {/* <FieldContainer>
                            <Label htmlFor='description'> Foto do Canhoto de Entrega </Label>
                            <Button block> Anexar Foto </Button>
                        </FieldContainer> */}

                        <FieldContainer>
                                <Label htmlFor='description'> Algum ponto a acrescentar? </Label>
                                <TextArea id='description' defaultValue={this.state.deliverDelivery.description} onChange={this.updateDeliverDeliveryDescription} />
                        </FieldContainer>


                        <SubmitContainer>
                            <ButtonPrimary onClick={this.deliverDelivery}> Finalizar </ButtonPrimary>
                        </SubmitContainer>
                    </WhiteCard>

                </ Modal>
            )
        }

        if (this.state.showOccurrenceModal && !this.state.loading) {
            return (
                <Modal title={`Comunicar Ocorrência com a Entrega #${this.props.delivery.locator}`} onExit={this.closeOccurrenceModal}>

                    <WhiteCard>
                        <FieldContainer>
                                <Label htmlFor='description'> Descreva o que aconteceu? </Label>
                                <TextArea id='description' onChange={this.updateOcurrenceDescription} defaultValue={this.state.onOccurrence.description}></TextArea>
                        </FieldContainer>


                        <SubmitContainer>
                            <ButtonDanger onClick={this.onOccurrence}> Colocar em Ocorrência </ButtonDanger>
                        </SubmitContainer>
                    </WhiteCard>

                </ Modal>
            )
        }


       const buttons = [
            <Col key={1} md={4}>
                <ButtonWarning block onClick={this.updateLocation}> <UpdateLocaleIcon /> Atualizar Localização </ButtonWarning>
            </Col>,
            <Col key={2} md={4}>
                <ButtonDanger block onClick={this.openOccurrenceModal}> <InOccurrenceIcon /> Em Ocorrência </ButtonDanger>
            </Col>,
            <Col key={3} md={4}>
                <ButtonPrimary block onClick={this.openFinishModal}> <DeliveredIcon /> Finalizar Entrega </ButtonPrimary>
            </Col>
        ]

        if (this.state.loading) {
            buttons.push(<Loader loading={this.state.loading} />)
        }

        return buttons
    }

}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    postUpdateLocation: (delivery, location, onSuccess) => dispatch(postUpdateLocationAsync(delivery, location, onSuccess)),
    postOnOccurrence: (delivery, location, description, onSuccess) => dispatch(postOnOccurrenceAsync(delivery, location, description, onSuccess)),
    postDeliverDelivery: (delivery, location, description, onSuccess) => dispatch(postDeliverDeliveryAsync(delivery, location, description, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(OnCarriageAction)