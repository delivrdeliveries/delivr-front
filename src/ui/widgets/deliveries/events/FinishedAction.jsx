import React, {Component} from 'react'
import {connect} from 'react-redux'

import styled from 'styled-components'
import StarRatingComponent from 'react-star-rating-component'

import { Col } from 'react-grid-system'
import { Button, ButtonPrimary } from '../../../components/Buttons'
import { WhiteCard } from '../../../components/Containers'
import { Label, FieldContainer, TextArea, SubmitContainer } from '../../../components/Forms'
import Modal from '../../../components/Modal'

import RateIcon from 'react-icons/lib/fa/star-o'
import { postRateAsync } from '../../../../deliveries/actions'
import Loader from '../../Loader';
import { getUserFromLocalStorage } from '../../../../user/actions';

const StyledStarRatingComponent = styled(StarRatingComponent)`
    margin: 10px 0px;

    .dv-star-rating-star {
        margin: 0px 5px;
        font-size: 25px
    }
`

class FinishedAction extends Component {

    state = {
        loading: false,
        showRateModal: false,
        rate: {
            value: 0,
            comment: ''
        }
    }

    constructor (props) {
        super(props)
        
        this.closeModal = this.closeModal.bind(this)
        this.openModal = this.openModal.bind(this)
        this.rateValueUpdated = this.rateValueUpdated.bind(this)
        this.rateCommentUpdated = this.rateCommentUpdated.bind(this)
        this.postRate = this.postRate.bind(this)
    }

    closeModal () {
        this.setState({showRateModal: false})
    }

    openModal () {
        this.setState({showRateModal: true})
    }

    rateValueUpdated (value) {
        const { rate }  = this.state
        rate.value = value
        this.setState({
            ...this.state,
            rate
        })
    }

    rateCommentUpdated (event) {
        const { rate }  = this.state
        const comment = event.target.value
        rate.comment = comment
        this.setState({
            ...this.state,
            rate
        })
    }

    postRate () {
        this.setState({loading: true})
        this.props.postRate(this.props.delivery, this.state.rate, () => this.setState({loading: false}))
    }

    render () {
        if (this.state.showRateModal & !this.state.loading) {
            return (
                <Modal title={`Avaliar Entrega #${this.props.delivery.locator}`} onExit={this.closeModal}>

                    <WhiteCard>
                         <FieldContainer>
                                <Label> Entre 1 e 5, que nota você dá para está entrega? </Label>
                                
                                <StyledStarRatingComponent 
                                    name="value" 
                                    starCount={5}
                                    onStarClick={this.rateValueUpdated}
                                    emptyStarColor='#ccc' />
                        </FieldContainer>

                        <FieldContainer>
                                <Label htmlFor='description'> Elogios e Críticas </Label>
                                <TextArea id='description' defaultValue={this.state.rate.comment} onChange={this.rateCommentUpdated} />
                        </FieldContainer>


                        <SubmitContainer>
                            <ButtonPrimary onClick={this.postRate}> Avaliar</ButtonPrimary>
                        </SubmitContainer>
                    </WhiteCard>

                </ Modal>
            )
        }

        const user = getUserFromLocalStorage()

        if (user.isShippingCompany() && !this.props.delivery.rate) {
            return <div />
        }

       return  (
            <Col md={4} offset={{md: 8}}>
                <Loader loading={this.state.loading} />

                {
                    this.props.delivery.rate
                    ?  <StyledStarRatingComponent 
                        name="value" 
                        starCount={5}
                        value={this.props.delivery.rate.value}
                        editing={false}
                        emptyStarColor='#ccc' />

                    : (<Button block onClick={this.openModal}> <RateIcon /> Avaliar Entrega </Button>)
                }
            </Col>
       )
    }

}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    postRate: (delivery, location, onSuccess) => dispatch(postRateAsync(delivery, location, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(FinishedAction)