import React, {Component} from 'react'
import {connect} from 'react-redux'

import { Col } from 'react-grid-system'
import { ButtonDanger, Button } from '../../../components/Buttons'

import { postDeliveryContinuedAsync, postCanceledAsync } from '../../../../deliveries/actions'
import { getGeolocation } from '../../../../geolocation';
import Loader from '../../Loader';
import { getUserFromLocalStorage } from '../../../../user/actions';

class InOccurrenceAction extends Component {

    state = {
        loading: false
    }

    constructor (props) {
        super(props)

        this.shipDelivery = this.shipDelivery.bind(this)
        this.cancelDelivery = this.cancelDelivery.bind(this)
    }

    shipDelivery() {
        this.setState({loading: true})
        getGeolocation()
        .then(coords =>  this.props.postDeliveryContinued(this.props.delivery, coords), () => this.setState({loading: false}))
        .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    }

    cancelDelivery() {
        this.setState({loading: true})
        getGeolocation()
        .then(coords =>  this.props.postCanceled(this.props.delivery, coords, 'Entrega cancelada'), () => this.setState({loading: false}))
        .catch(error => {
                window.alert(error.message)
                this.setState({loading: false})
            })
    }

    render () {
        const user = getUserFromLocalStorage()

        if (user.isShippingCompany()) {
            return <div />
        }

       const buttons = [
            <Col key={1} md={3} offset={{md: 6}} >
                <Button onClick={this.shipDelivery} block> Continuar Entrega </Button>
            </Col>,
            <Col key={2} md={3}>
                <ButtonDanger onClick={this.cancelDelivery} block> Cancelar Entrega </ButtonDanger>
            </Col>
        ]

        if (this.state.loading) {
            buttons.push(<Loader loading={this.state.loading}/>)
        }

        return buttons
    }

}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    postDeliveryContinued: (delivery, location, onSuccess) => dispatch(postDeliveryContinuedAsync(delivery, location, onSuccess)),
    postCanceled: (delivery, location, description, onSuccess) => dispatch(postCanceledAsync(delivery, location, description, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(InOccurrenceAction)