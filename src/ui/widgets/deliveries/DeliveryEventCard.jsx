import React, {Component} from 'react'
import styled from 'styled-components'

import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

import Modal from '../../components/Modal'

import states from '../../../deliveries/events'
import {getEventLabel} from '../../../deliveries/strings'
import {formatDateTime} from '../../../date'
import googleMapsConfig from '../../../config/googleMaps'

import {
    TinyText,
    Paragraph
} from '../../components/Texts'

const CardComponent = styled.div`
    background: ${p => p.theme.light};
    display: block;
    border-radius: 5px;
    border: 1px solid ${p => p.theme.grayDark};
    padding: 5px;
    padding-left: 60px;
    width: 500px;
    margin: 0 auto;
    margin-top: 10px;
    margin-bottom: 10px;
    position: relative;
    cursor: pointer;
    transition: background-color .2s linear;

    &:before {
        content: '';
        display: block;
        height: 100%;
        width: 50px;
        background: ${p => p.dataColor(p)};
        position: absolute;
        left: 0;
        top: 0;
        border-radius: 4px 0px 0px 4px;
    }

    &:hover {
        background: ${p => p.theme.contrastLight}
    }
`
const CardDate = styled(TinyText)`
    position: absolute;
    right: 20px;
`

class MapsComponent extends Component {
    render () {
        const {event} = this.props
        const latitude = parseFloat(event.latitude)
        const longitude = parseFloat(event.longitude)

        return (
            <GoogleMap
            defaultZoom={16}
            defaultCenter={{ lat: latitude, lng: longitude }}>

                <Marker position={{ lat: latitude, lng: longitude }} />
            </GoogleMap>
        )
    }
}

const MapsComponentWithProps = withScriptjs(withGoogleMap(MapsComponent))

export default class DeliveryEventCard extends Component {

    state = {showModal: false}
    
    constructor (props) {
        super(props)
        
        this.getColorFunction = this.getColorFunction.bind(this)
        this.openModal = this.openModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    getColorFunction(eventObj) {
        const {type:event} = eventObj
        const themeFunc = color => p => p.theme[color]

        const isCreated = event => event === states.CREATED
        const isOccurrence = event => event === states.OCCURRENCE
        const isCanceled = event => event === states.CANCELED
        const isFinished = event => event === states.FINISHED

        if (isFinished(event) || isCreated(event))
            return themeFunc('mainLight')
        else if (isOccurrence(event) || isCanceled(event))
            return themeFunc('dangerOpaque')
        else
            return themeFunc('warningOpaque')
    }

    openModal () {
        this.setState({...this.state, showModal: true})
    }

    closeModal () {
        this.setState({...this.state, showModal: false})
    }

    render () {
        const {event} = this.props

        if (!event)
            return <div />

        if (this.state.showModal) {
            return (
                <Modal title="Local da Interação" onExit={this.closeModal} >
                    <MapsComponentWithProps
                         event={event}
                         googleMapURL={googleMapsConfig.url}
                         loadingElement={<div style={{ height: `100%` }} />}
                         containerElement={<div style={{ height: `400px` }} />}
                         mapElement={<div style={{ height: `100%` }} />} />
                </Modal>
            )
        }
        
        return (
            <CardComponent dataColor={this.getColorFunction(event)} onClick={this.openModal}>
                
                <CardDate>{formatDateTime(event.created_at)}</CardDate>

                <Paragraph> <strong>{getEventLabel(event.type)}</strong> </Paragraph>

                <TinyText>
                    <span style={{display:'block'}}>{event.description}</span>
                </TinyText>
     
            </CardComponent>
        )
    }

}