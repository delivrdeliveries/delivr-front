import React, {Component} from 'react'
import styled from 'styled-components'

import WaitingShipmentIcon from 'react-icons/lib/fa/home'
import OnCarriageIcon from 'react-icons/lib/fa/truck'
import InOccurrenceIcon from 'react-icons/lib/fa/exclamation-triangle'
import FinishedIcon from 'react-icons/lib/fa/check-circle'
import DeliveredIcon from 'react-icons/lib/fa/archive'

import states from '../../../deliveries/status'
import {getStateLabel} from '../../../deliveries/strings'

import {
    TinyText
} from '../../components/Texts'

const StateTimeLineContainer = styled.div`
    padding: 20px;
    display: flex;
    align-items: center;
    margin-top: 20px;
    margin-bottom: 40px;
`
const StatesContainer = styled.div`
    display: flex;
    align-items: center;
    min-width: 60%;
    margin: 0 auto;
`

const StateCircle = styled.span`
    width: 70px;
    height: 70px;
    border-radius: 70px;
    display: flex;
    align-items: center;
    font-size: 2em;
    color: ${p => p.theme.light};
    background: ${p => p.dataColor(p)}
`

const State = styled.div`
    position: relative;
`

const StateSeparator = styled.span`
    width: 15%;
    height: 5px;
    background: ${p => p.dataColor(p)}    
`

const OnCarriageStyledIcon = styled(OnCarriageIcon)`
    transform: scaleX(-1);
`

const StyledTinyText = styled(TinyText)`
    position: absolute;
    width: 100px;
    text-align: center;
    left: -12px;
    top: 60px;
`

const iconsStyle = {
    margin: '0 auto'
}

export default class StateTimeLine extends Component {

    constructor (props) {
        super(props)
        
        this.getColor = this.getColor.bind(this)
        this.getColorFunc = this.getColorFunc.bind(this)
    }

    getColor () {
        switch (this.props.state) {
            case states.FINISHED:
                return 'mainDark'
            case states.IN_OCCURRENCE:
            case states.CANCELED:
                return 'danger'
            default:
                return 'warning'
        }
    }

    getColorFunc (virtualState) {
        const notActiveColor = 'grayDark'
        const themeFunc = color => p => p.theme[color]

        const isDelivered = (state) => state === states.DELIVERED
        const isFinished = (state) => state === states.FINISHED
        const isInOccurrence = (state) => state === states.IN_OCCURRENCE
        const isOnCarriage = (state) => state === states.ON_CARRIAGE
        const isAwaittingShipment = (state) => state === states.AWAITTING_SHIPMENT
        const isCanceled = (state) => state === states.CANCELED

        if (isCanceled(this.props.state)) {
            return themeFunc(this.getColor())
        } 

        if (isFinished(this.props.state)) {
            return themeFunc(this.getColor())
        } 
        
        if (isDelivered(this.props.state)) {

            if (isFinished(virtualState))
                return themeFunc(notActiveColor)
            else
                return themeFunc(this.getColor())

        }

        if (isInOccurrence(this.props.state)) {

            if (isDelivered(virtualState) || isFinished(virtualState))
                return themeFunc(notActiveColor)
            else
                return themeFunc(this.getColor())

        }

        if (isOnCarriage(this.props.state)) {

            if (isDelivered(virtualState) || isInOccurrence(virtualState) || isFinished(virtualState))
                return themeFunc(notActiveColor)
            else
                return themeFunc(this.getColor())

        }

        if (isAwaittingShipment(this.props.state)) {

            if (isAwaittingShipment(virtualState))
                return themeFunc(this.getColor())
            else
                return themeFunc(notActiveColor)

        }
    }

    render () {

        if (!this.props.state)
            return <span />

        return (
            <StateTimeLineContainer>

               <StatesContainer>

                    <State>
                        <StateCircle dataColor={this.getColorFunc(states.AWAITTING_SHIPMENT)} >
                            <WaitingShipmentIcon style={iconsStyle} />
                        </StateCircle>

                         <StyledTinyText>{getStateLabel(states.AWAITTING_SHIPMENT)}</StyledTinyText>
                    </State>

                    <StateSeparator dataColor={this.getColorFunc(states.ON_CARRIAGE)} />

                    <State>
                        <StateCircle dataColor={this.getColorFunc(states.ON_CARRIAGE)} >
                            <OnCarriageStyledIcon style={iconsStyle} />
                        </StateCircle>

                        <StyledTinyText>{getStateLabel(states.ON_CARRIAGE)}</StyledTinyText>
                    </State>

                    <StateSeparator dataColor={this.getColorFunc(states.IN_OCCURRENCE)} />

                   <State>
                        <StateCircle dataColor={this.getColorFunc(states.IN_OCCURRENCE)} >
                            <InOccurrenceIcon style={iconsStyle} />
                        </StateCircle>

                        <StyledTinyText>{getStateLabel(states.IN_OCCURRENCE)}</StyledTinyText>
                    </State>

                    <StateSeparator dataColor={this.getColorFunc(states.DELIVERED)} />

                   <State>
                        <StateCircle dataColor={this.getColorFunc(states.DELIVERED)}>
                            <DeliveredIcon style={iconsStyle} />
                        </StateCircle>

                        <StyledTinyText>{getStateLabel(states.DELIVERED)}</StyledTinyText>
                    </State>

                    <StateSeparator dataColor={this.getColorFunc(states.FINISHED)} />

                    <State>
                        <StateCircle dataColor={this.getColorFunc(states.FINISHED)}>
                            <FinishedIcon style={iconsStyle} />
                        </StateCircle>

                        <StyledTinyText>{getStateLabel(states.FINISHED)}</StyledTinyText>
                    </State>

                </StatesContainer>
                
            </StateTimeLineContainer>
        )
    }
}