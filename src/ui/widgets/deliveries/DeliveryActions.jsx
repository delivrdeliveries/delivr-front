import React, {Component} from 'react'

import states from '../../../deliveries/status'

import AwaittingShipmentAction from './events/AwaittingShipmentAction'
import OnCarriageAction from './events/OnCarriageAction'
import DeliveredAction from './events/DeliveredAction'
import FinishedAction from './events/FinishedAction'
import InOccurrenceAction from './events/InOccurrenceAction'

export default class DeliveryActions extends Component {

    render () {
        const {delivery} = this.props

        switch(delivery.status) {
            case states.AWAITTING_SHIPMENT:
                 return  <AwaittingShipmentAction delivery={delivery} />

             case states.ON_CARRIAGE:
                 return <OnCarriageAction delivery={delivery} />

             case states.DELIVERED:
                 return <DeliveredAction delivery={delivery} />

             case states.FINISHED:
                 return <FinishedAction delivery={delivery} />

            case states.IN_OCCURRENCE:
                 return <InOccurrenceAction delivery={delivery} />

            case states.CANCELED:
                 return <span></span>
                 
             default:
                 return
        }
    }

}