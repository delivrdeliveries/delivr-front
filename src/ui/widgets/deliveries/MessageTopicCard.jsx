import React, {Component} from 'react'
import styled from 'styled-components'

import {formatDate} from '../../../date'

import {
    TinyText,
    Paragraph
} from '../../components/Texts'

const CardComponent = styled.div`
    background: ${p => p.theme.contrastLight};
    display: block;
    border-radius: 5px;
    padding: 5px;
    padding-left: 20px;
    width: 500px;
    margin: 0 auto;
    margin-top: 10px;
    margin-bottom: 10px;
    position: relative;
    cursor: pointer;
    transition: background-color .2s linear;

    &:hover {
        background: ${p => p.theme.mainLight}
    }
`

const CardDate = styled(TinyText)`
    position: absolute;
    right: 20px;
`

export default class MessageTopicCard extends Component {

    render () {
        const {topic} = this.props

        if (!topic)
            return <div />

        return (
            <CardComponent onClick={this.props.onClick}>
                
                <CardDate>{formatDate(topic.date)}</CardDate>

                <Paragraph> Tópico: {topic.name} </Paragraph>
     
            </CardComponent>
        )
    }

}