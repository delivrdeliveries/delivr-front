import React, {Component} from 'react'
import {connect} from 'react-redux'
import Modal from '../../components/Modal';
import { Paragraph, RawLink, TinyText } from '../../components/Texts';
import { WhiteCard } from '../../components/Containers';
import parseCsv from '../../../csv/parseCsv';
import { Table, THeaderRow, THeaderCel, TRow, TCel } from '../../components/Tables';
import { ButtonPrimary } from '../../components/Buttons';
import { Line } from 'rc-progress'
import { Row, Col } from 'react-grid-system';
import transportingMethod from '../../../deliveries/transportingMethod';
import { postDeliveryAsync } from '../../../deliveries/actions';
import { getGeolocation } from '../../../geolocation';

class CsvDeliveryImporter extends Component {

    state = {
        percentDone: 0,
        loading: false,
        deliveriesLoaded: false,
        finished: false,
        uploading: false,
        deliveries: []
    }
    
    constructor (props) {
        super(props)
        
        this.loadFile = this.loadFile.bind(this)
        this.startUpload = this.startUpload.bind(this)
    }

    async loadFile (event) {
        this.setState({loading: true})

        const file = event.target.files[0]
        const lines = await parseCsv(file)
        const {user} = this.props

        const deliveries = lines.map(row => {return {
            load_description: row.nome,
            estimated_date: '2018-03-20',
            supplier_company_id: user.companyId,
            transporting_method: transportingMethod.PARTICULAR,
            product_type_id: parseInt(row.tipo_produto, 10),
            delivery_restriction_id: parseInt(row.restricao, 10),
            height: parseInt(row.altura, 10),
            length: parseInt(row.comprimento, 10),
            width: parseInt(row.largura, 10),
            weight: parseInt(row.peso, 10),
            start_address_id: parseInt(row.local_retirada, 10),
            end_address: {
                zip_code: row.cep,
                street: row.logradouro,
                number: row.numero,
                neighbor: row.bairro,
                reference: row.referencia,
                complement: row.complemento,
                city_id: parseInt(row.cidade, 10)
            },
            saved: false,
            status: 'Aguardando Upload'
        }})

        this.setState({loading: false, deliveriesLoaded: true, finished: false, deliveries})
    }

    async startUpload () {
        if (this.state.finished || this.state.uploading) {
            alert('Upload já realizado desse arquivo')
            return
        }
        
        const {deliveries} = this.state
        this.setState({uploading: true})

        var latitude, longitude
        try {
            const coords = await getGeolocation()
            latitude = coords.latitude
            longitude = coords.longitude
        } catch (error) {
            this.setState({finished: false, uploading: false})
            alert('Problemas ao pegar sua localização atual')
            return
        }

        var i = 0
        for (const delivery of deliveries) {
            delivery.latitude = latitude
            delivery.longitude = longitude

            try {
                await this.saveDelivery(delivery)
                delivery.saved = true
                delivery.status = 'Importada'
            } catch (e) {
                console.error(e)
                delivery.saved = false
                delivery.status = 'Erro'
            }
            
            i++
            const percentDone = parseInt(100 * i / deliveries.length,  10)
            this.setState({deliveries, percentDone})
        }

        this.setState({finished: true, uploading: false})
    }

    saveDelivery (delivery) {
        return new Promise((accept, reject) => {
            this.props.postDelivery(delivery, accept, reject)
        })
    }

    render () {
        if (!this.props.open)
            return <div> </div>

        return (
            <Modal title="Importar entregas com transportadora particular" onExit={this.props.onExit}>

                <WhiteCard>
                    <Paragraph> Como Importar? </Paragraph>

                    <TinyText>1. Faça download do template de entregas <RawLink download href="/assets/templates/deliveries_template.csv"> aqui </RawLink>.</TinyText>

                    <TinyText>2. Adicione as entregas que deseja e importe o arquivo (tem que ser .csv).</TinyText>

                    <input type="file" accept=".csv" onChange={this.loadFile}/>
                </WhiteCard>

                {this.state.loading ? (
                    <WhiteCard>
                        <TinyText>Carregando...</TinyText>
                    </WhiteCard>
                ) : null}

                {this.state.deliveriesLoaded ? (
                    <WhiteCard>
                        <Row>
                            <Col md={8}>
                                <Line percent={this.state.percentDone} strokeWidth="4" trailWidth="4" strokeColor="#77E9AB" style={{'paddingTop': '10px'}}/>
                            </Col>

                            <Col md={4}>
                                {this.state.uploading ? 
                                    <TinyText>Realizando Upload</TinyText>
                                : this.state.finished ?
                                    <TinyText> Upload realizado </TinyText>
                                : 
                                    <ButtonPrimary onClick={this.startUpload} block> Começar Upload </ButtonPrimary>
                                }
                            </Col> 
                        </Row>
                        <Table>
                            <THeaderRow>
                                <THeaderCel style={{ width: '80%' }}>Entrega</THeaderCel>
                                <THeaderCel style={{ width: '20%' }}>Status</THeaderCel>
                            </THeaderRow>

                            {this.state.deliveries.map((delivery, idx) => (
                                <TRow key={idx}>
                                    <TCel>
                                        <TinyText>{delivery.load_description} - {delivery.end_address.street}, {delivery.end_address.neighbor} - {delivery.end_address.zip_code}</TinyText>
                                    </TCel>
                                    <TCel>
                                        <TinyText style={{ color: delivery.saved ? '#25B265' : '#F76148' }}> 
                                            {delivery.status} 
                                        </TinyText>
                                    </TCel>
                                </TRow>
                            ))}
                        </Table>
                    </WhiteCard>
                ) : null}

            </Modal>
        )
    }

}
const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage
})

const mapDispatchToProps = dispatch => ({
    postDelivery: (delivery, onSuccess, onError) => dispatch(postDeliveryAsync(delivery, onSuccess, onError)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CsvDeliveryImporter)