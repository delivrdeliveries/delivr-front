import React, { Component } from 'react'
import {formatDate} from '../../../date'
import DeliveryStateIcon from '../../widgets/deliveries/StateIcon'
import { Table, TCel, TRow, THeaderRow, THeaderCel } from '../../components/Tables'
import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'

export default class ListDeliveries extends Component {

    render () {
        return (
            <div>
                <Row>
                    <Col>
                        <Table>
                            <THeaderRow>
                                <THeaderCel>Status</THeaderCel>
                                <THeaderCel>Entrega</THeaderCel>
                                <THeaderCel>Data Emissão</THeaderCel>
                                <THeaderCel>Data Prevista</THeaderCel>
                            </THeaderRow>

                            {this.props.deliveries.map(delivery => (
                                <TRow key={delivery.locator} title={delivery.statusLabel}>
                                    <TCel> <DeliveryStateIcon state={delivery.status} /> </TCel>
                                    <TCel> 
                                        <NavLink to={'/entrega/'+delivery.locator} style={{display: 'block'}} >
                                            #{delivery.locator} - {delivery.load_description}
                                        </NavLink>    
                                    </TCel>
                                    <TCel> {formatDate(delivery.created_at)} </TCel>
                                    <TCel> {formatDate(delivery.estimated_date)} </TCel>
                                </TRow>
                            ))}

                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}