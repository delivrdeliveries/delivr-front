import React, {Component} from 'react'
import styled from 'styled-components'

import states from '../../../deliveries/status'

const StateIcon = styled.span`
    display: inline-block;
    height: 15px;
    width: 15px;
    border-radius: 15px;
    margin-left: 15px;
`
const StateIconDelivered = StateIcon.extend`
    background: ${p => p.theme.warning }
`
const StateIconFinished = StateIcon.extend`
    background: ${p => p.theme.main }
`
const StateIconOnCarriage = StateIcon.extend`
    background: ${p => p.theme.warning }
`
const StateAwaittingShipment = StateIcon.extend`
    background: ${p => p.theme.warning }
`
const StateIconInOccurrence = StateIcon.extend`
    background: ${p => p.theme.danger }
`
const StateIconCanceled = StateIcon.extend`
    background: ${p => p.theme.danger }
`

export default class DeliveryStateIcon extends Component {
    render () {
        switch (this.props.state) {
            case states.DELIVERED:
                return <StateIconDelivered />
            case states.FINISHED:
                return <StateIconFinished />
            case states.ON_CARRIAGE:
                return <StateIconOnCarriage />
            case states.IN_OCCURRENCE:
                return <StateIconInOccurrence />
            case states.CANCELED:
                return <StateIconCanceled />
            case states.AWAITTING_SHIPMENT:
                return <StateAwaittingShipment />
            default:
                return <StateIcon />
        }
    }
}