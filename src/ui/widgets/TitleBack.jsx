import React, {Component} from 'react'
import styled from 'styled-components'
import {Redirect} from 'react-router-dom'
import BackIcon from 'react-icons/lib/fa/arrow-left'

const BackContainer = styled.span`
    float: right;
    margin-top: 12px;
    cursor: pointer;
    font-size: .5em;
    display: flex;
    align-items: center;  

    &:hover {
        color: ${p => p.theme.mainLight}
    }
`

export default class TitleBack extends Component {

    state = {
        redirect: false
    }

    constructor (props) {
        super(props)

        this.goBack = this.goBack.bind(this)
    }

    goBack () {
        if (!this.props.backTo) {
            window.history.back()
        } else {
            this.setState({redirect: true})
        }
    }

    render () {
        if (this.state.redirect) {
            return <Redirect to={this.props.backTo} />
        }

        return (
            <BackContainer onClick={this.goBack} >
                <BackIcon onClick={this.goBack}/> Voltar
            </BackContainer>
        )
    }

}