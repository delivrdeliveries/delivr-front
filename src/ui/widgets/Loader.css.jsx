import styled, {keyframes} from 'styled-components'
import {IsotypeIcon} from '../components/Icons'

export const LoaderContainer = styled.div`
    position: fixed;
    width: 100%;
    height: calc(100% - 55px);
    bottom: 0;
    left: 0px;
    display: flex;
    align-itens: center;
    background: rgba(255, 255, 255, .9);
    z-index: 1;
`

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

export const LoaderIsotype = styled(IsotypeIcon)`
    width: 75px;
    margin: 0 auto;
    animation: ${rotate360} 5s linear infinite;
`