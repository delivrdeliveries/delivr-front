import styled from 'styled-components'
import {NavLink} from 'react-router-dom'
import {IsotypeIcon, LogoIcon} from '../components/Icons'

export const HeaderContainer = styled.header`
    background: ${p => p.theme.mainLight};
    height: 55px;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    z-index: 1;
`
export const List = styled.ul`
    margin: 0px;
    padding: 0px;
    position: relative;
    left: 30px;
`
export const ListItem = styled.li`
    color: ${p => p.theme.dark};
    display: inline-block;
    margin: 0px 5px;
    padding: 3px 10px;
    padding-bottom: 5px;
    border-radius: 5px;
    text-align: center;
    transition: background-color .2s linear;

    &:hover {
        background: ${p => p.theme.contrastLight};
    }
`
export const StyledNavLink = styled(NavLink)`
    color: inherit;
    text-decoration: none;
    padding: 3px 10px;
    padding-bottom: 5px;
    display: inline;
`
export const CircleContainer = styled.div`
    position: absolute;
    right: 20px;

    &:hover {
        top: 18px;
    }
`
export const CircleName = styled.span`
    background: ${p => p.theme.contrastLight};
    padding: 10px 15px;
    border-radius: 30px;
    font-weight: bold;
    transition: background-color .5s linear;
    cursor: pointer;

    &:hover, ${CircleContainer}:hover & {
        background: ${p => p.theme.contrastDark};
        color: ${p => p.theme.light};
        padding: 10px  15px;
        position: absolute;
        right: 0px;
        top: -9px;
    }
`
export const FloatingPointer = styled.span`
    display: none;
    width: 0; 
    height: 0; 
    border-left: 8px solid transparent;
    border-right: 8px solid transparent;
    border-bottom: 8px solid ${p => p.theme.mainDark};
    position: absolute;
    top: 34px;
    right: 13px;

    &:hover, ${CircleContainer}:hover &, ${CircleName}:hover & {
        display: block; 
    }
`
export const FloatingMenu = styled.ul`
    display: none;
    position: relative;
    right: 8px;
    top: 25px;
    width: 150px;
    background: ${p => p.theme.mainDark};
    padding: 0px;
    border-radius: 5px;

    &:hover, ${CircleContainer}:hover &, ${CircleName}:hover & {
        display: block; 
    }
`
export const FloatingMenuItem = styled.li`
    margin: 0px 5px;
    display: block;
    margin: 0 auto;
    color: ${p => p.theme.light};
    border-bottom: 1px solid ${p => p.theme.contrastLight};
    transition: background-color .2s linear;
    
    &:hover {
        background: ${p => p.theme.mainLight};
    }

    &:last-child {
        border-radius: 0px 0px 5px 5px;
        border-bottom: 0px;
    }

    &:first-child {
        border-radius: 5px 5px 0px 0px;
    }

    &:only-child {
        border-radius: 5px;
        border-bottom: 0px;
    }
`
export const FloatingNavLink = styled(NavLink)`
    display: block;
    padding: 8px 15px;
    text-align: center;
    text-decoration: none;
    color: inherit;
`
export const Isotype = styled(IsotypeIcon)`
    width: 35px;
    position: relative;
    left: 20px;
`

export const Logo = styled(LogoIcon)`
    width: 130px;
    position: relative;
    left: 20px;
`