import React, {Component} from 'react'

import {
    LoaderContainer,
    LoaderIsotype
} from './Loader.css'

class Loader extends Component {
    render () {
        if (this.props.loading) {
            return (
                <LoaderContainer>
                    <LoaderIsotype />
                </LoaderContainer>
            )
        } 
        return <span></span>
    }
}

export default Loader