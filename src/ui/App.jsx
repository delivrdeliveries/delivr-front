import React, { Component } from 'react'
import { ConnectedRouter } from 'react-router-redux'
import {ThemeProvider} from 'styled-components'
import {connect} from 'react-redux'

import Header from './widgets/Header'
import RouterContainer from './RouterContainer'
import {theme} from './App.css'
import { push } from 'react-router-redux'
import { history } from '../redux/store'
import { isPublicRoute } from '../config/publicRoutes'
import { withFirebase } from 'react-redux-firebase'
import { loadUserFromLocalStorage } from '../user/actions';


class App extends Component {

    state = {
        loadingUser: true,
        userLogged: true,
    }


    componentDidMount() {
         const firebase = this.props.firebase

        firebase.auth().onAuthStateChanged(
            user => this.setState({
                userLogged: !!user,
                loadingUser: false
                }),
            () => this.setState({ userLogged: false })
        )

        this.props.loadUserFromLocalStorage()
    }

    render () {
        if (!this.state.userLogged && this.props.location && !isPublicRoute(this.props.location.pathname))
            this.props.redirect('/')

        return (
            <ConnectedRouter history={history}>
                <ThemeProvider theme={theme}>
                    <div>
                        <Header />
                        <RouterContainer />
                    </div>
                </ThemeProvider>
            </ConnectedRouter>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage,
    loadingUser: state.userReducer.loadingUser,
    location: state.routerReducer.location
})

const mapDispatchToProps = dispatch => ({
    loadUserFromLocalStorage: () => dispatch(loadUserFromLocalStorage()),
    redirect: route => dispatch(push(route)),
})

export default withFirebase(connect(mapStateToProps, mapDispatchToProps)(App))