import React, {Component} from 'react'
import styled from 'styled-components'

const ModalContainer = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background: rgba(255, 255, 255, .9);
    z-index: 1;
`
const ModalBody = styled.div`
    margin: 100px auto;
    width: 800px;
    max-height: 550px;
    min-height: 200px;
    background: ${p => p.theme.grayLight};
    border-radius: 5px;
    padding-bottom: 20px;
`
const ModalHeader = styled.h3`
    position: relative;
    margin: 0px;
    width: calc(100% - 20px);
    height: 50px;
    color: ${p => p.theme.dark};
    background: ${p => p.theme.mainLight};
    border-radius: 5px 5px 0px 0px;
    padding-left: 20px;
    line-height: 50px;
    margin-bottom: 20px;
`

const ModalExit = styled.span`
    position: absolute;
    right: 10px;
    top: 10px;
    line-height: 17px;
    width: 20px;
    height: 20px;
    text-align: center;
    cursor: pointer;
    border-radius: 5px;
    transition: background-color .2s linear;

    &:hover {
        background: ${p => p.theme.contrastLight};
    }
`

const ModalContent = styled.div`
    padding-right: ${p => p.noPadding ? '0px' : '20px'};
    padding-left: ${p => p.noPadding ? '0px' : '20px'};
    overflow-y: auto;
    max-height: 500px;
`

export default class Modal extends Component {

    render () {
        return (
            <ModalContainer>
                <ModalBody>
                    <ModalHeader>
                        {this.props.title}

                        <ModalExit onClick={this.props.onExit}>x</ModalExit>
                    </ModalHeader>

                    <ModalContent noPadding={this.props.noPadding}>
                        {this.props.children}
                    </ModalContent>
                </ModalBody>
            </ModalContainer>
        )
    }

}