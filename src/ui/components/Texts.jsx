import styled from 'styled-components'

import { NavLink } from 'react-router-dom'

export const Title = styled.h1`
    font-family: ${p => p.theme.featuredFont };
    color: ${p => p.theme.contrastDark};
    font-size: 2.5em;
    margin-bottom: 10px;
`
export const Subtitle = styled.h3`
    font-family: ${p => p.theme.featuredFont };
    color: ${p => p.theme.mainDark};
    font-size: 1em;
    font-style: italic;
    font-weight: normal;
`
export const Paragraph = styled.p`
    font-family: ${p => p.theme.mainFont };
    font-size: 0.98em;
    line-height: 22px;
`
export const TinyText = styled.p`
    font-family: ${p => p.theme.mainFont };
    font-size: 0.8em;
`
export const Link = styled(NavLink)`
    font-family: ${p => p.theme.mainFont };
    font-size: 0.98em;
    line-height: 22px;
    color: ${p => p.theme.mainDark};
    transition: color .2s linear;

    &:hover {
        color: ${p => p.theme.main}
    }
`
export const RawLink = styled.a`
    font-family: ${p => p.theme.mainFont };
    font-size: 0.98em;
    line-height: 22px;
    color: ${p => p.theme.mainDark};
    transition: color .2s linear;

    &:hover {
        color: ${p => p.theme.main}
    }
`