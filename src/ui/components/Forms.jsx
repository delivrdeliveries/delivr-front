import React, {Component} from 'react'
import styled from 'styled-components'
import MaskInput from 'react-text-mask'
import CurrencyInput from 'react-currency-input'

export const Form = styled.form`
    display: block;
    width: 100%;
`

export const Label = styled.label`
    font-family: ${p => p.theme.mainFont };
    color: ${p => p.theme.dark};
    font-size: 0.98em;
    line-height: 22px;
    display: block;
    margin-bottom: 5px;
`
export const FieldContainer = styled.div`
    display: block;
    width: 100%;
    margin-top: 10px;
    margin-bottom: 5px;
    text-align: left;
`

export const SubmitContainer = styled.div`
    display: block;
    margin-top: 20px;
    margin-bottom: 5px;
    text-align: right;

    & a {
        margin-right: 20px;
        margin-left: 20px;
    }
`
export const TextField = styled.input`
    position: relative;
    padding: 0 20px;
    height: 40px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 1px solid ${p => p.invalid ? p.theme.dangerLight : p.theme.main};
    color: ${p => p.invalid ? p.theme.danger : p.theme.mainDark};
    display: ${p => p.block ? 'block' : 'inline-block'};
    width: ${p => p.block ? 'calc(100% - 45px)' : 'auto'};

    ::placeholder {
        color: ${p => p.invalid ? p.theme.danger : p.theme.mainLight};
    }
`

export const MaskField = styled(MaskInput)`
    position: relative;
    padding: 0 20px;
    height: 40px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 1px solid ${p => p.invalid ? p.theme.dangerLight : p.theme.main};
    color: ${p => p.invalid ? p.theme.danger : p.theme.mainDark};
    display: ${p => p.block ? 'block' : 'inline-block'};
    width: ${p => p.block ? 'calc(100% - 45px)' : 'auto'};

    ::placeholder {
        color: ${p => p.invalid ? p.theme.danger : p.theme.mainLight};
    }
`

export const CurrencyField = styled(CurrencyInput)`
    position: relative;
    padding: 0 20px;
    height: 40px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 1px solid ${p => p.invalid ? p.theme.dangerLight : p.theme.main};
    color: ${p => p.invalid ? p.theme.danger : p.theme.mainDark};
    display: ${p => p.block ? 'block' : 'inline-block'};
    width: ${p => p.block ? 'calc(100% - 45px)' : 'auto'};

    ::placeholder {
        color: ${p => p.invalid ? p.theme.danger : p.theme.mainLight};
    }
`

export const TextArea = styled.textarea`
    padding: 10px 20px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 1px solid ${p => p.invalid ? p.theme.dangerLight : p.theme.main};
    color: ${p => p.invalid ? p.theme.danger : p.theme.mainDark};
    display: block;
    width: calc(100% - 45px);
    resize: none;

    ::placeholder {
        color: ${p => p.invalid ? p.theme.danger : p.theme.mainLight};
    }
`
export const Select = styled.select`
    padding: 0 20px;
    height: 40px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 1px solid ${p => p.invalid ? p.theme.dangerLight : p.theme.main};
    color: ${p => p.invalid ? p.theme.danger : p.theme.mainDark};
    display: ${p => p.block ? 'block' : 'inline-block'};
    width: ${p => p.block ? 'calc(100% - 5px)' : '0px'};
    min-width: 252px;
`
const RadioCheckmark = styled.span`
    position: absolute;
    top: 2px;
    left: 2px;
    height: 6px;
    width: 6px;
    background-color: ${p => p.theme.contrastLight};
    border-radius: 10px;
    transition: background-color .2s linear;
`
const RadioButton = styled.div`
    display: inline-block;
    position: relative;
    width: 10px;
    height: 10px;
    border: 1px solid ${p => p.theme.main};
    cursor: pointer;
    border-radius: 10px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    &:hover input ~ .checkmark {
        background-color:  ${p => p.theme.main};
    }

    & input:checked ~ .checkmark {
        background-color:  ${p => p.theme.mainDark};
    }

    & input:checked ~ .checkmark:after {
        display: block;
    }
      
`
const CheckboxCheckmark = styled.span`
    position: absolute;
    top: 0;
    left: 0;
    height: 10px;
    width: 10px;
    background-color: ${p => p.theme.light};
    transition: background-color .2s linear;
    
    &:after {
        content: "";
        position: absolute;
        display: none;
        left: 3px;
        top: 1px;
        width: 2px;
        height: 5px;
        border: solid white;
        border-width: 0 2px 2px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
        transition: display .2s linear;
    }
`
const CheckboxButton = styled.div`
    display: inline-block;
    position: relative;
    width: 10px;
    height: 10px;
    border: 1px solid ${p => p.theme.main};
    cursor: pointer;
    border-radius: 2px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    &:hover input ~ .checkmark {
        background-color:  ${p => p.theme.main};
    }

    & input:checked ~ .checkmark {
        background-color:  ${p => p.theme.mainDark};
    }

    & input:checked ~ .checkmark:after {
        display: block;
    }
      
`
const ButtonContainer = styled.div`
    display: inline;
    margin-right: 10px;
`
const PrivateLabel = styled.label`
    font-family: ${p => p.theme.mainFont };
    color: ${p => p.theme.mainDark};
    font-size: 0.98em;
    line-height: 22px;
    margin-left: 5px;
    display: inline;
`

export class Radio extends Component {

    state = {}

    componentDidMount () {
        this.setState({})
        this.onChange = this.onChange.bind(this)
    }


    onChange (evt) {
        if(this.props.onChange) {
            this.props.onChange(evt)
        }
    }

    render() {
        return (
            <ButtonContainer>
                <RadioButton>

                     <input type="radio" 
                        name={this.props.name} 
                        value={this.props.value} 
                        id={this.props.id} 
                        onClick={this.onChange}
                        onChange={this.onChange}
                        style={{
                            position: 'absolute',
                            opacity: 0
                        }} />

                        <RadioCheckmark className="checkmark"/>
                </RadioButton>

                {(() => {
                    if (this.props.label) 
                        return <PrivateLabel htmlFor={this.props.id}>{this.props.label}</PrivateLabel>
                })()}
            </ButtonContainer>
        )
    }
}

export class Checkbox extends Component {

    state = {
        checked: false
    }

    componentDidMount () {
        this.setState({checked: this.state.checked})
        this.onChange = this.onChange.bind(this)
    }


    onChange (evt) {
        this.setState({checked : !this.state.checked})

        if(this.props.onChange) {
            this.props.onChange(evt)
        }
    }

    render() {
        return (
            <ButtonContainer>
                <CheckboxButton onClick={this.onChange}>

                     <input type="checkbox" 
                        name={this.props.name} 
                        value={this.props.value} 
                        id={this.props.id} 
                        onClick={this.onChange}
                        onChange={this.onChange}
                        checked={this.state.checked}
                        style={{
                            position: 'absolute',
                            opacity: 0
                        }} />

                        <CheckboxCheckmark className="checkmark"/>
                </CheckboxButton>

                {(() => {
                    if (this.props.label) 
                        return <PrivateLabel htmlFor={this.props.id}>{this.props.label}</PrivateLabel>
                })()}
            </ButtonContainer>
        )
    }
}