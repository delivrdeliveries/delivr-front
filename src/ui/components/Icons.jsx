import React from 'react'

const Icon = ({className, name, alt, style}) => <img src={'/assets/icons/' + name + '.svg'} alt={alt} className={className} style={style}/>

export const IsotypeIcon = ({className, style}) => <Icon name='delivr_isotype' alt='Delivr' className={className} style={style}/>

export const LogoIcon = ({className, style}) => <Icon name='delivr_logo' alt='Delivr' className={className} style={style}/>

export default Icon