import styled from 'styled-components'

export const Button = styled.button`
    padding: ${p => p.tiny ? '5px 10px' : ' 0 20px'};
    height: ${p => p.tiny ? 'auto' : ' 40px'};
    border-radius: 5px;
    font-size: ${p => p.tiny ? '0.8em' : '0.96em'};
    cursor: pointer;
    background: ${p => p.selected ? p.theme.mainLight : p.theme.light};
    border: 1px solid ${p => p.selected ? p.theme.mainDark : p.theme.grayDark};
    color: ${p => p.theme.dark};
    display: ${p => p.block ? 'block' : 'inline-block'};
    width: ${p => p.block ? 'calc(100% - 5px)' : 'auto'};
    transition: background-color .2s linear;

    &:hover {
        background: ${p => p.theme.grayDark};
        border: 1px solid ${p => p.theme.grayDark};
        color: ${p => p.theme.light};
    }

    &:active {
        background: ${p => p.theme.light};
        border: 1px solid ${p => p.theme.grayDark};
        color: ${p => p.theme.dark};
    }

    & svg {
        position: relative;
        bottom: 1px;
        right: 2px;
    }
`
export const ButtonPrimary = Button.extend`
    ${p => !p.disabled ?
        `
        background: ${p.theme.mainDark};
        border: 1px solid ${p.theme.mainDark};
        color: ${p.theme.light};
    
        &:hover {
            background: ${p.theme.mainLight};
            border: 1px solid ${p.theme.mainLight};
            color: ${p.theme.light};
        }
    
        &:active {
            background: ${p.theme.mainDark};
            border: 1px solid ${p.theme.mainDark};
            color: ${p.theme.light};
        }
        ` :
        `
        background: ${p.theme.contrastLight};
        border: 1px solid ${p.theme.contrastLight};
        color: ${p.theme.mainLight};
        cursor: initial;
    
        &:hover {
            background: ${p.theme.contrastLight};
            border: 1px solid ${p.theme.contrastLight};
            color: ${p.theme.mainLight};
        }
    
        &:active {
            background: ${p.theme.contrastLight};
            border: 1px solid ${p.theme.contrastLight};
            color: ${p.theme.mainLight};
        }
        `}
`
export const ButtonWarning = Button.extend`
   ${p => !p.disabled ? 
    `
    background: ${p.theme.warning};
    border: 1px solid ${p.theme.warning};
    color: ${p.theme.light};

    &:hover {
        background: ${p.theme.warningLight};
        border: 1px solid ${p.theme.warningLight};
        color: ${p.theme.light};
    }

    &:active {
        background: ${p.theme.warning};
        border: 1px solid ${p.theme.warning};
        color: ${p.theme.light};
    }
    ` : 
    `
    background: ${p.theme.warningOpaque};
    border: 1px solid ${p.theme.warningOpaque};
    color: ${p.theme.light};
    cursor: initial;

    &:hover {
        background: ${p.theme.warningOpaque};
        border: 1px solid ${p.theme.warningOpaque};
        color: ${p.theme.light};
    }

    &:active {
        background: ${p.theme.warningOpaque};
        border: 1px solid ${p.theme.warningOpaque};
        color: ${p.theme.light};
    }
    `}
`
export const ButtonDanger = Button.extend`
    ${p => !p.disabled ? 
    `
    background: ${p.theme.danger};
    border: 1px solid ${p.theme.danger};
    color: ${p.theme.light};

    &:hover {
        background: ${p.theme.dangerLight};
        border: 1px solid ${p.theme.dangerLight};
        color: ${p.theme.light};
    }

    &:active {
        background: ${p.theme.danger};
        border: 1px solid ${p.theme.danger};
        color: ${p.theme.light};
    }
    ` :
    `
    background: ${p.theme.dangerOpaque};
    border: 1px solid ${p.theme.dangerOpaque};
    color: ${p.theme.light};
    cursor: initial;

    &:hover {
        background: ${p.theme.dangerOpaque};
        border: 1px solid ${p.theme.dangerOpaque};
        color: ${p.theme.light};
    }

    &:active {
        background: ${p.theme.dangerOpaque};
        border: 1px solid ${p.theme.dangerOpaque};
        color: ${p.theme.light};
    }
    `}
`

export const FloatingActionButton = styled.button`
    width: 50px;
    height: 50px;
    border-radius: 100px;
    position: fixed;
    bottom: 20px;
    right: 2%;
    font-size: 2em;
    display: flex;
    box-align: center;
    text-aligin: center;
    margin: 5px;
    cursor: pointer;
    background: ${p => p.theme.main};
    color: ${p => p.theme.light};
    border: 0px;
    transition: background-color .2s linear;

    &:hover {
        background: ${p => p.theme.mainDark};
    }
`