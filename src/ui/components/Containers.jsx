import styled from 'styled-components'

export const Card = styled.div`
    background: ${p => p.theme.grayLight};
    padding: 20px;
    border-radius: 5px;
    margin-top: 10px;
    position: relative;
`
export const Tab = styled.span`
    background: ${p => p.active ? p.theme.grayLight : p.theme.grayDark};
    display: inline-block;
    padding: 15px;
    border-radius: 5px 5px 0px 0px;
    margin-right: 1px;
    margin-top: 10px;
    transition: background-color .2s linear;
    cursor: pointer;
    color: ${p => p.theme.contrastDark};

    & svg {
        color: ${p => p.theme.mainDark};
        position: relative;
        bottom: 1px;
    }

    &:hover {
        background: ${p => p.theme.contrastLight};
    }
`
export const TabbedCard = styled.div`
    ${Card} {
        margin-top: 0px;
        border-radius: 0px 5px 5px 5px;
    }
`
export const WhiteCard = styled.div`
    background: ${p => p.theme.light};
    padding: 20px;
    border-radius: 5px;
    margin-top: 10px;
    margin-bottom: 10px;
    position: relative;
`

export const Separator = styled.div`
    background: ${p => p.theme.light};
    display: block;
    height: 1px;
    width: 100%;
    margin-top: 10px;
    margin-bottom: 10px;
`

export const PaginationContainer = styled.div`
    margin-top: 10px;
    display: flex;
    justify-content: space-between;
`