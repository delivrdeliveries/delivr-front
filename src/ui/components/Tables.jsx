import React, {Component} from 'react'
import styled from 'styled-components'

const TableWrapper = styled.table`
    display: table;
    width: calc(100%);
    padding: 20px;
    border-radius: 5px;
    border-spacing: 0;
    border-collapse: collapse;
    margin-top: 10px;
    background: ${p => p.theme.grayLight};
`

export class Table extends Component {
    render () {
        return (
            <TableWrapper>
                <tbody>
                    {this.props.children}
                </tbody>
            </TableWrapper>
        )
    }
}

export const THeaderCel = styled.th`
    text-align: left;
    padding: 10px;

    &:first-child {
        padding-left: 20px
    }
`
export const TCel = styled.td`
    text-align: left;
    padding: 10px;

    &:first-child {
        padding-left: 20px
    }
`
export const THeaderRow = styled.tr`
    display: table-row;

    &:last-child td:last-child {
        border-radius: 0px 0px 5px 0px;
    }
    
    &:last-child td:first-child {
        border-radius: 0px 0px 0px 5px;
    }

    &:first-child td:last-child {
        border-radius: 0px 5px 0px 0px;
    }
    
    &:first-child td:first-child {
        border-radius: 5px 0px 0px 0px;
    }
`
export const TRow = THeaderRow.extend`
    display: table-row;
    border-radius: 5px;
    transition: background-color .2s linear;
    background: ${p => p.selected ? p.theme.contrastLight : 'transparent'};
    cursor: ${p => p.selectable ? 'pointer' : 'initial' };

    &:hover {
        background: ${p => p.theme.grayDark}
    }
`