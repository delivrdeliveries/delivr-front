import React, {Component} from 'react'
import styled from 'styled-components'
import {NavLink} from 'react-router-dom'

const Container = styled.div`
    background: ${p => p.theme.grayLight};
    border-radius: 5px;
    min-height: 300px;
    padding-top: 10px;
    padding-bottom: 10px;
    margin-top: 10px;
`
const List = styled.ul`
    padding: 0px;
`
const Item = styled.li`
    list-style: none;
    line-height: 24px;
`
const Link = styled(NavLink)`
    display: block;
    padding: 10px;
    padding-left: 20px;
    font-size: 0.9em;
    color: ${p => p.theme.contrastDark};
    ${p => p.selected ? `background: ${p.theme.grayDark};`: ''}

    & svg {
        color: ${p => p.theme.mainDark};
        position: relative;
        bottom: 2px;
    }

    &:hover {
        background: ${p => p.theme.contrastLight};
    }
`

export class Menu extends Component {
    render () {
        return (
            <Container>
                <List>
                    {this.props.children}
                </List>
            </Container>
        )
    }
}

export class MenuItem extends Component {
    render () {
        return (
            <Item>
                <Link selected={this.props.selected} to={this.props.to}>{this.props.children}</Link>
            </Item>
        )
    }
}