import { injectGlobal } from 'styled-components'

injectGlobal`
    @import url('https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i|Raleway:400,400i,600,600i,700,700i');
   
    * {
        font-family: 'Lato', sans-serif;
    }

    p, a, li, span {
        font-size: 0.98em;
    }

    body {
        margin: 0;
        padding: 0;
    }

    a {
        color: #000;
        text-decoration: none;
    }
`

export const theme = {
    mainFont: "'Lato', sans-serif",
    featuredFont: "'Raleway', sans-serif",

    main: '#30E582',
    mainDark: '#25B265',
    mainLight: '#77E9AB',
    grayLight: '#F5F5F5',
    grayDark: '#E3E3E3',
    warning: '#E2BD00',
    warningLight: '#FFDF3E',
    warningOpaque: '#FFEE95',
    danger: '#F76148',
    dangerLight: '#FF8571',
    dangerOpaque: '#FF9C8C',
    light: '#FFFFFF',
    dark: '#000000',
    contrastLight: '#DEFFEB',
    contrastDark: '#00441B',
}