import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import ListDeliveries from './ListDeliveries'
import CreateDelivery from './CreateDelivery'

export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/fornecedora/entregas' component={ListDeliveries} />
                <Route exact path='/fornecedora/entregas/nova' component={CreateDelivery} />
            </div>
        )
    }
}