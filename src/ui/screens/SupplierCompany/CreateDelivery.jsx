import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Row, Col } from 'react-grid-system'
import TitleBack from '../../widgets/TitleBack'
import Loader from '../../widgets/Loader'
import { Title, Paragraph } from '../../components/Texts'
import { Card, WhiteCard } from '../../components/Containers'
import { FieldContainer, Label, TextField, MaskField, Select, SubmitContainer } from '../../components/Forms'
import { Table, TRow, TCel, THeaderCel, THeaderRow } from '../../components/Tables'
import { ButtonPrimary, Button } from '../../components/Buttons'
import { loadSupplierCompanyAddressesAsync } from '../../../supplierCompanies/actions'
import { loadCitiesAsync } from '../../../cities/actions'
import { loadDeliveryRestrictionsAsync } from '../../../deliveryRestrictions/actions'
import { loadProductTypesAsync } from '../../../productTypes/actions'
import { loadShippingCompaniesSuggestionsAsync } from '../../../shippingCompanies/actions'
import { postDeliveryAsync } from '../../../deliveries/actions'
import { getGeolocation, getAddressByCep } from '../../../geolocation'
import transportingMethod from '../../../deliveries/transportingMethod';
import { getUserFromLocalStorage } from '../../../user/actions';
import FormValidator, { isEmpty, isCEP } from '../../../formValidator';
import formatCurrency from '../../../currency/formatCurrency';

class CreateDelivery extends Component {

    state = {
        loading: false,
        delivery: {
            estimated_date: '2018-03-20',
            end_address: {},
        }
    }

    constructor (props) {
        super(props)

        this.onChangeDeliveryAttr = this.onChangeDeliveryAttr.bind(this)
        this.onChangeAddressAttr = this.onChangeAddressAttr.bind(this)
        this.submitDelivery = this.submitDelivery.bind(this)
        this.loadAddressByCep = this.loadAddressByCep.bind(this)

        this.validator = new FormValidator([
            {name: 'load_description', validators: [isEmpty]},
            {name: 'product_type', validators: [isEmpty]},
            {name: 'delivery_restriction', validators: [isEmpty]},
            {name: 'weight', validators: [isEmpty]},
            {name: 'length', validators: [isEmpty]},
            {name: 'height', validators: [isEmpty]},
            {name: 'width', validators: [isEmpty]},
            {name: 'zip_code', validators: [isCEP]},
            {name: 'street', validators: [isEmpty]},
            {name: 'number', validators: [isEmpty]},
            {name: 'neighbor', validators: [isEmpty]},
            {name: 'city', validators: [isEmpty]},
            {name: 'start_address', validators: [isEmpty]},
        ])
    }

    componentDidMount () {
        const user = getUserFromLocalStorage()

        if (user.isShippingCompany()) {
            window.location.href = '/'
        }

        this.props.loadSupplierCompanyAddresses(user.companyId)
        this.props.loadCities()
        this.props.loadDeliveryRestrictions()
        this.props.loadProductTypes()
    }

    onChangeDeliveryAttr (evt) {
        const delivery = this.state.delivery
        delivery[evt.target.name] = evt.target.value 
        this.setState({ delivery })
    }

    onChangeAddressAttr (evt) {
        const delivery = this.state.delivery
        delivery.end_address[evt.target.name] = evt.target.value 
        this.setState({ delivery })

        if (delivery.shipping_company_id) {
            this.selectTransportMethod(null)
        }
    }

    async selectTransportMethod (method) {
        if (this.validator.isFormInvalid()) {
            alert('Por favor corrija os campos inválidos')
            return
        }

        const { delivery } = this.state
        delivery.transporting_method = method
        delivery.shipping_company_id = null
        delivery.estimated_price = null

        this.setState({ delivery })

        if (method === transportingMethod.OUTSOURCED) {
            this.setState({loading: true})
            this.props.loadShippingCompanySuggestions(
                this.state.delivery,
                () => this.setState({loading: false}),
                (error) => {
                    alert('Problemas ao sugestões de transportadoras')
                    console.error(error)
                    this.setState({loading: false})
                }
            )
        }

    }

    getCity(id) {
        return this.props.cities.filter(c => c.id === parseInt(id, 10))[0]
    }

    getSelectedEndAddress () {
        const {end_address} = this.state.delivery
        end_address.city = this.getCity(end_address.city_id)
        return end_address
    }

    getSelectedStartAddress () {
        return this.props.addresses.filter(a => a.id === parseInt(this.state.delivery.start_address_id, 10))[0]
    }

    submitDelivery () {
        if (this.validator.isFormInvalid()) {
            alert('Por favor corrija os campos inválidos')
            return
        }

        const {delivery} = this.state
        const {postDelivery} = this.props

        if (!delivery.transporting_method) {
            alert('selecione um método de entrega')
            return
        }

        if (delivery.transporting_method === transportingMethod.OUTSOURCED && !delivery.shipping_company_id) {
            alert('Selecione uma transportadora')
            return
        }

        delivery.supplier_company_id = this.props.user.companyId
        delivery.end_address.zip_code = delivery.end_address.zip_code.replace(/[^0-9]/g, '')

        this.setState({loading: true})
        getGeolocation().then(coords => {
            delivery.latitude = coords.latitude
            delivery.longitude = coords.longitude
            postDelivery(delivery, 
                (delivery) => {
                    window.location.href = '/entrega/' + delivery.locator
                }, 
                () => this.setState({loading: false}))
        }).catch(err => {
            this.setState({loading: false})
            window.alert(err.message)
        })
    }

    async loadAddressByCep() {
        const {end_address} = this.state.delivery

        if (!end_address.zip_code || end_address.zip_code.length !== 9) {
            return
        }

        try {
            this.setState({loading: true})
            const address = await getAddressByCep(this.state.delivery.end_address.zip_code)            

            end_address.street = address.logradouro
            end_address.neighbor = address.bairro
            end_address.city_id = address.ibge

            this.setState({loading: false})
        } catch (error) {
            alert('CEP não econtrado.')
            this.setState({loading: false})
        }
    }

    selectShippingCompany(shippingCompany) {
        const {delivery} = this.state
        delivery.shipping_company_id = shippingCompany.id
        delivery.estimated_price = shippingCompany.estimated_price
        this.setState({delivery})
    }

    render () {
        const {delivery} = this.state
        const {isFieldInvalid} = this.validator
        return (
            <div>                
                <Title>Nova Entrega <TitleBack /></Title>
                <Card>
                    <Paragraph>Informações gerais da entrega</Paragraph>
                    <WhiteCard>
                        <Row>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="load_description">Nome do Produto/Entrega</Label>
                                    <TextField 
                                            id="load_description" 
                                            name="load_description" 
                                            placeholder="Nome" 
                                            block 
                                            onChange={this.onChangeDeliveryAttr}
                                            invalid={isFieldInvalid('load_description', delivery.load_description)} />
                                </FieldContainer>
                            </Col>
                        </Row>

                        <Row> 
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="product_type_id">Tipo do Produto</Label>
                                      <Select onChange={this.onChangeDeliveryAttr} block name="product_type_id" invalid={isFieldInvalid('product_type', delivery.product_type_id)}>
                                            <option></option>
                                        { this.props.productTypes.map(type => (
                                            <option key={type.id} value={type.id}> 
                                                {type.name}
                                            </option>
                                        )) }
                                    </Select>
                                </FieldContainer>
                            </Col>

                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="delivery_restriction_id">Restrição</Label>
                                    <Select onChange={this.onChangeDeliveryAttr} block name="delivery_restriction_id" invalid={isFieldInvalid('delivery_restriction', delivery.delivery_restriction_id)} >
                                        <option></option>
                                        { this.props.deliveryRestrictions.map(restriction => (
                                            <option key={restriction.id} value={restriction.id}> 
                                                {restriction.name}
                                            </option>
                                        )) }
                                    </Select>
                                </FieldContainer>
                            </Col>
                        </Row>
                        
                        <Row>
                             <Col md={6}>
                                <FieldContainer>
                                    <Label>Peso (kg)</Label>
                                    <TextField onChange={this.onChangeDeliveryAttr} type="number" name="weight" placeholder="Ex: 500" block invalid={isFieldInvalid('weight', delivery.weight)} />
                                </FieldContainer>
                            </Col>

                                <Col md={6}>
                                <FieldContainer>
                                    <Label>Altura (cm)</Label>
                                    <TextField onChange={this.onChangeDeliveryAttr} type="number" name="height" placeholder="Ex: 120" block invalid={isFieldInvalid('height', delivery.height)}  />
                                </FieldContainer>
                            </Col>

                            <Col md={6}>
                                <FieldContainer>
                                    <Label>Largura (cm)</Label>
                                        <TextField onChange={this.onChangeDeliveryAttr} type="number" name="width" placeholder="Ex: 120" block invalid={isFieldInvalid('width', delivery.width)} />
                                </FieldContainer>
                            </Col>

                            <Col md={6}>
                                <FieldContainer>
                                    <Label>Comprimento (cm)</Label>
                                    <TextField onChange={this.onChangeDeliveryAttr} type="number" name="length" placeholder="Ex: 120" block invalid={isFieldInvalid('length', delivery.length)} />
                                </FieldContainer>
                            </Col>
                        </Row>
                    </WhiteCard>

                    <Paragraph>Informações de Localização</Paragraph>
                    <WhiteCard>
                        <Row>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="zip_code">CEP</Label>
                                    <MaskField
                                        onBlur={this.loadAddressByCep} 
                                        id="zip_code" 
                                        name="zip_code" 
                                        placeholder="00000-00"  
                                        block 
                                        onChange={this.onChangeAddressAttr}
                                        invalid={isFieldInvalid('zip_code', delivery.end_address.zip_code)} 
                                        mask={[ /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}/>
                                </FieldContainer>
                            </Col>

                             <Col>
                                <FieldContainer>
                                    <Label htmlFor="street">Logradouro</Label>
                                    <TextField value={delivery.end_address.street} id="street" name="street" placeholder="Rua" block onChange={this.onChangeAddressAttr} invalid={isFieldInvalid('street', delivery.end_address.street)} />
                                </FieldContainer>
                            </Col>
                        </Row>

                         <Row>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="number">Número</Label>
                                    <TextField id="number" name="number" placeholder="000" onChange={this.onChangeAddressAttr} invalid={isFieldInvalid('number', delivery.end_address.number)} />
                                </FieldContainer>
                            </Col>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="complement">Complemento</Label>
                                    <TextField id="complement" name="complement" placeholder="Apt 00" block onChange={this.onChangeAddressAttr}/>
                                </FieldContainer>
                            </Col>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="reference">Referência</Label>
                                    <TextField id="reference" name="reference" placeholder="Próxima a"  block onChange={this.onChangeAddressAttr}/>
                                </FieldContainer>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="neighbor">Bairro</Label>
                                    <TextField value={delivery.end_address.neighbor} id="neighbor" name="neighbor" placeholder="Bairro"  block onChange={this.onChangeAddressAttr} invalid={isFieldInvalid('neighbor', delivery.end_address.neighbor)} />
                                </FieldContainer>
                            </Col>

                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="city_id">Cidade</Label>
                                    <Select value={delivery.end_address.city_id} id="city_id" name="city_id" block onChange={this.onChangeAddressAttr} invalid={isFieldInvalid('city', delivery.end_address.city_id)} >
                                        <option></option>
                                        { this.props.cities.map(city => (
                                            <option key={city.id} value={city.id}> 
                                                {city.name} - {city.state}, {city.country}  
                                            </option>
                                        )) }
                                    </Select>
                                </FieldContainer>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <FieldContainer>
                                    <Label htmlFor="start_address_id">Local de Coleta</Label>
                                    <Select id="start_address_id" name="start_address_id" block onChange={this.onChangeDeliveryAttr} invalid={isFieldInvalid('start_address', delivery.start_address_id)} >
                                    <option></option>
                                        { this.props.addresses.map(address => (
                                            <option key={address.id} value={address.id}> 
                                                {address.street}. {address.city.name} - {address.city.state}, {address.city.country}  
                                            </option>
                                        )) }
                                    </Select>
                                </FieldContainer>
                            </Col>
                        </Row>
                    </WhiteCard>

                    <Paragraph> Informações sobre o transporte </Paragraph>
                    <WhiteCard>
                        <Row>
                            <Col md={12}>
                                <Paragraph> Utilizar Transportadora </Paragraph>
                            </Col>

                            <Col md={6}>
                                <Button 
                                    block
                                    selected={delivery.transporting_method === transportingMethod.PARTICULAR} 
                                    onClick={this.selectTransportMethod.bind(this, transportingMethod.PARTICULAR)}> 
                                    Particular 
                                </Button>
                            </Col>

                            <Col md={6}>
                                <Button 
                                    block 
                                    selected={delivery.transporting_method === transportingMethod.OUTSOURCED} 
                                    onClick={this.selectTransportMethod.bind(this, transportingMethod.OUTSOURCED)}> 
                                    Terceira 
                                </Button>
                            </Col>
                        </Row>

                        {(() => {
                            if (delivery.transporting_method === transportingMethod.OUTSOURCED) {
                                return (
                                    <Row>
                                        <Col md={12}>
                                            <Paragraph>Sugestões</Paragraph>
                                        </Col>

                                        <Col md={12}>
                                            <Table>
                                                <THeaderRow>
                                                    <THeaderCel>Transportadora</THeaderCel>
                                                    <THeaderCel>Preço Estimado</THeaderCel>
                                                </THeaderRow>

                                                {this.props.shippingCompanies.map(shippingCompany => (
                                                    <TRow key={shippingCompany.id} 
                                                            selectable
                                                            onClick={this.selectShippingCompany.bind(this, shippingCompany)}
                                                            selected={delivery.shipping_company_id === shippingCompany.id}>
                                                        <TCel>{shippingCompany.social_name}</TCel>
                                                        <TCel>{formatCurrency(shippingCompany.estimated_price)}</TCel>
                                                    </TRow>
                                                ))}

                                            </Table>
                                        </Col>
                                    </Row>
                                )
                            }
                        })()}
                    </WhiteCard>

                    <SubmitContainer>
                        <ButtonPrimary onClick={this.submitDelivery}>
                            Salvar
                        </ButtonPrimary>
                    </SubmitContainer>
                </Card>
                
                <Loader loading={this.state.loading} />
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage,
    delivery: state.deliveryReducer.delivery,
    addresses: state.supplierCompanyReducer.supplierCompanyAddresses,
    cities: state.cityReducer.cities,
    productTypes: state.productTypeReducer.productTypes,
    deliveryRestrictions: state.deliveryRestrictionReducer.deliveryRestrictions,
    shippingCompanies: state.shippingCompanyReducer.shippingCompanies
})

const mapDispatchToProps = dispatch => ({
    postDelivery: (delivery, onSuccess, onError) => dispatch(postDeliveryAsync(delivery, onSuccess, onError)),
    loadSupplierCompanyAddresses: (id) => dispatch(loadSupplierCompanyAddressesAsync(id)),
    loadCities: () => dispatch(loadCitiesAsync()),
    loadProductTypes: () => dispatch(loadProductTypesAsync()),
    loadDeliveryRestrictions: () => dispatch(loadDeliveryRestrictionsAsync()),
    loadShippingCompanySuggestions: (delivery, onSuccess, onError) => dispatch(loadShippingCompaniesSuggestionsAsync(delivery, onSuccess, onError)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateDelivery)