import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getStateLabel } from '../../../deliveries/strings'
import { allStates } from '../../../deliveries/status'
import AddIcon from 'react-icons/lib/md/add'
import ImportIcon from 'react-icons/lib/fa/folder-o'

import Loader from '../../widgets/Loader'
import { Title } from '../../components/Texts'
import { FloatingActionButton, Button, ButtonPrimary } from '../../components/Buttons'
import { Select, TextField } from '../../components/Forms'
import { Card, PaginationContainer } from '../../components/Containers'
import DeliveriesTable from '../../widgets/deliveries/DeliveriesTable'
import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'
import { withFirebase } from 'react-redux-firebase';
import { getUserFromLocalStorage } from '../../../user/actions';
import { loadSupplierCompanyDeliveries } from '../../../supplierCompanies/actions';
import CsvDeliveryImporter from '../../widgets/deliveries/CsvDeliveryImporter';
import { PAGINATION_SIZE } from '../../../config/pagination';

class ListDeliveries extends Component {
    state = {
        loading: true,
        isDeliveryImporterOpen: false,
        status: '',
        locator: '',
        page: 0
    }

    constructor(props) {
        super(props)

        this.loadDeliveries = this.loadDeliveries.bind(this)
        this.updateState = this.updateState.bind(this)
        this.filter = this.filter.bind(this)
        this.openCsvDeliveryImporter = this.openCsvDeliveryImporter.bind(this)
        this.closeCsvDeliveryImporter = this.closeCsvDeliveryImporter.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }

    componentDidMount() {
        this.user = getUserFromLocalStorage()
        this.loadDeliveries()
    }

    loadDeliveries() {
        this.setState({ loading: true })
        this.props.loadDeliveries(this.user.companyId, () => this.setState({ loading: false }), this.state)
    }

    filter(evt) {
        if (evt.key && evt.key !== 'Enter')
            return

        this.updateState(evt)
        this.setState({ page: 0 })
        setTimeout(() => this.loadDeliveries(), 20)
    }

    updateState(evt) {
        const newState = this.state
        newState[evt.target.name] = evt.target.value
        this.setState(newState)
    }

    openCsvDeliveryImporter() {
        this.setState({ isDeliveryImporterOpen: true })
    }

    closeCsvDeliveryImporter() {
        this.setState({ isDeliveryImporterOpen: false })
    }

    nextPage() {
        if (this.props.deliveries.length < PAGINATION_SIZE) {
            return
        }

        this.setState({ page: this.state.page + 1 })
        setTimeout(() => this.loadDeliveries(), 20)
    }

    previousPage() {
        if (this.state.page === 0) {
            return
        }

        this.setState({ page: this.state.page - 1 })
        setTimeout(() => this.loadDeliveries(), 20)
    }

    render() {
        return (
            <div>
                <Title>Entregas da Fornecedora</Title>

                <Loader loading={this.loading} />

                <CsvDeliveryImporter open={this.state.isDeliveryImporterOpen} onExit={this.closeCsvDeliveryImporter} />

                <Row>
                    <Col>
                        <Select name='status' block onChange={this.filter} value={this.state.status}>
                            <option value="">Estado</option>
                            {allStates.map(state =>
                                <option key={state} value={state}>
                                    {getStateLabel(state)}
                                </option>
                            )}
                        </Select>
                    </Col>
                    <Col>
                        <TextField
                            name="locator"
                            value={this.state.locator}
                            placeholder="Localizador"
                            onBlur={this.filter}
                            onKeyPress={this.filter}
                            onChange={this.updateState}
                            block />
                    </Col>

                    <Col>
                        <Button block onClick={this.openCsvDeliveryImporter}><ImportIcon /> Importar em Lote</Button>
                    </Col>
                </Row>

                {
                    this.props.deliveries.length > 0 || this.state.page > 0
                        ? (
                            <div>
                                <DeliveriesTable deliveries={this.props.deliveries} />
                                <PaginationContainer>
                                    <ButtonPrimary disabled={this.state.page === 0} onClick={this.previousPage}>Página anterior</ButtonPrimary>
                                    <ButtonPrimary disabled={this.props.deliveries.length < PAGINATION_SIZE} onClick={this.nextPage}>Próxima página</ButtonPrimary>
                                </PaginationContainer>
                            </div>
                        )
                        : (<Card>Você ainda não possui nenhuma entrega! Crie uma agora :)</Card>)
                }

                <NavLink to='/fornecedora/entregas/nova'>
                    <FloatingActionButton>
                        <AddIcon style={{ margin: '0 auto' }} />
                    </FloatingActionButton>
                </NavLink>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage,
    deliveries: state.supplierCompanyReducer.deliveries,
})

const mapDispatchToProps = dispatch => ({
    loadDeliveries: (id, onSuccess, params) => dispatch(loadSupplierCompanyDeliveries(id, onSuccess, params))
})

export default connect(mapStateToProps, mapDispatchToProps)(withFirebase(ListDeliveries))