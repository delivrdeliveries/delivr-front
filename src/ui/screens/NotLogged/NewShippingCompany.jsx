import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'
import {connect} from 'react-redux'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph, Subtitle } from '../../components/Texts'
import { ButtonPrimary } from '../../components/Buttons'
import { FieldContainer, TextField, Label, SubmitContainer, MaskField, Form } from '../../components/Forms'
import TitleBack from '../../widgets/TitleBack'
import UserForm from '../../widgets/notLogged/UserForm';
import FormValidator, { isCNPJ, isEmpty, isEmail } from '../../../formValidator';
import User from '../../../user/user';
import { Redirect } from 'react-router-dom'
import { withFirebase } from 'react-redux-firebase';
import { postShippingCompanyAsync, putShippingCompanyAsync } from '../../../shippingCompanies/actions';
import Loader from '../../widgets/Loader';
import { saveUserToLocalStorage } from '../../../user/actions';

class NewShippingCompany extends Component{

    state = {
        loading: false,
        user: {},
        company: {}
    }

    constructor (props) {
        super(props)

        this.onUpdateUser = this.onUpdateUser.bind(this)
        this.companyUpdated = this.companyUpdated.bind(this)
        this.signUp = this.signUp.bind(this)

        this.validator = new FormValidator([
            {name: 'legal_name', validators: [ isEmpty ]},
            {name: 'document', validators: [ isCNPJ ]},
            {name: 'social_name', validators: [ isEmpty ]},
            {name: 'phone', validators: [ isEmpty ]},
            {name: 'email', validators: [ isEmail ]},
        ])
    }

    onUpdateUser (user, isUserValid) {
        user.isValid = isUserValid
        this.setState({user})
    }

    companyUpdated(event) {
        var {name, value} = event.target
        const {company} = this.state

        name = name.replace('company_', '')
        company[name] = value
        this.setState({company: this.formatCompany(company)})
    }

    formatCompany (company) {
        if (company.document) {
            company.document = company.document.replace(/[^\d]/g, '')
        }

        return company
    }

    async signUp (event) {
        event.preventDefault()

        const {user, company} = this.state

        if (!user.isValid || this.validator.isFormInvalid()) {
            alert("Por favor corrija os erros do formulário")
            return false
        }

        company.contact = {
            email: company.email,
            contact: company.contact
        }

        try {
            this.setState({loading: true})
            
            company.user = '__TEMP__'
            const response = await this.createCompany(company)
            company.id = response.id

            await this.props.firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
            
            const firebaseUser = this.props.firebase.auth().currentUser

            company.user = firebaseUser.uid
            await this.updateCompany(company)

            await firebaseUser.updateProfile({
                displayName: user.name,
            })

            this.props.saveUserToLocalStorage(new User({
                firebaseId: firebaseUser.uid,
                companyId: company.id, 
                companyType: 'ShippingCompany'
            }))

            this.setState({redirect: true})
        } catch (error) {
            if (company.id) {
                company.document = '__ERROR__'
                await this.updateCompany(company)
            }

            console.error(error)
            this.setState({loading: false})
        }
        
        return false
    }

    createCompany (company) {
        return new Promise((accept, reject) => {
            this.props.postShippingCompany(company, accept, reject)
        })
    }

    updateCompany (company) {
        return new Promise((accept, reject) => {
            this.props.putShippingCompany(company.id, company, accept, reject)
        })
    }


    render () {
        const {isFieldInvalid} = this.validator
        const {company} = this.state

        if (this.state.redirect) {
            return <Redirect to='/home' />
        }

        return (
            <div>
                
                <Loader loading={this.state.loading} />

                <Title>Cadastro <TitleBack /></Title>

                <Subtitle>Basta cadastrar essas opções para começar a utilizar o sistema! Posteriormente ajuderemos você com o que for necessário :)</Subtitle>
                
                <Form onSubmit={this.signUp}>
                    <Card>
                        <Paragraph> Informações Pessoais </Paragraph>
                        <UserForm onUpdateUser={this.onUpdateUser} />

                        <Paragraph> Informações da Básicas Empresa </Paragraph>
                        <WhiteCard>
                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="company_document">CNPJ</Label>
                                            <MaskField id="company_document" 
                                                        name="company_document"
                                                        invalid={isFieldInvalid('document', company.document)} 
                                                        block 
                                                        onChange={this.companyUpdated}
                                                        mask={[/\d/,/\d/,'.',/\d/,/\d/,/\d/,'.',/\d/,/\d/,/\d/,'/',/\d/,/\d/,/\d/,/\d/,'-',/\d/,/\d/,]} />
                                        </FieldContainer>
                                    </Col>

                                        <Col>
                                        <FieldContainer>
                                            <Label htmlFor="company_legal_name">Razão Social</Label>
                                            <TextField id="company_legal_name" 
                                                        name="company_legal_name"
                                                        invalid={isFieldInvalid('legal_name', company.legal_name)} 
                                                        block 
                                                        onChange={this.companyUpdated}/>
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="company_social_name">Nome Fantasia</Label>
                                            <TextField id="company_social_name" 
                                                        name="company_social_name"
                                                        invalid={isFieldInvalid('social_name', company.social_name)} 
                                                        block  
                                                        onChange={this.companyUpdated}/>
                                        </FieldContainer>
                                    </Col>
                                </Row>

                            <Row>
                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="company_email">E-mail da Empresa</Label>
                                        <TextField id="company_email" 
                                                    name="company_email" 
                                                    invalid={isFieldInvalid('email', company.email)} 
                                                    block  
                                                    onChange={this.companyUpdated}/>
                                    </FieldContainer>
                                </Col>

                                    <Col>
                                    <FieldContainer>
                                        <Label htmlFor="company_phone">Telefone Comercial</Label>
                                        <MaskField id="company_phone" 
                                                    name="company_phone" 
                                                    invalid={isFieldInvalid('phone', company.phone)} 
                                                    block  
                                                    onChange={this.companyUpdated}
                                                    mask={['(',/\d/,/\d/,')',' ',/\d/,/\d/,/\d/,/\d/,/\d/,'-',/\d/,/\d/,/\d/,/\d/,]} />
                                    </FieldContainer>
                                </Col>
                            </Row>
                        </WhiteCard>
                    </Card>

                    <SubmitContainer>
                        <ButtonPrimary> Salvar </ButtonPrimary>
                    </SubmitContainer>
                </Form>
            </div>
        )
    }

}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
    postShippingCompany: (company, onSuccess, onError) => dispatch(postShippingCompanyAsync(company, onSuccess, onError)),
    putShippingCompany: (id, company, onSuccess, onError) => dispatch(putShippingCompanyAsync(id, company, onSuccess, onError)),
    saveUserToLocalStorage: (user) => dispatch(saveUserToLocalStorage(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(withFirebase(NewShippingCompany))