import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Login from './Login'
import NewSupplierCompany from './NewSupplierCompany'
import NewShippingCompany from './NewShippingCompany'

export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/' component={Login} />
                <Route exact path='/fornecedora/criar' component={NewSupplierCompany} />
                <Route exact path='/transportadora/criar' component={NewShippingCompany} />
            </div>
        )
    }
}