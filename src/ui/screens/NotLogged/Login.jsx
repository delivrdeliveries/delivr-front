import React, {Component} from 'react'

import { Row, Col } from 'react-grid-system'

import { LogoIcon } from '../../components/Icons'
import { Paragraph, Link } from '../../components/Texts'
import Loader from '../../widgets/Loader'
import { ButtonPrimary } from '../../components/Buttons'
import { Card, WhiteCard } from '../../components/Containers'
import { FieldContainer, Label, TextField, SubmitContainer } from '../../components/Forms'
import { withFirebase } from 'react-redux-firebase'
import { Redirect } from 'react-router-dom'
import { saveUserToLocalStorage } from '../../../user/actions';
import {connect} from 'react-redux'
import { loadCompanyByUserAsync } from '../../../companies/actions';
import User from '../../../user/user';

class Login extends Component {

    state = {
        loading: false,
        email: '',
        password: '',
        userLogged: false,
    }

    constructor (props) {
        super(props)
        
        this.login = this.login.bind(this)
        this.updateInput = this.updateInput.bind(this)
    }

    componentDidMount () {
        const firebase = this.props.firebase

        firebase.auth().onAuthStateChanged(async user => {
            if (user) {
                const company = await this.getCompanyByUser(user)
                this.props.saveUserToLocalStorage(new User({
                    firebaseId: user.uid,
                    companyId: company.id, 
                    companyType: company.type
                }))
                this.setState({
                    userLogged: true
                })
            }
        }, () => this.setState({ userLogged: false })
        )
    }

    updateInput (event) {
         const { name, value } = event.target

        this.setState({
            [name]: value
        })
    }

    async login (event) {
        event.preventDefault()

        this.setState({loading: true})
        const firebase = this.props.firebase
        const { email, password } = this.state

        try {
            await firebase.login({ email, password })
        } catch (error) {
            switch (error.code) {
                case 'auth/invalid-email':
                case 'auth/wrong-password':
                case 'auth/user-not-found':
                    window.alert('E-mail ou senha inválidos')
                    break;
                default:
                    console.error(error)
            }
        }

        this.setState({loading: false})

        return false
    }

    getCompanyByUser (user) {
        return new Promise((accept) => {
            this.props.loadCompanyByUser(user.uid, accept)
        })
    }

    render () {
        if (this.state.userLogged) {
            return <Redirect to="/home" />
        }

        return (
            <Row>
                <Loader loading={this.state.loading} />
                <Col md={6} offset={{md: 3}} style={{textAlign: 'center'}}>
                    <Card style={{textAlign: 'center'}}>
                        <WhiteCard>

                            <LogoIcon style={{width: '200px'}}/>

                            <form onSubmit={this.login}>
                                <FieldContainer>
                                    <Label htmlFor="email"> E-mail </Label>
                                    <TextField type="email" block name="email" id="email" onChange={this.updateInput} />
                                </FieldContainer>

                                <FieldContainer>
                                    <Label htmlFor="password"> Senha </Label>
                                    <TextField type="password" block name="password" id="password" onChange={this.updateInput} />
                                </FieldContainer>

                                <SubmitContainer>
                                    <ButtonPrimary> Entrar </ButtonPrimary>
                                </SubmitContainer>
                            </form>

                        </WhiteCard>

                    </Card>

                    <Paragraph style={{textAlign: 'center'}}>
                        <Paragraph>Ainda não é Cadastrado? Se cadastre <Link to='/fornecedora/criar'>como uma fornecedora</Link> ou <Link to='/transportadora/criar'> como uma transportadora</Link></Paragraph>
                    </Paragraph>
                </Col>
            </Row>
        )
    }

}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
    saveUserToLocalStorage: (a, b, c) => dispatch(saveUserToLocalStorage(a, b, c)),
    loadCompanyByUser: (user, onSuccess) => dispatch(loadCompanyByUserAsync(user, onSuccess))
})

export default withFirebase(connect(mapStateToProps, mapDispatchToProps)(Login))