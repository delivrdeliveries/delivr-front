import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Row, Col } from 'react-grid-system'
import Loader from '../../../widgets/Loader';
import { Title, Paragraph, Link } from '../../../components/Texts';
import TitleBack from '../../../widgets/TitleBack';
import SettingsMenu from '../../../widgets/settings/SettingsMenu';
import { WhiteCard, Card } from '../../../components/Containers';
import { Form, FieldContainer, Label, MaskField, TextField, Select, SubmitContainer } from '../../../components/Forms';
import { ButtonPrimary } from '../../../components/Buttons';
import { getAddressByCep } from '../../../../geolocation';
import { loadCitiesAsync } from '../../../../cities/actions';
import FormValidator, { isCEP, isEmpty } from '../../../../formValidator';
import { loadSupplierCompanyAddressAsync, editSupplierCompanyAddress, createtSupplierCompanyAddress } from '../../../../supplierCompanies/actions';
import { Redirect } from 'react-router-dom'
import { getUserFromLocalStorage } from '../../../../user/actions';

class SupplierAddressForm extends Component {

    state = {
        loading: true,
        addressSaved: false,
        address: {}
    }

    constructor (props) {
        super(props)

        this.loadAddressByCep = this.loadAddressByCep.bind(this)
        this.updateField = this.updateField.bind(this)
        this.submitForm = this.submitForm.bind(this)

        this.validator = new FormValidator([
            {name: 'zip_code', validators: [ isCEP ]},
            {name: 'street', validators: [ isEmpty ]},
            {name: 'number', validators: [ isEmpty ]},
            {name: 'neighbor', validators: [ isEmpty ]},
            {name: 'city_id', validators: [ isEmpty ]},
        ])
    }

    isEditMode () {
        return this.getAddressId()
     }
 
     getAddressId () {
         return this.props.match.params.id
     }

    componentDidMount() {
        var citiesLoaded = false
        var addressLoaded = true

        const onSuccess = () => {
            if (citiesLoaded && addressLoaded) {
                this.setState({loading: false})
            }
        }

        this.props.loadCities(() => { citiesLoaded = true; onSuccess() })

        if (this.isEditMode()) {
            addressLoaded = false
            const user = getUserFromLocalStorage()
            this.props.loadAddress(user.companyId, this.getAddressId(), (address) => { 
                this.setState({address})
                addressLoaded = true
                onSuccess()
             })
        }
    }

    updateField(event) {
        const {name, value} = event.target
        const {address} = this.state

        address[name] = value

        this.setState({address})
    }

    async loadAddressByCep() {
        const {address} = this.state

        if (!address.zip_code || address.zip_code.length !== 9) {
            return
        }

        try {
            this.setState({loading: true})
            const remoteAddress = await getAddressByCep(this.state.address.zip_code)

            address.street = remoteAddress.logradouro
            address.neighbor = remoteAddress.bairro
            address.city_id = remoteAddress.ibge

            this.setState({loading: false, address})
        } catch (error) {
            alert('CEP não econtrado.')
            this.setState({loading: false})
        }
    }

    submitForm (event) {
        event.preventDefault()

        if (this.validator.isFormInvalid()) {
            alert('Por favor corrija os campos inválidos')
            return false
        }

        const {address} = this.state
        address.zip_code = address.zip_code.replace(/[^\d]/g, '')
        this.saveAddress(address, this.props.user.companyId)

        return false
    }

    saveAddress (address, companyId) {
        this.setState({loading: true})

        const onSuccess = () => this.setState({ addressSaved: true })

        if (address.id) {
            this.props.editAddress(companyId, address.id, address, onSuccess)
        } else {
            this.props.createAddress(companyId, address, onSuccess)
        }
    }

    render () {
        if (this.state.addressSaved) {
            return <Redirect to="/configuracoes/locais-retirada" />        
        }

        const {address} = this.state
        const {isFieldInvalid} = this.validator
        return (
            <div>
                <Loader loading={this.state.loading}/>

                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='SupplierAddresses' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph>Adicionar Local de Retirada</Paragraph>
                            <WhiteCard>
                                
                                <Form onSubmit={this.submitForm}>
                                    <Row>
                                        <Col md={12}>
                                            <FieldContainer>
                                                <Label htmlFor="zip_code">CEP</Label>
                                                <MaskField
                                                    value={address.zip_code}
                                                    onBlur={this.loadAddressByCep} 
                                                    id="zip_code" 
                                                    name="zip_code" 
                                                    placeholder="00000-00"  
                                                    block 
                                                    onChange={this.updateField}
                                                    mask={[ /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/]}
                                                    invalid={isFieldInvalid('zip_code', address.zip_code)}/>
                                            </FieldContainer>
                                        </Col>

                                        <Col md={12}>
                                            <FieldContainer>
                                                <Label htmlFor="street">Logradouro</Label>
                                                <TextField value={address.street} 
                                                            id="street" 
                                                            name="street" 
                                                            placeholder="Rua" 
                                                            block 
                                                            onChange={this.updateField}
                                                            invalid={isFieldInvalid('street', address.street)}/>
                                            </FieldContainer>
                                        </Col>
                                    
                                        <Col md={6}>
                                            <FieldContainer>
                                                <Label htmlFor="number">Número</Label>
                                                <TextField id="number" 
                                                            value={address.number}
                                                            name="number" 
                                                            placeholder="000" 
                                                            onChange={this.updateField} 
                                                            block 
                                                            invalid={isFieldInvalid('number', address.number)}/>
                                            </FieldContainer>
                                        </Col>

                                        <Col md={6}>
                                            <FieldContainer>
                                                <Label htmlFor="complement">Complemento</Label>
                                                <TextField id="complement" 
                                                            value={address.complement}
                                                            name="complement" 
                                                            placeholder="Apt 00" 
                                                            block 
                                                            onChange={this.updateField}/>
                                            </FieldContainer>
                                        </Col>

                                        <Col md={12}>
                                            <FieldContainer>
                                                <Label htmlFor="reference">Referência</Label>
                                                <TextField id="reference" 
                                                            value={address.reference}
                                                            name="reference" 
                                                            placeholder="Próxima a" 
                                                             block 
                                                             onChange={this.updateField}/>
                                            </FieldContainer>
                                        </Col>
                                    
                                        <Col md={12}>
                                            <FieldContainer>
                                                <Label htmlFor="neighbor">Bairro</Label>
                                                <TextField value={address.neighbor} 
                                                    id="neighbor" 
                                                    name="neighbor" 
                                                    placeholder="Bairro"  
                                                    block 
                                                    onChange={this.updateField}
                                                    invalid={isFieldInvalid('neighbor', address.neighbor)}/>
                                            </FieldContainer>
                                        </Col>

                                        <Col md={12}>
                                            <FieldContainer>
                                                <Label htmlFor="city_id">Cidade</Label>
                                                <Select value={address.city_id} id="city_id" name="city_id" block onChange={this.updateField} invalid={isFieldInvalid('city_id', address.city_id)} >
                                                    <option></option>
                                                    { this.props.cities.map(city => (
                                                        <option key={city.id} value={city.id}> 
                                                            {city.name} - {city.state}, {city.country}  
                                                        </option>
                                                    )) }
                                                </Select>
                                            </FieldContainer>
                                        </Col>

                                        <Col md={12}>
                                            <SubmitContainer>
                                                <Link to="/configuracoes/locais-retirada/">Voltar</Link>

                                                <ButtonPrimary>Salvar</ButtonPrimary>
                                            </SubmitContainer>
                                        </Col>
                                    </Row>
                                </Form>

                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    cities: state.cityReducer.cities,
    user: state.userReducer.userLocalStorage
})

const mapDispatchToProps = dispatch => ({
    loadCities: (onSuccess) => dispatch(loadCitiesAsync(onSuccess)),
    loadAddress: (companyId, addressId, onSuccess) => dispatch(loadSupplierCompanyAddressAsync(companyId, addressId, onSuccess)),
    editAddress: (companyId, addressId, address, onSuccess) => dispatch(editSupplierCompanyAddress(companyId, addressId,  address, onSuccess)),
    createAddress: (companyId, address, onSuccess) => dispatch(createtSupplierCompanyAddress(companyId, address, onSuccess))
})

export default connect(mapStateToProps, mapDispatchToProps)(SupplierAddressForm)