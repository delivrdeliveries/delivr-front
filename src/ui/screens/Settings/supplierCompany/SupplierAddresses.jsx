import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'

import { Card, WhiteCard} from '../../../components/Containers'
import { Title, Paragraph, TinyText } from '../../../components/Texts'
import { Table, TRow, TCel, THeaderRow, THeaderCel } from '../../../components/Tables'
import { Button } from '../../../components/Buttons'
import SettingsMenu from '../../../widgets/settings/SettingsMenu'
import TitleBack from '../../../widgets/TitleBack'
import { loadSupplierCompanyAddressesAsync } from '../../../../supplierCompanies/actions';
import { getUserFromLocalStorage } from '../../../../user/actions';
import Loader from '../../../widgets/Loader';
import formatAddress from '../../../../addresses/formatAddress';

class SupplierAddresses extends Component{

    state = {
        loading: true
    }

    componentDidMount () {
        const user = getUserFromLocalStorage()
        this.props.loadAddresses(user.companyId, () => this.setState({loading: false}))
    }

    render () {
        return (
            <div>
                
                <Loader loading={this.state.loading}/>

                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='SupplierAddresses' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph>Locais de Retirada</Paragraph>
                            <WhiteCard>
                                <Row>
                                    <Col md={4} offset={{md: 8}}>
                                        <NavLink to='/configuracoes/locais-retirada/adicionar'>
                                            <Button block>Adicionar Local</Button>
                                        </NavLink>
                                    </Col>
                                </Row>

                                {
                                    this.props.addresses.length === 0 
                                    ? <Paragraph> Você ainda não possui locais de retirada  </Paragraph>
                                    : (
                                        <Table>
                                            <THeaderRow>
                                                <THeaderCel> # </THeaderCel>
                                                <THeaderCel> Endereço </THeaderCel>
                                                <THeaderCel> Ação </THeaderCel>
                                            </THeaderRow>

                                            {
                                                this.props.addresses.map(address => (
                                                    <TRow key={address.id}>
                                                        <TCel>
                                                            <TinyText>{address.id}</TinyText>
                                                        </TCel>
                                                        <TCel>
                                                            <TinyText>{formatAddress(address)}</TinyText>
                                                        </TCel>
                                                        <TCel>
                                                            <NavLink to={'/configuracoes/locais-retirada/editar/' + address.id}>
                                                                <TinyText>Editar</TinyText>
                                                            </NavLink>
                                                        </TCel>
                                                    </TRow>
                                                ))
                                            }
                                            
                                        </Table>
                                    )
                                }

                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    addresses: state.supplierCompanyReducer.supplierCompanyAddresses,
})

const mapDispatchToProps = dispatch => ({
    loadAddresses: (shippingCompany, onSuccess) => dispatch(loadSupplierCompanyAddressesAsync(shippingCompany, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SupplierAddresses)