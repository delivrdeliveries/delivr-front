import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard } from '../../../components/Containers'
import { Title, Paragraph, TinyText, Link } from '../../../components/Texts'
import SettingsMenu from '../../../widgets/settings/SettingsMenu'
import TitleBack from '../../../widgets/TitleBack'
import { Table, TRow, TCel, THeaderCel, THeaderRow } from '../../../components/Tables'
import { Button } from '../../../components/Buttons'
import { getUserFromLocalStorage } from '../../../../user/actions';
import { getInvoices, getInvoiceDeliveries } from '../../../../payments/endpoints';
import Loader from '../../../widgets/Loader';
import { formatPaymentsCurrency } from '../../../../currency/formatCurrency';
import { formatCompetency } from '../../../../date';
import StatusBadge from '../../../widgets/payments/StatusBadge';
import Modal from '../../../components/Modal';
import { generateLocator } from '../../../../deliveries/strings';

export default class Invoices extends Component {

    state = {
        loading: true,
        invoices: [],
        deliveries: [],
        currentInvoice: undefined,
        showDeliveriesModal: false
    }

    constructor(props) {
        super(props)

        const { companyId } = getUserFromLocalStorage()
        this.companyId = companyId

        this.closeDeliveriesModal = this.closeDeliveriesModal.bind(this)
        this.openDeliveriesModal = this.openDeliveriesModal.bind(this)
        this.getDeliveriesModal = this.getDeliveriesModal.bind(this)
    }

    componentDidMount() {
        this.loadInvoices()
    }

    async loadInvoices() {
        this.setState({ loading: true })
        const invoices = await getInvoices(this.companyId)
        this.setState({ invoices, loading: false })
    }

    getDeliveriesModal() {
        const { currentInvoice: invoice, deliveries } = this.state
        let totalPrice = 0
        let totalFee = 0

        return (
            <Modal title={`Entregas do pagamento de ${formatCompetency(invoice.competency)}`} onExit={this.closeDeliveriesModal}>

                <Paragraph>Quantidade de entregas: {deliveries.length}</Paragraph>

                <Table>
                    <THeaderRow>
                        <THeaderCel>Entrega</THeaderCel>
                        <THeaderCel>Valor</THeaderCel>
                        <THeaderCel>Taxa Delivr</THeaderCel>
                        <THeaderCel>Valor Final</THeaderCel>
                    </THeaderRow>

                    {deliveries.map((delivery, idx) => {
                        totalPrice += delivery.price
                        totalFee += delivery.fee

                        const locator = generateLocator(delivery.delivery_id)
                        return (
                            <TRow key={idx}>
                                <TCel>
                                    <Link to={`/entrega/${locator}`} target="_blank">
                                        <TinyText>#{locator}</TinyText>
                                    </Link>
                                </TCel>

                                <TCel>
                                    <TinyText>{formatPaymentsCurrency(delivery.price)}</TinyText>
                                </TCel>

                                <TCel>
                                    <TinyText>{formatPaymentsCurrency(delivery.fee)}</TinyText>
                                </TCel>

                                <TCel>
                                    <TinyText>{formatPaymentsCurrency(delivery.fee + delivery.price)}</TinyText>
                                </TCel>
                            </TRow>
                        )
                    })}
                    <THeaderRow>
                        <THeaderCel>Total: </THeaderCel>
                        <THeaderCel>{formatPaymentsCurrency(totalPrice)}</THeaderCel>
                        <THeaderCel>{formatPaymentsCurrency(totalFee)}</THeaderCel>
                        <THeaderCel>{formatPaymentsCurrency(invoice.total_price)}</THeaderCel>
                    </THeaderRow>
                </Table>

            </Modal>
        )
    }

    async openDeliveriesModal(currentInvoice) {
        this.setState({ loading: true })
        const deliveries = await getInvoiceDeliveries(this.companyId, currentInvoice.competency)
        this.setState({ currentInvoice, deliveries, showDeliveriesModal: true, loading: false })
    }

    closeDeliveriesModal() {
        this.setState({ showDeliveriesModal: false })
    }

    render() {
        return (
            <div>

                {this.state.showDeliveriesModal ? this.getDeliveriesModal() : null}

                <Loader loading={this.state.loading} />

                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='Invoices' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph> Faturas </Paragraph>
                            <WhiteCard>
                                <Table>
                                    {this.state.invoices.map((invoice, idx) => (
                                        <TRow key={idx}>
                                            <TCel>
                                                <StatusBadge status={invoice.status} />
                                            </TCel>

                                            <TCel>
                                                <TinyText>
                                                    {formatCompetency(invoice.competency)}
                                                </TinyText>
                                            </TCel>

                                            <TCel>
                                                <TinyText>
                                                    {formatPaymentsCurrency(invoice.total_price)}
                                                </TinyText>
                                            </TCel>

                                            <TCel>
                                                <Button block
                                                    tiny
                                                    onClick={this.openDeliveriesModal.bind(this, invoice)}>
                                                    Ver entregas
                                                </Button>
                                            </TCel>

                                            <TCel>
                                                {
                                                    invoice.status === 'awaiting_payment' ?
                                                        (<Button block tiny> Ver boleto </Button>) :
                                                        null
                                                }
                                            </TCel>
                                        </TRow>
                                    ))}
                                </Table>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}