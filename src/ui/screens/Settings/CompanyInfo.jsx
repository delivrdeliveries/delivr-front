import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import { ButtonPrimary } from '../../components/Buttons'
import { FieldContainer, TextField, Label, SubmitContainer } from '../../components/Forms'
import SettingsMenu from '../../widgets/settings/SettingsMenu'
import TitleBack from '../../widgets/TitleBack'

export default class CompanyInfo extends Component{

    render () {
        return (
            <div>
                
                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='CompanyInfo' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph> Informações da Empresa </Paragraph>
                            <WhiteCard>
                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="document">CNPJ</Label>
                                            <TextField id="document" disabled block value="89.625.288/0001-01" />
                                        </FieldContainer>
                                    </Col>

                                     <Col>
                                        <FieldContainer>
                                            <Label htmlFor="name">Razão Social</Label>
                                            <TextField id="name" block value="Produtos Eletrônicos LTDA" />
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="fantasyName">Nome Fantasia</Label>
                                            <TextField id="fantasyName" block value="Produtos do Rômulo" />
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <SubmitContainer>
                                    <ButtonPrimary> Salvar </ButtonPrimary>
                                </SubmitContainer>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}