import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Invoices from './supplierCompany/Invoices'
import Settings from './Settings'
import CompanyInfo from './CompanyInfo'
import PersonalInfo from './PersonalInfo'
import UpdatePassword from './UpdatePassword'
import SupplierAddresses from './supplierCompany/SupplierAddresses'
import DeliveryProfiles from './shippingCompany/DeliveryProfiles'
import DeliveryProfileForm from './shippingCompany/DeliveryProfileForm'
import SupplierAddressForm from './supplierCompany/SupplierAddressForm';
import Receipts from './shippingCompany/Receipts';

export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/configuracoes' component={Settings} />
                <Route exact path='/configuracoes/faturas' component={Invoices} />
                <Route exact path='/configuracoes/pagamentos' component={Receipts} />
                <Route exact path='/configuracoes/alterar-senha' component={UpdatePassword} />
                <Route exact path='/configuracoes/informacoes-empresa' component={CompanyInfo} />
                <Route exact path='/configuracoes/informacoes-pessoais' component={PersonalInfo} />

                <Route exact path='/configuracoes/locais-retirada' component={SupplierAddresses} />
                <Route exact path='/configuracoes/locais-retirada/adicionar' component={SupplierAddressForm} />
                <Route exact path='/configuracoes/locais-retirada/editar/:id' component={SupplierAddressForm} />

                <Route exact path='/configuracoes/perfis-de-entrega' component={DeliveryProfiles} />
                <Route exact path='/configuracoes/perfis-de-entrega/adicionar' component={DeliveryProfileForm} />
                <Route exact path='/configuracoes/perfis-de-entrega/editar/:id' component={DeliveryProfileForm} />
            </div>
        )
    }
}