import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import { ButtonPrimary } from '../../components/Buttons'
import { FieldContainer, TextField, Label, SubmitContainer } from '../../components/Forms'
import SettingsMenu from '../../widgets/settings/SettingsMenu'
import TitleBack from '../../widgets/TitleBack'

export default class PersonalInfo extends Component{

    render () {
        return (
            <div>
                
                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='PersonalInfo' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph> Informações Pessoais </Paragraph>
                            <WhiteCard>
                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="document">CPF</Label>
                                            <TextField id="document" disabled block value="089.202.699.55" />
                                        </FieldContainer>
                                    </Col>

                                     <Col>
                                        <FieldContainer>
                                            <Label htmlFor="name">Nome</Label>
                                            <TextField id="name" block value="Rômulo Farias" />
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="email">E-mail</Label>
                                            <TextField id="email" block value="romulodefarias@gmail.com" />
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <SubmitContainer>
                                    <ButtonPrimary> Salvar </ButtonPrimary>
                                </SubmitContainer>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}