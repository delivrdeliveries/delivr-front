import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import SettingsMenu from '../../widgets/settings/SettingsMenu'
import TitleBack from '../../widgets/TitleBack'

export default class Settings extends Component{

    render () {
        return (
            <div>
                
                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu />
                    </Col>

                    <Col>
                        <Card>
                            <WhiteCard>
                                <Paragraph>Selecione uma Configuração</Paragraph>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}