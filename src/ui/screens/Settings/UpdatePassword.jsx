import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import { ButtonPrimary } from '../../components/Buttons'
import { FieldContainer, TextField, Label, SubmitContainer } from '../../components/Forms'
import SettingsMenu from '../../widgets/settings/SettingsMenu'
import TitleBack from '../../widgets/TitleBack'

export default class PersonalInfo extends Component{

    render () {
        return (
            <div>
                
                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='UpdatePassword' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph> Alterar Senha </Paragraph>
                            <WhiteCard>
                                <Row>
                                    <Col>
                                        <FieldContainer>
                                            <Label htmlFor="actualPassword">Senha Atual</Label>
                                            <TextField id="actualPassword" type="password" block />
                                        </FieldContainer>

                                         <FieldContainer>
                                            <Label htmlFor="newPassword">Nova Senha</Label>
                                            <TextField id="newPassword" type="password" block />
                                        </FieldContainer>

                                         <FieldContainer>
                                            <Label htmlFor="confirmNewPassword">Confirmar Nova Senha</Label>
                                            <TextField id="confirmNewPassword" type="password" block />
                                        </FieldContainer>
                                    </Col>
                                </Row>

                                <SubmitContainer>
                                    <ButtonPrimary> Salvar </ButtonPrimary>
                                </SubmitContainer>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}