import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard } from '../../../components/Containers'
import { Title, Paragraph, TinyText, Link } from '../../../components/Texts'
import SettingsMenu from '../../../widgets/settings/SettingsMenu'
import TitleBack from '../../../widgets/TitleBack'
import { Table, TRow, TCel, THeaderCel, THeaderRow } from '../../../components/Tables'
import { Button } from '../../../components/Buttons'
import { getUserFromLocalStorage } from '../../../../user/actions';
import { getReceipts, getReceiptDeliveries } from '../../../../payments/endpoints';
import Loader from '../../../widgets/Loader';
import { formatPaymentsCurrency } from '../../../../currency/formatCurrency';
import { formatCompetency } from '../../../../date';
import StatusBadge from '../../../widgets/payments/StatusBadge';
import Modal from '../../../components/Modal';
import { generateLocator } from '../../../../deliveries/strings';

export default class Receipts extends Component {

    state = {
        loading: true,
        receipts: [],
        deliveries: [],
        currentReceipt: undefined,
        showDeliveriesModal: false
    }

    constructor(props) {
        super(props)

        const { companyId } = getUserFromLocalStorage()
        this.companyId = companyId

        this.closeDeliveriesModal = this.closeDeliveriesModal.bind(this)
        this.openDeliveriesModal = this.openDeliveriesModal.bind(this)
        this.getDeliveriesModal = this.getDeliveriesModal.bind(this)
    }

    componentDidMount() {
        this.loadReceipts()
    }

    async loadReceipts() {
        this.setState({ loading: true })
        const receipts = await getReceipts(this.companyId)
        this.setState({ receipts, loading: false })
    }

    getDeliveriesModal() {
        const { currentReceipt: receipt, deliveries } = this.state
        return (
            <Modal title={`Entregas da fatura de ${formatCompetency(receipt.competency)}`} onExit={this.closeDeliveriesModal}>

                <Paragraph>Quantidade de entregas: {deliveries.length}</Paragraph>

                <Table>
                    <THeaderRow>
                        <THeaderCel>Entrega</THeaderCel>
                        <THeaderCel>Valor</THeaderCel>
                    </THeaderRow>

                    {deliveries.map((delivery, idx) => {
                        const locator = generateLocator(delivery.delivery_id)
                        return (
                            <TRow key={idx}>
                                <TCel>
                                    <Link to={`/entrega/${locator}`} target="_blank">
                                        <TinyText>#{locator}</TinyText>
                                    </Link>
                                </TCel>

                                <TCel>
                                    <TinyText>{formatPaymentsCurrency(delivery.price)}</TinyText>
                                </TCel>
                            </TRow>
                        )
                    })}
                    <THeaderRow>
                        <THeaderCel>Total: </THeaderCel>
                        <THeaderCel>{formatPaymentsCurrency(receipt.total_price)}</THeaderCel>
                    </THeaderRow>
                </Table>

            </Modal>
        )
    }

    async openDeliveriesModal(currentReceipt) {
        this.setState({ loading: true })
        const deliveries = await getReceiptDeliveries(this.companyId, currentReceipt.competency)
        this.setState({ currentReceipt, deliveries, showDeliveriesModal: true, loading: false })
    }

    closeDeliveriesModal() {
        this.setState({ showDeliveriesModal: false })
    }

    render() {
        return (
            <div>

                {this.state.showDeliveriesModal ? this.getDeliveriesModal() : null}

                <Loader loading={this.state.loading} />

                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='Receipts' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph> Pagamentos </Paragraph>
                            <WhiteCard>
                                <Table>
                                    {this.state.receipts.map((receipt, idx) => (
                                        <TRow key={idx}>
                                            <TCel>
                                                <StatusBadge status={receipt.status} />
                                            </TCel>

                                            <TCel>
                                                <TinyText>
                                                    {formatCompetency(receipt.competency)}
                                                </TinyText>
                                            </TCel>

                                            <TCel>
                                                <TinyText>
                                                    {formatPaymentsCurrency(receipt.total_price)}
                                                </TinyText>
                                            </TCel>

                                            <TCel>
                                                <Button block
                                                    tiny
                                                    onClick={this.openDeliveriesModal.bind(this, receipt)}>
                                                    Ver entregas
                                                </Button>
                                            </TCel>
                                        </TRow>
                                    ))}
                                </Table>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}