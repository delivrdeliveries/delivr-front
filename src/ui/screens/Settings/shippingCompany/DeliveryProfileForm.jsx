import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Row, Col } from 'react-grid-system'
import { Redirect } from 'react-router-dom'

import { loadCitiesAsync } from '../../../../cities/actions'
import { loadProductTypesAsync } from '../../../../productTypes/actions'
import { loadDeliveryRestrictionsAsync } from '../../../../deliveryRestrictions/actions'
import { updateDeliveryProfileAsync, createDeliveryProfileAsync, loadDeliveryProfileAsync } from '../../../../deliveryProfiles/actions'

import { Card, WhiteCard } from '../../../components/Containers'
import { Title, Paragraph, Link } from '../../../components/Texts'
import SettingsMenu from '../../../widgets/settings/SettingsMenu'
import TitleBack from '../../../widgets/TitleBack'
import Loader from '../../../widgets/Loader'
import { Form, TextField, FieldContainer, Label, Select, CurrencyField, SubmitContainer } from '../../../components/Forms'
import { ButtonPrimary } from '../../../components/Buttons'
import { getUserFromLocalStorage } from '../../../../user/actions';
import FormValidator, { isEmpty } from '../../../../formValidator';


class DeliveryProfileForm extends Component{

    state = {
        loadingDependencies: true,
        profile: {
            active: '1',
            estimated_price_per_km: 0
        }
    }

    constructor (props) {
        super(props)

        this.saveDeliveryProfile = this.saveDeliveryProfile.bind(this)
        this.valueUpdated = this.valueUpdated.bind(this)
        this.maskedValueUpdated = this.maskedValueUpdated.bind(this)
        this.isEditMode = this.isEditMode.bind(this)
        this.getProfileId = this.getProfileId.bind(this)
        this.formToProfile = this.formToProfile.bind(this)
        this.profileToForm = this.profileToForm.bind(this)

        this.validator = new FormValidator([
            {name: 'name', validators: [isEmpty]},
            {name: 'estimatedPricePerKm', validators: [isEmpty]},
            {name: 'active', validators: [isEmpty]},
            {name: 'productType', validators: [isEmpty]},
            {name: 'deliveryRestriction', validators: [isEmpty]},
            {name: 'startCity', validators: [isEmpty]},
            {name: 'endCity', validators: [isEmpty]},
            {name: 'maxWeight', validators: [isEmpty]},
            {name: 'maxLength', validators: [isEmpty]},
            {name: 'maxHeight', validators: [isEmpty]},
            {name: 'maxWidth', validators: [isEmpty]},
        ])
    }

    isEditMode () {
       return this.getProfileId()
    }

    getProfileId () {
        return this.props.match.params.id
    }

    componentDidMount () {
        const didDependenciesLoad = () => {
            const { cities, deliveryRestrictions, productTypes, deliveryProfile } = this.props

            if (cities.length 
                && deliveryRestrictions.length 
                && productTypes.length 
                && (! this.isEditMode() || deliveryProfile)
            ) {
                const {profile} = this.state    
                profile.deliveryRestriction = deliveryRestrictions[0].id
                profile.productType = productTypes[0].id

                this.setState({profile, loadingDependencies: false})
            }
        }

        this.props.loadCities(didDependenciesLoad)
        this.props.loadDeliveryRestrictions(didDependenciesLoad)
        this.props.loadProductTypes(didDependenciesLoad)

        if (this.isEditMode()) {
            const user = getUserFromLocalStorage()
            this.props.loadDeliveryProfile(user.companyId, this.getProfileId(), profile => {
                this.setState({profile: this.profileToForm(profile)})
                didDependenciesLoad()
            })
        }
    }

    maskedValueUpdated (event, maskedValue, floatValue) {
        const { name } = event.target
        const { profile } = this.state
        profile[name] = floatValue
        this.setState({profile})
    }

    valueUpdated (event) {
        const { value, name } = event.target
        const { profile } = this.state
        profile[name] = value
        this.setState({profile})
    }

    saveDeliveryProfile (evt) {
        const { profile } = this.state

        this.setState({savingProfile: true})
        
        if (this.isEditMode()) {
            this.props.updateDeliveryProfile(
                this.props.user.companyId, 
                this.getProfileId(), 
                this.formToProfile(profile), 
                () => this.setState({shallRedirectToProfilesList: true}), 
                () => this.setState({savingProfile: false})
            )
        } else {
            this.props.createDeliveryProfile(
                this.props.user.companyId, 
                this.formToProfile(profile), 
                () => this.setState({shallRedirectToProfilesList: true}), 
                () => this.setState({savingProfile: false})
            )
        }

        evt.preventDefault()
        return false
    }

    formToProfile (form) {
        return {
           name: form.name,
           estimated_price_per_km: form.estimatedPricePerKm,
           active: form.active === '1',
           product_type_id: parseInt(form.productType, 10),
           delivery_restriction_id: parseInt(form.deliveryRestriction, 10),
           start_city_id: parseInt(form.startCity, 10),
           end_city_id: parseInt(form.endCity, 10),
           measurement_restriction: {
               max_weight: parseInt(form.maxWeight, 10) || null,
               max_length: parseInt(form.maxLength, 10) || null,
               max_height: parseInt(form.maxHeight, 10) || null,
               max_width: parseInt(form.maxWidth, 10) || null
           }
        }
    }

    profileToForm (profile) {
        return {
            name: profile.name,
            estimatedPricePerKm: profile.estimated_price_per_km,
            active: profile.active ? '1' : '0',
            productType: profile.product_type_id,
            deliveryRestriction: profile.delivery_restriction_id,
            startCity: profile.start_city_id,
            endCity: profile.end_city_id,
            maxWeight: profile.measurement_restriction.max_weight,
            maxLength: profile.measurement_restriction.max_length,
            maxHeight: profile.measurement_restriction.max_height,
            maxWidth: profile.measurement_restriction.max_width,
        }
    }

    render () {
        if (this.state.shallRedirectToProfilesList) {
            return <Redirect to="/configuracoes/perfis-de-entrega" />
        }

        const {profile} = this.state
        const {isFieldInvalid} = this.validator

        return (
            <div>
                
                <Loader loading={this.state.loadingDependencies || this.state.savingProfile} />
                
                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='DeliveryProfiles' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph>Criar Perfil de entrega</Paragraph>
                            <WhiteCard>
                                <Row>
                                   <Col>
                                         <Form onSubmit={this.saveDeliveryProfile}>
                                            <Row>
                                                <Col md={12}>
                                                    <FieldContainer>
                                                        <Label>Nome do Perfil de Entrega</Label>
                                                        <TextField value={profile.name} onChange={this.valueUpdated} block name="name" type="text" placeholder="Ex: Perfil Curitiba Belo Horizonte" invalid={isFieldInvalid('name', profile.name)} />
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Preço estimado por quilometro rodado</Label>
                                                        <CurrencyField value={profile.estimatedPricePerKm} onChangeEvent={this.maskedValueUpdated} block="true" name="estimatedPricePerKm" placeholder="Ex: R$ 0,70"  decimalSeparator="," thousandSeparator="." precision="2" prefix="R$ " invalid={isFieldInvalid('estimatedPricePerKm', profile.estimatedPricePerKm)}/>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Perfil de Entrega Ativo</Label>
                                                        <Select value={profile.active} name="active" onChange={this.valueUpdated} block invalid={isFieldInvalid('active', profile.active)}>
                                                            <option value="1">Sim</option>
                                                            <option value="0">Não</option>
                                                        </Select>
                                                    </FieldContainer>
                                                </Col>

                                                 <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Tipo do Produto</Label>
                                                        <Select value={profile.productType} onChange={this.valueUpdated} block name="productType" invalid={isFieldInvalid('productType', profile.productType)}>
                                                            { this.props.productTypes.map(type => (
                                                                <option key={type.id} value={type.id}> 
                                                                    {type.name}
                                                                </option>
                                                            )) }
                                                        </Select>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Restrição da entrega</Label>
                                                        <Select value={profile.deliveryRestriction} onChange={this.valueUpdated} block name="deliveryRestriction" invalid={isFieldInvalid('deliveryRestriction', profile.deliveryRestriction)}>
                                                            { this.props.deliveryRestrictions.map(restriction => (
                                                                <option key={restriction.id} value={restriction.id}> 
                                                                    {restriction.name}
                                                                </option>
                                                            )) }
                                                        </Select>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={12}>
                                                    <FieldContainer>
                                                        <Label>Retirada na cidade</Label>
                                                        <Select value={profile.startCity} onChange={this.valueUpdated} block name="startCity" invalid={isFieldInvalid('startCity', profile.startCity)}>
                                                        <option></option>
                                                            { this.props.cities.map(city => (
                                                                <option key={city.id} value={city.id}> 
                                                                    {city.name} - {city.state}, {city.country}  
                                                                </option>
                                                            )) }
                                                        </Select>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={12}>
                                                    <FieldContainer>
                                                        <Label>Entrega na cidade</Label>
                                                        <Select value={profile.endCity} onChange={this.valueUpdated} block name="endCity" invalid={isFieldInvalid('endCity', profile.endCity)}>
                                                        <option></option>
                                                            { this.props.cities.map(city => (
                                                                <option key={city.id} value={city.id}> 
                                                                    {city.name} - {city.state}, {city.country}  
                                                                </option>
                                                            )) }
                                                        </Select>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Peso máximo (kg)</Label>
                                                        <TextField value={profile.maxWeight} onChange={this.valueUpdated} type="number" name="maxWeight" placeholder="Ex: 500" block invalid={isFieldInvalid('maxWeight', profile.maxWeight)} />
                                                    </FieldContainer>
                                                </Col>

                                                 <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Altura máxima (cm)</Label>
                                                        <TextField value={profile.maxHeight} onChange={this.valueUpdated} type="number" name="maxHeight" placeholder="Ex: 120" block invalid={isFieldInvalid('maxHeight', profile.maxHeight)} />
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Largura máxima (cm)</Label>
                                                        <TextField value={profile.maxWidth} onChange={this.valueUpdated} type="number" name="maxWidth" placeholder="Ex: 120" block invalid={isFieldInvalid('maxWidth', profile.maxWidth)}/>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={6}>
                                                    <FieldContainer>
                                                        <Label>Comprimento máximo (cm)</Label>
                                                        <TextField value={profile.maxLength} onChange={this.valueUpdated} type="number" name="maxLength" placeholder="Ex: 120" block invalid={isFieldInvalid('maxLength', profile.maxLength)}/>
                                                    </FieldContainer>
                                                </Col>

                                                <Col md={12}>
                                                    <SubmitContainer>
                                                        <Link to="/configuracoes/perfis-de-entrega/">Voltar</Link>

                                                        <ButtonPrimary>Salvar</ButtonPrimary>
                                                    </SubmitContainer>
                                                </Col>

                                            </Row>
                                        </Form> 
                                   </Col>
                                </Row>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage,
    cities: state.cityReducer.cities,
    deliveryRestrictions: state.deliveryRestrictionReducer.deliveryRestrictions,
    productTypes: state.productTypeReducer.productTypes,
    deliveryProfile: state.deliveryProfileReducer.deliveryProfile
})

const mapDispatchToProps = dispatch => ({
    loadCities: (onSuccess) => dispatch(loadCitiesAsync(onSuccess)),
    loadDeliveryRestrictions: (onSuccess) => dispatch(loadDeliveryRestrictionsAsync(onSuccess)),
    loadProductTypes: (onSuccess) => dispatch(loadProductTypesAsync(onSuccess)),
    loadDeliveryProfile: (shippingCompanyId, deliveryProfileId, onSuccess) => dispatch(loadDeliveryProfileAsync(shippingCompanyId, deliveryProfileId, onSuccess)),
    createDeliveryProfile: (companyId, profile, onSuccess, onError) => dispatch(createDeliveryProfileAsync(companyId, profile, onSuccess, onError)),
    updateDeliveryProfile: (companyId, profileId, profile, onSuccess, onError) => dispatch(updateDeliveryProfileAsync(companyId, profileId, profile, onSuccess, onError))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryProfileForm)