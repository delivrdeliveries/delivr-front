import React, { Component } from 'react'
import {connect} from 'react-redux'
import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'

import { loadDeliveryProfilesAsync } from '../../../../deliveryProfiles/actions'

import { Card, WhiteCard } from '../../../components/Containers'
import { Title, Paragraph, TinyText } from '../../../components/Texts'
import Loader from '../../../widgets/Loader'
import { Button } from '../../../components/Buttons'
import SettingsMenu from '../../../widgets/settings/SettingsMenu'
import TitleBack from '../../../widgets/TitleBack'
import { getUserFromLocalStorage } from '../../../../user/actions';


class DeliveryProfiles extends Component{

    state = {
        profilesLoaded: false,
        actives: [],
        inactives: []
    }

    componentDidMount () {
        const user = getUserFromLocalStorage()
        this.props.loadDeliveryProfiles(user.companyId, (profiles) => {
            this.setState({
                profilesLoaded: true,
                actives: profiles.filter(p => p.active),
                inactives: profiles.filter(p => !p.active)
            })
        })
    }

    profileRowTemplate (profile) {
        return (
            <Row key={profile.id}>
                <Col md={9}><Paragraph>Perfil {profile.name}</Paragraph></Col>
                <Col md={3}>
                    <NavLink to={'/configuracoes/perfis-de-entrega/editar/' + profile.id}>
                        <Button block>Editar</Button>
                    </NavLink>
                </Col>
            </Row>
        )
    }

    render () {
        return (
            <div>
                
                <Loader loading={!this.state.profilesLoaded} />

                <Title>Configurações <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <SettingsMenu active='DeliveryProfiles' />
                    </Col>

                    <Col>
                        <Card>
                            <Paragraph>Perfis de entrega</Paragraph>
                            <WhiteCard>
                                <Row>
                                    <Col md={4} offset={{md: 8}}>
                                        <NavLink to="/configuracoes/perfis-de-entrega/adicionar">
                                            <Button block>+ Perfil de Entrega</Button>
                                        </NavLink>
                                    </Col>
                                </Row>

                                <TinyText>Ativos</TinyText>
                                <Card>
                                    {this.state.actives.length === 0
                                        ? <Paragraph> Sem perfis de entrega ativos </Paragraph>
                                        : this.state.actives.map(this.profileRowTemplate)}
                                </Card>

                                <TinyText>Inativos</TinyText>
                                <Card>
                                    {this.state.inactives.length === 0
                                        ? <Paragraph> Sem perfis de entrega inativos </Paragraph>
                                        : this.state.inactives.map(this.profileRowTemplate)}
                                </Card>

                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    deliveryProfiles: state.deliveryProfileReducer.deliveryProfiles,
})

const mapDispatchToProps = dispatch => ({
    loadDeliveryProfiles: (shippingCompany, onSuccess) => dispatch(loadDeliveryProfilesAsync(shippingCompany, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryProfiles)