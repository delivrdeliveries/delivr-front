import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Bar } from 'react-chartjs-2'
import { Card, WhiteCard } from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import ReportsMenu from '../../widgets/reports/ReportsMenu'
import TitleBack from '../../widgets/TitleBack'
import Loader from '../../widgets/Loader';
import { getUserFromLocalStorage } from '../../../user/actions';
import { getDeliveriesPerShippingCompany, downloadCsvDeliveriesPerShippingCompany } from '../../../reports/endpoints';
import { FieldContainer, Label, MaskField } from '../../components/Forms';
import { Button, ButtonPrimary } from '../../components/Buttons';
import moment from 'moment'

const options = {
    scales: {
        xAxes: [{
            stacked: true
        }],
        yAxes: [{
            stacked: true
        }]
    }
}


export default class DeliveriesPerShippingCompanyReport extends Component {

    state = {
        loading: true,
        startDateText: '',
        endDateText: '',
        data: {}
    }

    constructor(props) {
        super(props)

        this.search = this.search.bind(this)
        this.updateText = this.updateText.bind(this)
        this.downloadCsv = this.downloadCsv.bind(this)
    }

    componentDidMount() {
        let startAt = moment()
        startAt = startAt.subtract(1, 'month').startOf('month')

        let endAt = moment()
        endAt = endAt.startOf('month')

        this.setState({
            startDateText: startAt.format('DD/MM/YYYY'),
            endDateText: endAt.format('DD/MM/YYYY')
        })

        this.fetchData(startAt, endAt)
    }

    async fetchData(startAt, endAt) {
        this.setState({ loading: true })
        const company = getUserFromLocalStorage()
        const { data } = await getDeliveriesPerShippingCompany(
            company.companyId,
            startAt.format('YYYY-MM-DD'),
            endAt.format('YYYY-MM-DD')
        )

        const chartData = {
            datasets: [{
                label: 'Aguardando retirada',
                data: data.map(d => d.awaiting_shipment),
                backgroundColor: '#6868fd',
                borderColor: 'black',
            },
            {
                label: 'Em transporte',
                data: data.map(d => d.on_carriage),
                backgroundColor: '#e6e657',
                borderColor: 'black',
            },
            {
                label: 'Problemas',
                data: data.map(d => d.problems),
                backgroundColor: '#d22c2c',
                borderColor: 'black',
            },
            {
                label: 'Finalizada',
                data: data.map(d => d.delivered),
                backgroundColor: '#42b942',
                borderColor: 'black',
            },],
            labels: data.map(d => `${d.shipping_company} (${d.total})`)
        }
        this.setState({ loading: false, data: chartData })
    }

    updateText({ target }) {
        this.setState({ [target.name]: target.value })
    }

    search(evt) {
        evt.preventDefault()
        const dates = this.getDates()
        if (!dates) {
            return false
        }

        const { startAt, endAt } = dates

        this.setState({
            startDateText: startAt.format('DD/MM/YYYY'),
            endDateText: endAt.format('DD/MM/YYYY')
        })

        this.fetchData(startAt, endAt)

        return false
    }

    getDates() {
        const startAt = moment(this.state.startDateText, "DD/MM/YYYY")
        const endAt = moment(this.state.endDateText, "DD/MM/YYYY")

        if (!startAt.isValid() || !endAt.isValid()) {
            alert('Preencha datas validas')
            return false
        }

        if (startAt > endAt) {
            alert('A data inicial deve ser menor que a final')
            return false
        }

        return { startAt, endAt }
    }

    async downloadCsv() {
        const dates = this.getDates()
        if (!dates) {
            return false
        }

        const { startAt, endAt } = dates

        const company = getUserFromLocalStorage()
        await downloadCsvDeliveriesPerShippingCompany(
            company.companyId,
            startAt.format('YYYY-MM-DD'),
            endAt.format('YYYY-MM-DD')
        )
    }

    render() {
        return (
            <div>
                <Loader loading={this.state.loading} />

                <Title>Relatórios <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <ReportsMenu active={'ShippingCompanyReport'} />
                    </Col>

                    <Col>
                        <Card>

                            <Row>
                                <Col md={8}>
                                    <Paragraph >Relatório de entregas do ano</Paragraph>
                                </Col>

                                <Col md={4}>
                                    <Button block onClick={this.downloadCsv}>Baixar CSV</Button>
                                </Col>
                            </Row>

                            <form onSubmit={this.search}>
                                <Row>
                                    <Col md={4}>
                                        <FieldContainer>
                                            <Label htmlFor='startDateText'>De:</Label>
                                            <MaskField id='startDateText' name='startDateText' value={this.state.startDateText}
                                                block
                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                onChange={this.updateText} />
                                        </FieldContainer>
                                    </Col>

                                    <Col md={4}>
                                        <FieldContainer>
                                            <Label htmlFor='endDateText'>Até:</Label>
                                            <MaskField id='endDateText' name='endDateText' value={this.state.endDateText}
                                                block
                                                mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                onChange={this.updateText} />
                                        </FieldContainer>
                                    </Col>
                                    <Col md={4} style={{ display: 'flex', alignItems: 'flex-end' }}>
                                        <FieldContainer>
                                            <ButtonPrimary block onClick={this.search}>Pesquisar</ButtonPrimary>
                                        </FieldContainer>
                                    </Col>
                                </Row>

                            </form>

                            <WhiteCard>
                                <Bar data={this.state.data} options={options} />
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}