import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Bar } from 'react-chartjs-2'
import { Card, WhiteCard } from '../../components/Containers'
import { Title } from '../../components/Texts'
import ReportsMenu from '../../widgets/reports/ReportsMenu'
import TitleBack from '../../widgets/TitleBack'
import Loader from '../../widgets/Loader';
import { getDeliveriesPerMonthReport } from '../../../reports/endpoints';
import { getUserFromLocalStorage } from '../../../user/actions';
import { Select, Label, FieldContainer } from '../../components/Forms';
import { Button } from '../../components/Buttons';
import endpoints from '../../../config/endpoints';

const options = {
  scales: {
    xAxes: [{
      stacked: true
    }],
    yAxes: [{
      stacked: true
    }]
  }
}

export default class DeliveriesPerMonth extends Component {

  state = {
    loading: true,
    data: {},
    year: (new Date()).getFullYear()
  }

  constructor(props) {
    super(props)

    this.changeYear = this.changeYear.bind(this)
    this.fetchData = this.fetchData.bind(this)
  }

  componentDidMount() {
    this.fetchData()
  }

  changeYear(evt) {
    this.setState({ year: evt.target.value })
    setTimeout(this.fetchData, 20)
  }

  async fetchData() {
    this.setState({ loading: true })
    const company = getUserFromLocalStorage()
    const { data } = await getDeliveriesPerMonthReport(company.companyId, this.state.year)
    const months = ['Jan.', 'Fev.', 'Mar.', 'Abr.', 'Mai.', 'Jun.', 'Jul.', 'Ago.', 'Set.', 'Out.', 'Nov.', 'Dez.']

    const chartData = {
      datasets: [{
        label: 'Aguardando retirada',
        data: data.map(d => d.awaiting_shipment),
        backgroundColor: '#6868fd',
        borderColor: 'black',
      },
      {
        label: 'Em transporte',
        data: data.map(d => d.on_carriage),
        backgroundColor: '#e6e657',
        borderColor: 'black',
      },
      {
        label: 'Problemas',
        data: data.map(d => d.problems),
        backgroundColor: '#d22c2c',
        borderColor: 'black',
      },
      {
        label: 'Finalizada',
        data: data.map(d => d.delivered),
        backgroundColor: '#42b942',
        borderColor: 'black',
      },],
      labels: data.map((d, k) => `${months[k]} (${d.total})`)
    }

    this.setState({ loading: false, data: chartData })
  }

  render() {
    const company = getUserFromLocalStorage()
    return (
      <div>
        <Loader loading={this.state.loading} />

        <Title>Relatórios <TitleBack /></Title>

        <Row>
          <Col md={3}>
            <ReportsMenu active='DeliveryReport' />
          </Col>

          <Col>
            <Card>
              <Row>
                <Col md={8}>
                  <FieldContainer>
                    <Label htmlFor='year'>Relatório de entregas do ano</Label>
                    <Select id='year' value={this.state.year} onChange={this.changeYear}>
                      {(() => {
                        const years = []
                        for (var year = 2017; year <= (new Date()).getFullYear(); year++) {
                          years.push(
                            <option value={year} key={year}>{year}</option>
                          )
                        }

                        return years
                      })()}
                    </Select>
                  </FieldContainer>
                </Col>

                <Col md={4} style={{ display: 'flex', alignItems: 'flex-end' }}>
                  <FieldContainer>
                    <a href={`${endpoints.REPORTS}/${company.companyId}/deliveries/per-month.csv?year=${this.state.year}`} download>
                      <Button block>Baixar CSV</Button>
                    </a>
                  </FieldContainer>
                </Col>
              </Row>

              <WhiteCard>
                <Bar data={this.state.data} options={options} />
              </WhiteCard>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }

}