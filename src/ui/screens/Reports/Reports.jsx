import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'

import { Card, WhiteCard} from '../../components/Containers'
import { Title, Paragraph } from '../../components/Texts'
import ReportsMenu from '../../widgets/reports/ReportsMenu'
import TitleBack from '../../widgets/TitleBack'

export default class Reports extends Component{

    render () {
        return (
            <div>
                
                <Title>Relatórios <TitleBack /></Title>

                <Row>
                    <Col md={3}>
                        <ReportsMenu />
                    </Col>

                    <Col>
                        <Card>
                            <WhiteCard>
                                <Paragraph>Selecione um Relatório</Paragraph>
                            </WhiteCard>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}