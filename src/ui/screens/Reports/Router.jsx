import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Reports from './Reports'
import DeliveriesPerShippingCompanyReport from './DeliveriesPerShippingCompanyReport';
import DeliveriesPerMonth from './DeliveriesPerMonthReport';


export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/relatorios' component={Reports} />
                <Route exact path='/relatorios/entregas-por-transportadoras' component={DeliveriesPerShippingCompanyReport} />
                <Route exact path='/relatorios/entregas-por-mes' component={DeliveriesPerMonth} />
            </div>
        )
    }
}