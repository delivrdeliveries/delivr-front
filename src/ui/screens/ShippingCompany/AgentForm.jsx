import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system'
import {connect} from 'react-redux'
import TitleBack from '../../widgets/TitleBack'
import { Title } from '../../components/Texts'
import { Card, WhiteCard } from '../../components/Containers'
import { FieldContainer, Label, TextField, SubmitContainer, Form, Select } from '../../components/Forms'
import { ButtonPrimary } from '../../components/Buttons'
import FormValidator, { isEmpty, isEmail } from '../../../formValidator';
import { createAgent, editAgent, loadAgent } from '../../../shippingCompanies/actions';
import Loader from '../../widgets/Loader';
import { Redirect } from 'react-router-dom'
import { getUserFromLocalStorage } from '../../../user/actions';

const PASSWORD_MASK = '__PWD_NOT_CHANGED__'

class AgentForm extends Component {

    state = {
        loading: false,
        agentSaved: false,
        agent: {active: 1}
    }

    constructor (props) {
        super(props)

        this.updateAgentField = this.updateAgentField.bind(this)
        this.submitForm = this.submitForm.bind(this)
        this.passwordsDoNotMatch = this.passwordsDoNotMatch.bind(this)

        this.validator = new FormValidator([
            {name: 'name', validators: [ isEmpty ]},
            {name: 'active', validators: [ isEmpty ]},
            {name: 'email', validators: [ isEmail ]},
            {name: 'password', validators: [ isEmpty ]},
        ])
    }

    componentDidMount () {
        if (this.isEditMode()) {
            this.setState({loading: true})
            const user = getUserFromLocalStorage()
            this.props.loadAgent(user.companyId, this.getAgentId(), 
                                    agent => {
                                        agent.active = agent.active ? '1' : '0'
                                        agent.password = PASSWORD_MASK
                                        agent.passwordConfirmation = PASSWORD_MASK
                                        this.setState({agent, loading: false})
                                    }
                                )
        }
    }

    isEditMode () {
        return this.getAgentId()
     }
 
     getAgentId () {
         return this.props.match.params.id
     }

    updateAgentField (event) {
        const {name, value} = event.target
        const {agent} = this.state
        agent[name] = value
        this.setState({agent})
    }

    passwordsDoNotMatch () {
        const {password, passwordConfirmation} = this.state.agent
        return password !== passwordConfirmation
    }

    submitForm (event) {
        event.preventDefault()

        if (this.validator.isFormInvalid()) {
            alert('Corrija os campos do formulário')
            return
        }

        this.saveAgent(this.props.user.companyId, this.state.agent)

        return false
    }

    saveAgent (companyId, agent) {
        this.setState({loading: true})
        
        const onSuccess = () => this.setState({agentSaved: true})

        if (agent.id) {
            if (agent.password === PASSWORD_MASK) {
                agent.password = ''
            }

            this.props.editAgent(companyId, agent.id, agent, onSuccess)
        } else {
            this.props.createAgent(companyId, agent, onSuccess)
        }
    }

    render () {
        if (this.state.agentSaved) {
            return <Redirect to="/transportadora/agentes/" />
        }

        const {isFieldInvalid} = this.validator
        const {agent} = this.state
        return (
            <div>
                <Loader loading={this.state.loading} />

                <Title>Novo Agente <TitleBack /></Title>
                <Card>
                    <WhiteCard>
                        <Form onSubmit={this.submitForm}>
                            <Row>
                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="name">Nome</Label>
                                        <TextField id="name" 
                                                    name="name" 
                                                    onChange={this.updateAgentField}
                                                    block
                                                    value={agent.name}
                                                    invalid={isFieldInvalid('name', agent.name)}/>
                                    </FieldContainer>
                                </Col>

                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="email">E-mail</Label>
                                        <TextField id="email" 
                                                    name="email" 
                                                    onChange={this.updateAgentField}
                                                    type="email" 
                                                    block
                                                    value={agent.email}
                                                    invalid={isFieldInvalid('email', agent.email)} />
                                    </FieldContainer>
                                </Col>
                            </Row>

                            <Row>
                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="active">Ativo</Label>
                                        <Select id="active" 
                                                    name="active" 
                                                    onChange={this.updateAgentField}
                                                    block
                                                    value={agent.active}
                                                    invalid={isFieldInvalid('active', agent.active)}>
                                            <option value="1">Sim</option>
                                            <option value="0">Não</option>
                                        </Select>
                                    </FieldContainer>
                                </Col>

                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="password">Senha</Label>
                                        <TextField id="password" 
                                                    name="password" 
                                                    onChange={this.updateAgentField}
                                                    type="password" 
                                                    block
                                                    value={agent.password}
                                                    invalid={isFieldInvalid('password', agent.password)}/>
                                    </FieldContainer>
                                </Col>

                                <Col>
                                    <FieldContainer>
                                        <Label htmlFor="passwordConfirmation">Confirmação Senha</Label>
                                        <TextField id="passwordConfirmation" 
                                                    name="passwordConfirmation" 
                                                    onChange={this.updateAgentField}
                                                    type="password" 
                                                    block
                                                    value={agent.passwordConfirmation}
                                                    invalid={this.passwordsDoNotMatch()}/>
                                    </FieldContainer>
                                </Col>
                            </Row>

                            <SubmitContainer>
                                <ButtonPrimary>
                                    Salvar
                                </ButtonPrimary>
                            </SubmitContainer>
                        </Form>
                    </WhiteCard>
                </Card>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.userReducer.userLocalStorage
})

const mapDispatchToProps = dispatch => ({
    createAgent: (companyId, agent, onSuccess) => dispatch(createAgent(companyId, agent, onSuccess)),
    editAgent: (companyId, agentId, agent, onSuccess) => dispatch(editAgent(companyId, agentId, agent, onSuccess)),
    loadAgent: (companyId, agentId, onSuccess) => dispatch(loadAgent(companyId, agentId, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AgentForm)