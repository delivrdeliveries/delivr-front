import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getStateLabel } from '../../../deliveries/strings'
import { allStates } from '../../../deliveries/status'
import Loader from '../../widgets/Loader'
import { Card, PaginationContainer } from '../../components/Containers'
import { Title } from '../../components/Texts'
import { Button, ButtonPrimary } from '../../components/Buttons'
import { Select, TextField } from '../../components/Forms'

import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'
import SettingsIcon from 'react-icons/lib/md/settings'

import DeliveriesTable from '../../widgets/deliveries/DeliveriesTable'
import { loadShippingCompanyDeliveries } from '../../../shippingCompanies/actions';
import { getUserFromLocalStorage } from '../../../user/actions';
import { PAGINATION_SIZE } from '../../../config/pagination';

class ListDeliveries extends Component {
    state = {
        loading: true,
        state: '',
        name: '',
        page: 0
    }

    constructor(props) {
        super(props)

        this.updateState = this.updateState.bind(this)
        this.filter = this.filter.bind(this)
        this.loadDeliveries = this.loadDeliveries.bind(this)
        this.nextPage = this.nextPage.bind(this)
        this.previousPage = this.previousPage.bind(this)
    }

    componentDidMount() {
        this.user = getUserFromLocalStorage()
        this.loadDeliveries()
    }

    loadDeliveries() {
        this.setState({ loading: true })
        this.props.loadDeliveries(this.user.companyId, () => this.setState({ loading: false }), this.state)
    }

    filter(evt) {
        if (evt.key && evt.key !== 'Enter')
            return

        this.updateState(evt)
        this.setState({ page: 0 })
        setTimeout(() => this.loadDeliveries(), 20)
    }

    updateState(evt) {
        const newState = this.state
        newState[evt.target.name] = evt.target.value
        this.setState(newState)
    }

    nextPage() {
        if (this.props.deliveries.length < PAGINATION_SIZE) {
            return
        }

        this.setState({ page: this.state.page + 1 })
        setTimeout(() => this.loadDeliveries(), 20)
    }

    previousPage() {
        if (this.state.page === 0) {
            return
        }

        this.setState({ page: this.state.page - 1 })
        setTimeout(() => this.loadDeliveries(), 20)
    }


    render() {
        return (
            <div>
                <Loader loading={this.state.loading} />

                <Title>Entregas da Transportadora</Title>

                <Row>
                    <Col>
                        <Select name='state' block onChange={this.filter} value={this.state.state}>
                            <option value="">Estado</option>
                            {allStates.map(state =>
                                <option key={state} value={state}>
                                    {getStateLabel(state)}
                                </option>
                            )}
                        </Select>
                    </Col>
                    <Col>
                        <TextField
                            name="name"
                            value={this.state.name}
                            placeholder="Nome"
                            onBlur={this.filter}
                            onKeyPress={this.filter}
                            onChange={this.updateState}
                            block />
                    </Col>
                    <Col>
                        <NavLink to="/transportadora/agentes">
                            <Button block><SettingsIcon /> Gerenciar Agentes</Button>
                        </NavLink>
                    </Col>
                </Row>

                {
                    this.props.deliveries.length > 0 || this.state.page > 0
                        ? (
                            <div>
                                <DeliveriesTable deliveries={this.props.deliveries} />
                                <PaginationContainer>
                                    <ButtonPrimary disabled={this.state.page === 0} onClick={this.previousPage}>Página anterior</ButtonPrimary>
                                    <ButtonPrimary disabled={this.props.deliveries.length < PAGINATION_SIZE} onClick={this.nextPage}>Próxima página</ButtonPrimary>
                                </PaginationContainer>
                            </div>
                        )
                        : (<Card>Você ainda não possui nenhuma entrega! Crie uma agora :)</Card>)
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    deliveries: state.shippingCompanyReducer.deliveries,
})

const mapDispatchToProps = dispatch => ({
    loadDeliveries: (id, success, params) => dispatch(loadShippingCompanyDeliveries(id, success, params)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ListDeliveries)