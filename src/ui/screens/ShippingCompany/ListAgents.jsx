import React, { Component } from 'react'
import {connect} from 'react-redux'

import { loadAgents } from '../../../shippingCompanies/actions'
import Loader from '../../widgets/Loader'
import { Title, Paragraph } from '../../components/Texts'
import { Button, ButtonPrimary } from '../../components/Buttons'
import { Table, TCel, TRow, THeaderRow, THeaderCel } from '../../components/Tables'
import TitleBack from '../../widgets/TitleBack'

import { Row, Col } from 'react-grid-system'
import { NavLink } from 'react-router-dom'
import AddIcon from 'react-icons/lib/md/add'
import { getUserFromLocalStorage } from '../../../user/actions';
import { Card } from '../../components/Containers';

class ListAgents extends Component {
    state = {
        loading: true
    }

    componentDidMount() {
        const user = getUserFromLocalStorage()
        this.props.loadAgents(user.companyId, () => this.setState({loading: false}))
    }

    render () {
        return (
            <div>
                <Loader loading={this.state.loading} />

                <Title>Agentes <TitleBack /></Title>

                <Row>
                    <Col md={4} offset={{md: 8}}>
                        <NavLink to='/transportadora/agentes/novo'>
                            <Button block><AddIcon /> Novo Agente</Button>
                        </NavLink>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        { this.props.agents.length ===  0 
                            ? <Card> <Paragraph> Você ainda não possui nenhum agente</Paragraph> </Card>
                            : (
                                <Table>
                                    <THeaderRow>
                                        <THeaderCel>Nome</THeaderCel>
                                        <THeaderCel>Email</THeaderCel>
                                        <THeaderCel>Ativo</THeaderCel>
                                        <THeaderCel>Ações</THeaderCel>
                                    </THeaderRow>

                                    {this.props.agents.sort(a => !a.active).map(agent => (
                                        <TRow key={agent.id}>
                                            <TCel> {agent.name} </TCel>
                                            <TCel> {agent.email} </TCel>
                                            <TCel> {agent.active ? 'Sim' : 'Não'} </TCel>
                                            <TCel> 
                                                <NavLink to={'/transportadora/agentes/editar/' + agent.id}> 
                                                    <ButtonPrimary> Editar </ButtonPrimary> 
                                                </NavLink> 
                                            </TCel>
                                        </TRow>
                                    ))}

                                </Table>
                            )}
                    </Col>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    agents: state.shippingCompanyReducer.agents,
})

const mapDispatchToProps = dispatch => ({
    loadAgents: (companyId, onSuccess) => dispatch(loadAgents(companyId, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ListAgents)