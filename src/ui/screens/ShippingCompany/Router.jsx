import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import ListDeliveries from './ListDeliveries'
import ListAgents from './ListAgents'
import AgentForm from './AgentForm'

export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/transportadora/entregas' component={ListDeliveries} />
                <Route exact path='/transportadora/agentes' component={ListAgents} />
                <Route exact path='/transportadora/agentes/novo' component={AgentForm} />
                <Route exact path='/transportadora/agentes/editar/:id' component={AgentForm} />
            </div>
        )
    }
}