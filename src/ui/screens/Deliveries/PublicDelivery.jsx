import React, { Component } from 'react'
import {connect} from 'react-redux'

import InteractionIcon from 'react-icons/lib/fa/list'
import InfoIcon from 'react-icons/lib/fa/map'

import {loadDeliveryAsync} from '../../../deliveries/actions'

import Loader from '../../widgets/Loader'
import NotFound from '../../widgets/NotFound'
import StateTimeLine from '../../widgets/deliveries/StateTimeLine'
import DeliveryInfos from './DeliveryInfos'
import DeliveryEvents from './DeliveryEvents'

import { Card, TabbedCard, Tab } from '../../components/Containers'
import { Title, Subtitle } from '../../components/Texts'
import TitleBack from '../../widgets/TitleBack';

class Delivery extends Component {

    state = {
        loading: true,
        notFound: false,
        activeComponent: 'DeliveryInfos',
    }

    constructor (props) {
        super(props)
        
        this.setActiveComponent = this.setActiveComponent.bind(this)
        this.getActiveComponent = this.getActiveComponent.bind(this)
    }

    componentDidMount () {
        this.props.loadDelivery(this.props.match.params.locator,
            () => this.setState({loading: false}),
            () => this.setState({notFound: true})
        )
    }

    setActiveComponent(activeComponent) {
        return () => this.setState({activeComponent})
    }

    getActiveComponent() {
        switch (this.state.activeComponent) {
            case 'DeliveryInfos':
                return <DeliveryInfos delivery={this.props.delivery} public={true} />
            case 'DeliveryEven`ts':
                return <DeliveryEvents delivery={this.props.delivery} />
            default:
                return
        }
    }

    render () {
        if (this.state.notFound) {
            return <NotFound reason={`Desculpe mas não foi possível localizar a Entrega #${this.props.match.params.locator}`} backTo="/publico/entrega"/>
        }

        if (this.state.loading) {
            return <Loader loading={true} />
        }

        const {delivery} = this.props

        return (
            <div>
                <Title>
                    Entrega #{delivery.locator} <TitleBack backTo="/publico/entrega" />
                </Title>

                <Subtitle> Aqui você pode ver tudo o que interessa sobre a sua entrega ;)</Subtitle>

                <StateTimeLine state={delivery.status}/>          

                <TabbedCard>
                    <Tab onClick={this.setActiveComponent('DeliveryInfos')} active={this.state.activeComponent === 'DeliveryInfos'}>  
                        <InfoIcon /> Informações Gerais
                    </Tab>
                    <Tab onClick={this.setActiveComponent('DeliveryEvents')} active={this.state.activeComponent === 'DeliveryEvents'}> 
                        <InteractionIcon /> Interações
                    </Tab>

                    <Card>
                        {this.getActiveComponent()}
                    </Card>
                </TabbedCard>
               
            </div>
        )
    }
}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery,
})

const mapDispatchToProps = dispatch => ({
    loadDelivery: (locator, onSuccess, onError) => dispatch(loadDeliveryAsync(locator, onSuccess, onError)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Delivery)