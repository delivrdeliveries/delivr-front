import React, { Component } from 'react'
import { Row, Col } from 'react-grid-system';
import { Card, WhiteCard } from '../../components/Containers';
import { LogoIcon } from '../../components/Icons';
import { Form, SubmitContainer, TextField, Label, FieldContainer } from '../../components/Forms';
import { ButtonPrimary } from '../../components/Buttons';
import { Redirect } from 'react-router-dom'

export default class PublicDeliveryLocator extends Component {

    state = {
        locator: '',
        redirect: false
    }

    constructor (props) {
        super(props)

        this.locateDelivery = this.locateDelivery.bind(this)
        this.updateLocator = this.updateLocator.bind(this)
    }

    locateDelivery (event) {
        this.setState({redirect: true})
        event.preventDefault()
        return false
    }

    updateLocator (event) {
        this.setState({locator: event.target.value})
    }


    render () {
        if (this.state.redirect) {
            return <Redirect to={`/publico/entrega/${this.state.locator}`} />
        }

       return (
        <div>
            <Row>
                <Col md={6} offset={{md: 3}} style={{textAlign: 'center'}}>
                    <Card style={{textAlign: 'center'}}>
                        <WhiteCard>

                            <LogoIcon style={{width: '200px'}}/>

                            <Form onSubmit={this.locateDelivery}>
                                <FieldContainer>
                                    <Label style={{textAlign: 'center'}} htmlFor="locator"> Localize aqui sua entrega </Label>
                                    <TextField type="text" placeholder="localizador" block name="locator" id="locator" onChange={this.updateLocator} />
                                </FieldContainer>

                                <SubmitContainer>
                                    <ButtonPrimary block> Buscar </ButtonPrimary>
                                </SubmitContainer>
                            </Form>

                        </WhiteCard>

                    </Card>
                </Col>
            </Row>
        </div>
       )
    }

}