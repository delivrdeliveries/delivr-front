import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import Delivery from './Delivery'
import PublicDelivery from './PublicDelivery'
import PublicDeliveryLocator from './PublicDeliveryLocator';

export default class RouterContainer extends Component {
    render () {
        return (
            <div>
                <Route exact path='/entrega/:locator' component={Delivery} />
                <Route exact path='/publico/entrega' component={PublicDeliveryLocator} />
                <Route exact path='/publico/entrega/:locator' component={PublicDelivery} />
            </div>
        )
    }
}