import React, { Component } from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'

import InteractionIcon from 'react-icons/lib/fa/list'
import MessagesIcon from 'react-icons/lib/md/message'
import InfoIcon from 'react-icons/lib/fa/map'
import AddIcon from 'react-icons/lib/md/add'

import {loadDeliveryAsync} from '../../../deliveries/actions'

import Loader from '../../widgets/Loader'
import NotFound from '../../widgets/NotFound'
import TitleBack from '../../widgets/TitleBack'
import StateTimeLine from '../../widgets/deliveries/StateTimeLine'
import DeliveryActions from '../../widgets/deliveries/DeliveryActions'
import DeliveryInfos from './DeliveryInfos'
import DeliveryEvents from './DeliveryEvents'
import DeliveryMessages from './DeliveryMessages'

import { Card, TabbedCard, Tab } from '../../components/Containers'
import { Title } from '../../components/Texts'
import { FloatingActionButton } from '../../components/Buttons'

import { Row } from 'react-grid-system'

class Delivery extends Component {

    state = {
        activeComponent: 'DeliveryInfos',
        loading: true
    }

    constructor (props) {
        super(props)
        
        this.setActiveComponent = this.setActiveComponent.bind(this)
        this.getActiveComponent = this.getActiveComponent.bind(this)
    }

    componentDidMount () {
        this.props.loadDelivery(this.props.match.params.locator, () => this.setState({loading: false}))
    }

    setActiveComponent(activeComponent) {
        return () => this.setState({activeComponent})
    }

    getActiveComponent() {
        switch (this.state.activeComponent) {
            case 'DeliveryInfos':
                return <DeliveryInfos delivery={this.props.delivery} />
            case 'DeliveryEvents':
                return <DeliveryEvents delivery={this.props.delivery} />
            case 'DeliveryMessages':
                return <DeliveryMessages delivery={this.props.delivery} />
            default:
                return
        }
    }

    render () {
        if (this.state.loading)
            return <Loader loading={this.state.loading}/>

        const {delivery} = this.props
        if (!delivery)
            return <NotFound reason={'Desculpe mas não foi possível localizar a Entrega #' + this.props.match.params.locator}/>

        return (
            <div>
                <Title style={{marginBottom: '30px'}}>
                    Entrega #{delivery.locator} <TitleBack /> 
                </Title>

                <Row>
                    <DeliveryActions delivery={delivery} />
                </Row> 

                <StateTimeLine state={delivery.status}/>          

                <TabbedCard>
                    <Tab onClick={this.setActiveComponent('DeliveryInfos')} active={this.state.activeComponent === 'DeliveryInfos'}>  
                        <InfoIcon /> Informações Gerais
                    </Tab>
                    <Tab onClick={this.setActiveComponent('DeliveryEvents')} active={this.state.activeComponent === 'DeliveryEvents'}> 
                        <InteractionIcon /> Interações
                    </Tab>
                    <Tab onClick={this.setActiveComponent('DeliveryMessages')} active={this.state.activeComponent === 'DeliveryMessages'}>  
                        <MessagesIcon /> Mensagens
                    </Tab>

                    <Card>
                        {this.getActiveComponent()}
                    </Card>
                </TabbedCard>

                 <NavLink to='/fornecedora/entregas/nova'>
                    <FloatingActionButton>
                        <AddIcon style={{margin: '0 auto'}}/>
                    </FloatingActionButton>
                </NavLink>
               
            </div>
        )
    }
}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery
})

const mapDispatchToProps = dispatch => ({
    loadDelivery: (locator, onSuccess) => dispatch(loadDeliveryAsync(locator, onSuccess)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Delivery)