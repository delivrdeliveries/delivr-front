import React, { Component } from 'react'
import {connect} from 'react-redux'

import {
    loadTopicsAsync, createTopic
} from '../../../messages/actions'

import MessagesModal from './MessagesModal'
import Loader from '../../widgets/Loader'
import MessageTopicCard from '../../widgets/deliveries/MessageTopicCard'
import { WhiteCard } from '../../components/Containers'
import { Paragraph } from '../../components/Texts';
import { Button, ButtonPrimary } from '../../components/Buttons';
import { Col, Row } from 'react-grid-system';
import { TextField } from '../../components/Forms';


class DeliveryMessages extends Component {

    state = {
        loading: true,
        topicName: '',
        creatingTopic: false,
        selectedTopic: null
    }

    constructor (props) {
        super(props)

        this.openMessagesModal = this.openMessagesModal.bind(this)
        this.closeMessagesModal = this.closeMessagesModal.bind(this)
        this.topicNameChanged = this.topicNameChanged.bind(this)
        this.createTopic = this.createTopic.bind(this)
        this.exitEditMode = this.exitEditMode.bind(this)
        this.enterEditMode = this.enterEditMode.bind(this)
    }

    componentDidMount () {
        this.loadTopics()
    }

    loadTopics () {
        this.props.loadTopics(this.props.delivery.locator, () => {
            this.setState({loading: false})
        })
    }

    openMessagesModal(topic) {
        this.setState({selectedTopic: topic})
    }

    closeMessagesModal() {
        this.setState({selectedTopic: null})
    }

    topicNameChanged(event){
        this.setState({topicName: event.target.value})
    }

    createTopic(){
        if (this.state.topicName.trim().length === 0) {
            alert('Nome do tópico é obrigatório')
            return 
        }

        this.props.createTopic(
            this.props.delivery.locator, 
            {name: this.state.topicName}, 
            () => {
                this.loadTopics()
                this.exitEditMode()
            }
        )
    }

    exitEditMode(){
        this.setState({creatingTopic: false, topicName: ''})
    }

    enterEditMode(){
        this.setState({creatingTopic: true})
    }
    
    render () {

        return (
            <WhiteCard>
                <Loader loading={this.state.loading} />

                {this.state.creatingTopic ? (
                    <Row>
                        <Col md={7}><TextField block value={this.state.topicName} onChange={this.topicNameChanged}/></Col>
                        <Col md={3}><ButtonPrimary block onClick={this.createTopic}>Criar Novo Tópico</ButtonPrimary></Col>
                        <Col md={2}><Button block onClick={this.exitEditMode}>Cancelar</Button></Col>
                    </Row>
                ):(
                    <Row>
                        <Col md={3} offset={{md: 9}}><Button block onClick={this.enterEditMode}>Criar Novo Tópico</Button></Col>
                    </Row>
                )}
                
                {this.props.topics.length === 0 ? <Paragraph style={{textAlign: 'center'}}>Essa entrega ainda não possui tópicos</Paragraph> : null }
                {this.props.topics.map(topic => <MessageTopicCard key={topic._id} topic={topic} onClick={() => this.openMessagesModal(topic)}/>)}

                 {(() => {
                    if (this.state.selectedTopic)
                        return <MessagesModal topic={this.state.selectedTopic} onExit={this.closeMessagesModal} />
                })()}
            </WhiteCard>
        )
    }
}

const mapStateToProps = state => ({
    topics: state.messageReducer.topics,
    loadingMessages: state.messageReducer.loadingMessages
})

const mapDispatchToProps = dispatch => ({
    loadTopics: (deliveryLocator, onSuccess) => dispatch(loadTopicsAsync(deliveryLocator, onSuccess)),
    createTopic: (deliveryLocator, topic, onSuccess) => dispatch(createTopic(deliveryLocator, topic, onSuccess))
})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryMessages)