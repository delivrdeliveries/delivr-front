import React, { Component } from 'react'
import {connect} from 'react-redux'

import DeliveryEventCard from '../../widgets/deliveries/DeliveryEventCard'
import { WhiteCard } from '../../components/Containers'

class DeliveryEvents extends Component {

    render () {
        return (
            <WhiteCard>
                {this.props.delivery.events.map(event => <DeliveryEventCard key={event.id} event={event} />)}
            </WhiteCard>
        )
    }
}

const mapStateToProps = state => ({
    delivery: state.deliveryReducer.delivery,
})

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryEvents)