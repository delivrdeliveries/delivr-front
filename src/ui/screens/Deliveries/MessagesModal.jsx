import React, { Component } from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'

import {loadMessagesAsync} from '../../../messages/actions'

import Modal from '../../components/Modal'
import {ButtonPrimary, Button} from '../../components/Buttons'
import { enterMessageChat } from '../../../messages/socket';
import { formatDateTime } from '../../../date';
import { TinyText } from '../../components/Texts';

const MessagesContainer = styled.div`
    width: 100%;
    height: 400px;
    overflow-x: auto;
    padding: 10px 20px;
    width: 760px;
`
const ComposerContainer = styled.div`
    position: relative;
    display: flex;
    padding: 10px 20px;
`
const ComposerArea = styled.textarea`
    padding: 10px 20px;
    border-radius: 5px;
    font-size: 0.96em;
    cursor: pointer;
    background: ${p => p.theme.light};
    border: 0;
    color: ${p => p.theme.mainDark};
    display: inline-block;
    height: 30px;
    width: 600px;
    resize: none;
    border-radius: 5px 0px 0px 5px;
`
const SendButton = ButtonPrimary.extend`
    display: inline-block;
    width: 120px;
    height: 50px;
    margin: 0px;
    border-radius: 0px 5px 5px 0px;
`
const MessageReceived = styled.div`
    position: relative;
    display:block;
    margin-top: 10px;
    margin-bottom: 20px;
    text-align: left;

    & .content {
        display: inline-block;
        padding: 20px 10px;
        background: ${p => p.theme.light};
        border-radius: 5px;
        width: 400px;
        font-size: 0.9em;
    }

    & .time {
        font-size: 0.7em;
        margin-left: 10px;
    }
`
const MessageSent = styled.div`
    position: relative;
    display:block;
    margin-top: 10px;
    margin-bottom: 20px;
    text-align: right;

    & .content {
        display: inline-block;
        padding: 20px 10px;
        background: ${p => p.theme.mainLight};
        border-radius: 5px;
        width: 400px;
        text-align: left;
        font-size: 0.9em;
    }

    & .time {
        font-size: 0.7em;
        margin-right: 10px;
    }
`
const TopText = TinyText.extend`
    text-align: center;
    font-style: italic;
    margin-bottom: 20px;
`

const LoadMoreButton = Button.extend`
    font-size: 0.7em;
    margin: 0 auto;
    display: block;
    margin-bottom: 20px;
`

class MessagesModal extends Component {

    state = {
        loading: true,
        noOlderMessages: false,
        message: '',
        messages: []
    }

    constructor (props) {
        super(props)

        this.updateMessage = this.updateMessage.bind(this)
        this.sendMessage = this.sendMessage.bind(this)
        this.sendMessageThroughKeyEnter = this.sendMessageThroughKeyEnter.bind(this)
        this.loadOlderMessages = this.loadOlderMessages.bind(this)
    }

    componentDidMount () {
        const {_id: topicId, delivery: deliveryId } = this.props.topic
        
        this.props.loadMessages(deliveryId, topicId, (messages) => { 
            this.setState({
                messages, 
                loading: false, 
                noOlderMessages: messages.length < 10
            }) 
            this.scrollMessagesContainerBottom()
        })  

        this.socket = enterMessageChat()
        this.socket.on(`message:${deliveryId}-${topicId}`, message => {
            const { messages } = this.state
            messages.push(message)
            this.setState({ messages })
            this.scrollMessagesContainerBottom()
        })
    }

    componentWillUnmount () {
        this.socket.disconnect()
    }

    loadOlderMessages () {
        const {messages} = this.state
        const {_id: topicId, delivery: deliveryId } = this.props.topic

        this.setState({loading: true})

        this.props.loadMessages(deliveryId, topicId, olderMessages => {
            const allMessages =  olderMessages.concat(messages)
            this.setState({
                messages: allMessages, 
                loading: false, 
                noOlderMessages: olderMessages.length < 10
            }) 
        }, messages.length)

        
    }

    updateMessage (event) {
        this.setState({ message: event.target.value })
    }

    sendMessageThroughKeyEnter (event) {
        if (event.key !== 'Enter' || event.shiftKey) {
            return
        }

        event.preventDefault()
        this.sendMessage()
        return false
    }
    
    scrollMessagesContainerBottom() {
        setTimeout(() => {
            const objDiv = document.getElementById('messagesContainer')
            objDiv.scrollTop = objDiv.scrollHeight
        }, 20)
    }

    sendMessage () {
        const { messages, message: messageContent } = this.state
        const {_id: topicId, delivery: deliveryId } = this.props.topic
        const { companyId } = this.props.user

        if (!messageContent || messageContent.trim().length === 0) {
            return
        }

        const message = {
            message: messageContent.trim(),
            sentAt: new Date(),
            sentBy: {
                companyId
            }
        }  

        this.socket.emit('message:sent', {  deliveryId, topicId, message })
        messages.push(message)
        this.setState({ message: '', messages })
        this.scrollMessagesContainerBottom()
    }

    render () {
        const { companyId  } = this.props.user 

        return (
            <Modal title={this.props.topic.name} onExit={this.props.onExit} noPadding={true}> 
                <MessagesContainer id="messagesContainer">

                    { 
                        this.state.loading
                        ? <TopText style={{textAlign: "center"}}>Carregando...</TopText>
                        : this.state.noOlderMessages
                        ? <TopText style={{textAlign: "center"}}>Não há mensagens antigas</TopText>
                        : <LoadMoreButton onClick={this.loadOlderMessages}>Carregar mensagens anteriores</LoadMoreButton>
                    }

                    {
                        this.state.messages.map((message, idx) => {
                            if (message.sentBy.companyId === companyId) {
                                return (
                                    <MessageSent key={idx}>
                                        <span className="time">{formatDateTime(message.sentAt)}</span>
                                        <span className="content">{message.message}</span>
                                    </MessageSent>
                                )
                            } else {
                                return (
                                    <MessageReceived key={idx}>
                                        <span className="content" >{message.message}</span>
                                        <span className="time">{formatDateTime(message.sentAt)}</span>
                                    </MessageReceived>
                                )
                            }
                        })
                    }
                
                </MessagesContainer>

                <ComposerContainer>
                    <ComposerArea onChange={this.updateMessage} onKeyPress={this.sendMessageThroughKeyEnter} value={this.state.message} />
                    <SendButton onClick={this.sendMessage}> Enviar </SendButton>
                </ComposerContainer>

            </Modal>
        )
    }
}

const mapStateToProps = state => ({
    messages: state.messageReducer.messages,
    user: state.userReducer.userLocalStorage
})

const mapDispatchToProps = dispatch => ({
    loadMessages: (deliveryId, topicId, onSuccess, skip = 0) => dispatch(loadMessagesAsync(deliveryId, topicId, onSuccess, skip)),
})

export default connect(mapStateToProps, mapDispatchToProps)(MessagesModal)