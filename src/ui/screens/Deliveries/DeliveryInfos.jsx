import React, { Component } from 'react'

import { Paragraph } from '../../components/Texts'
import { formatDate, formatDateTime } from '../../../date'

import { Row, Col } from 'react-grid-system'
import { WhiteCard } from '../../components/Containers'
import transportingMethod from '../../../deliveries/transportingMethod';
import formatAddress from '../../../addresses/formatAddress';
import formatCurrency from '../../../currency/formatCurrency';

export default class DeliveryInfos extends Component {

    render () {
        const {delivery} = this.props

        return (
            <WhiteCard>
                
                <Row>
                    <Col>
                        <Paragraph> <strong>Nome do Produto:</strong> {delivery.load_description} </Paragraph>
                    </Col>

                     <Col>
                        <Paragraph> <strong>Tipo do Produto:</strong>  { delivery.product_type.name } </Paragraph>
                    </Col>
                </Row>

                 <Row>
                    <Col>
                        <Paragraph> <strong>Data Emissão:</strong> {formatDateTime(delivery.created_at)} </Paragraph>
                    </Col>

                    <Col>
                        <Paragraph> <strong>Data Estimada:</strong> {formatDate(delivery.expected_date) || 'Sem data Estimada'} </Paragraph>
                    </Col>
                </Row>

                <Row>
                    <Col>
                      <Paragraph> <strong>Data Coletado:</strong> {formatDateTime(delivery.shipped_at) || 'Não Coletado'} </Paragraph>
                    </Col>

                    <Col>
                      <Paragraph> <strong>Data Finalização:</strong> {formatDateTime(delivery.delivered_at) || 'Não Finalizado'} </Paragraph>
                    </Col>
                </Row>   

                 <Row>
                    <Col>
                      <Paragraph> <strong>Transportadora: </strong> {
                          delivery.transporting_method === transportingMethod.OUTSOURCED ?
                          delivery.shipping_company.legal_name :
                          'Particular'
                      } </Paragraph>
                    </Col>

                    <Col>
                      <Paragraph> <strong>Endereço:</strong> {formatAddress(delivery.end_address)} </Paragraph>
                    </Col>
                </Row>   

                <Row>
                    <Col>
                      {  
                          !this.props.public
                          ? <Paragraph> <strong>Preço:</strong> {formatCurrency(delivery.estimated_price)} (estimado)  </Paragraph>
                          : null
                      }
                    </Col>

                    <Col>
                      <Paragraph> <strong>Restrição: </strong> { delivery.delivery_restriction.name } </Paragraph>
                    </Col>
                </Row>   
               
            </WhiteCard>
        )
    }
}