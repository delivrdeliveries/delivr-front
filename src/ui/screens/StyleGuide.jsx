import React, { Component } from 'react'

import Loader from '../widgets/Loader'

import {
    Container,
    Row,
    Col
} from 'react-grid-system'

import {
    Title,
    Subtitle,
    Paragraph,
    TinyText
} from '../components/Texts'

import {
    Button,
    ButtonPrimary,
    ButtonWarning,
    ButtonDanger
} from '../components/Buttons'

import {
    FieldContainer,
    Label,
    TextField,
    TextArea,
    Select,
    Radio,
    Checkbox,
} from '../components/Forms'

class StyleGuide extends Component {
    state = {
        loading: false
    }

    constructor () {
        super()
        this.simulateLoading = this.simulateLoading.bind(this)
    }

    simulateLoading () {
        this.setState({loading: true})
        setTimeout(() => this.setState({loading: false}), 500)
    }

    render () {
        return (
            <div>
                <Title id="texts">Texts (Title)</Title>
                <Subtitle>Subtitle Subtitle Subtitle Subtitle</Subtitle>

                <Paragraph>
                    <strong>Paragraph.</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ante vitae nisi interdum aliquet. Phasellus mattis elementum diam a placerat. Sed nec nunc ultricies erat hendrerit elementum quis ac lacus. Pellentesque rhoncus ipsum gravida, egestas ligula quis, tristique dui. Sed feugiat tincidunt nibh ut cursus. Quisque vitae hendrerit erat. Etiam vulputate condimentum interdum. Nunc viverra, leo in rutrum iaculis, odio justo ullamcorper mauris, vel pulvinar tellus est eget urna.
                </Paragraph>

                <Paragraph>
                    Maecenas venenatis, ligula eu interdum pulvinar, nulla metus ullamcorper mi, rutrum mollis ligula erat ac orci. Fusce venenatis erat tincidunt, sodales mi ut, convallis augue. Pellentesque et orci risus. Phasellus vestibulum nisi enim, id euismod diam condimentum eget. Duis ut metus justo. Duis porta luctus est, et gravida ante gravida id. Nam dictum blandit tincidunt. Phasellus et facilisis urna, eu consequat velit. Cras et massa pharetra est viverra blandit. Phasellus non ex id dui aliquet ornare. Curabitur efficitur nisi nisl, vel rhoncus enim fringilla non. Cras vulputate lorem non ligula condimentum, eget iaculis risus luctus. Curabitur a dolor eleifend, accumsan lectus sit amet, tempus ante.
                </Paragraph>

                <TinyText>
                    <strong>TinyText.</strong> Vestibulum luctus bibendum lorem non egestas. Ut hendrerit semper varius. Ut vitae mauris nisi. Nunc non erat ante. Nam eget lacus lobortis, tempor tortor non, laoreet est. In convallis ipsum non ante viverra accumsan. Pellentesque mattis gravida fringilla. Sed facilisis diam id nulla viverra, eu molestie sapien dignissim. Proin pulvinar felis a felis bibendum pellentesque. Pellentesque ultricies ipsum efficitur augue vehicula sagittis. Suspendisse sit amet auctor purus, ac ultrices urna. Vestibulum eu auctor sapien. Donec posuere pulvinar leo, eget semper ligula hendrerit a. Pellentesque eget mi tincidunt, sodales lorem at, pulvinar sapien. 
                </TinyText>

                <Title id="buttons">Buttons</Title>

                <Button>Button</Button> <br />

                <ButtonPrimary>ButtonPrimary</ButtonPrimary>
                <ButtonPrimary disabled>ButtonPrimary disabled</ButtonPrimary> <br />

                <ButtonWarning>ButtonWarning</ButtonWarning>
                <ButtonWarning disabled>ButtonWarning disabled</ButtonWarning><br />

                <ButtonDanger>ButtonDanger</ButtonDanger>
                <ButtonDanger disabled>ButtonDanger</ButtonDanger>

                <Button block> ButtonPrimary block </Button>

                <Title id="grid-system">Grid System</Title>
                <Container>
                    <Row>
                        <Col>1 Col</Col>
                    </Row>
                    <Row>
                        <Col>1 Col</Col>
                        <Col>2 Col</Col>
                    </Row>
                    <Row>
                        <Col>1 Col</Col>
                        <Col>2 Col</Col>
                        <Col>3 Col</Col>
                    </Row>
                    <Row>
                        <Col>1 Col</Col>
                        <Col>2 Col</Col>
                        <Col>3 Col</Col>
                        <Col>4 Col</Col>
                    </Row>
                    <Row>
                        <Col>1 Col</Col>
                        <Col>2 Col</Col>
                        <Col>3 Col</Col>
                        <Col>4 Col</Col>
                        <Col>5 Col</Col>
                    </Row>
                    <Row>
                        <Col>1 Col</Col>
                        <Col>2 Col</Col>
                        <Col>3 Col</Col>
                        <Col>4 Col</Col>
                        <Col>5 Col</Col>
                        <Col>6 Col</Col>
                    </Row>
                </Container>

                <Title id="loader">Loader</Title>
                
                <Loader loading={this.state.loading} />

                <Button block onClick={this.simulateLoading}>Click here to load</Button>

                <Title id="forms">Forms</Title>

                <FieldContainer>
                    <Label htmlFor="textField">Label TextField</Label>
                    <TextField id="textField" placeholder="TextField" type="text"/> 
                </FieldContainer>
                
                <FieldContainer>
                    <Label htmlFor="textFieldInvalid">Label TextField Invalid</Label>
                    <TextField id="textFieldInvalid" placeholder="TextField Invalid" type="text" invalid />
                </FieldContainer>

                 <FieldContainer>
                    <Label htmlFor="select">Label Select</Label>
                    <Select id="select">
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </Select>
                </FieldContainer>

                <FieldContainer>
                    <Label htmlFor="selectInvalid">Label Select Invalid</Label>
                    <Select id="selectInvalid" invalid>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </Select>
                </FieldContainer>


                   <FieldContainer>
                    <Radio label="Radio 1" id="radio1" name="radio" value="radio1"/>
                    <Radio label="Radio 2" id="radio2" name="radio" value="radio2"/>
                    <Radio label="Radio 3" id="radio3" name="radio" value="radio3"/>
                </FieldContainer>

                <FieldContainer>
                    <Checkbox label="Checkbox 1" id="checkbox1" name="checkbox1" value="checkbox1"/>
                    <Checkbox label="Checkbox 2" id="checkbox2" name="checkbox2" value="checkbox2"/>
                    <Checkbox label="Checkbox 3" id="checkbox3" name="checkbox3" value="checkbox3"/>
                </FieldContainer>

                <FieldContainer>
                    <Label htmlFor="selectBlock">Label Select Block</Label>
                    <Select id="selectBlock" block>
                        <option value="Option 1">Option 1</option>
                        <option value="Option 2">Option 2</option>
                        <option value="Option 3">Option 3</option>
                        <option value="Option 4">Option 4</option>
                    </Select>
                </FieldContainer>
                
                <FieldContainer>
                    <Label htmlFor="textFieldBlock">Label TextField Block</Label>
                    <TextField id="textFieldBlock" placeholder="TextField block" type="text" block />
                </FieldContainer>

                <FieldContainer>
                    <Label htmlFor="textArea">Label TextArea</Label>
                    <TextArea id="textArea" placeholder="TextArea" rows="10"/>
                </FieldContainer>

                <FieldContainer>
                    <Label htmlFor="textAreaInvalid">Label TextArea Invalid</Label>
                    <TextArea id="textAreaInvalid" placeholder="TextArea Invalid" invalid rows="5"/>
                </FieldContainer>

            </div>
        )
    }
}

export default StyleGuide