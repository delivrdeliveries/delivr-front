import React, { Component } from 'react'
import { Route } from 'react-router-dom'

import DeliveryRouter from './screens/Deliveries/Router'
import ShippingCompanyRouter from './screens/ShippingCompany/Router'
import SupplierCompanyRouter from './screens/SupplierCompany/Router'
import ReportsRouter from './screens/Reports/Router'
import SettingsRouter from './screens/Settings/Router'
import NotLoggedRouter from './screens/NotLogged/Router'
import Home from './screens/Home'
import StyleGuide from './screens/StyleGuide'

import styled from 'styled-components'
export const Container = styled.div`
    margin: 70px auto;
    max-width: 900px;
    padding: 0 20px;
`

class RouterContainer extends Component {
    render () {
        return (
            <Container>
                <Route exact path='/home' component={Home} />
                <Route path='/style-guide' component={StyleGuide} />

                <NotLoggedRouter />
                <DeliveryRouter />
                <ShippingCompanyRouter />
                <ReportsRouter />
                <SupplierCompanyRouter />
                <SettingsRouter />

            </Container>
        )
    }
}

export default RouterContainer