import { SET_PRODUCT_TYPES } from './constants'

const initialState = {
  productTypes: []
}

const setProductTypes = (state, action) => ({
  ...state,
  productTypes: action.payload
})

const productTypesReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_PRODUCT_TYPES:
        return setProductTypes(state, action)
      default:
        return state
  }
}

export default productTypesReducer