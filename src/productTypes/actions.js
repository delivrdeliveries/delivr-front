import {SET_PRODUCT_TYPES} from './constants'
import { getProductTypes } from './endpoints'

export const setProductTypes = productType =>({
    type: SET_PRODUCT_TYPES,
    payload: productType
})

export const loadProductTypesAsync = (onSuccess) => {
    return dispatch => {
        getProductTypes().then(productTypes => {
            dispatch(setProductTypes(productTypes))
            if (onSuccess) {
                onSuccess(productTypes)
            }
        })
    }
}