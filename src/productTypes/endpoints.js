import requests from '../requests'

export const getProductTypes = () => {
    return requests.get('/product_types').then(
        req => req.data
    )
}