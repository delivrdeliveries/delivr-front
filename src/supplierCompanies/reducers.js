import { SET_SUPPLIER_COMPANY_ADDRESSES, SET_SUPPLIER_COMPANY_ADDRESS, SET_SUPPLIER_COMPANY_DELIVERIES } from './constants'

const initialState = {
  address: {},
  supplierCompanyAddresses: [],
  deliveries: []
}

const setSupplierCompanyAddresses = (state, action) => ({
  ...state,
  supplierCompanyAddresses: action.payload
})

const setSupplierCompanyAddress = (state, action) => ({
  ...state,
  supplierCompanyAddress: action.payload
})

const setDeliveries = (state, action) => ({
  ...state,
  deliveries: action.payload
})


const supplierCompanyReducer = (state = initialState, action) => {
  switch (action.type) {
      case SET_SUPPLIER_COMPANY_ADDRESSES:
        return setSupplierCompanyAddresses(state, action)
      case SET_SUPPLIER_COMPANY_ADDRESS:
        return setSupplierCompanyAddress(state, action)
      case SET_SUPPLIER_COMPANY_DELIVERIES:
        return setDeliveries(state, action)
      default:
        return state
  }
}

export default supplierCompanyReducer