import {SET_SUPPLIER_COMPANY_ADDRESSES, SET_SUPPLIER_COMPANY_DELIVERIES, SET_SUPPLIER_COMPANY_ADDRESS} from './constants'
import { getSupplierCompanyAddresses, postSupplierCompany, putSupplierCompany, getSupplierCompanyDeliveries, postSupplierCompanyAddress, putSupplierCompanyAddress, getSupplierCompanyAddress } from './endpoints'
import { getStateLabel } from '../deliveries/strings';

export const setSupplierCompanyAddresses = addresses =>({
    type: SET_SUPPLIER_COMPANY_ADDRESSES,
    payload: addresses
})

export const setSupplierCompanyAddress = address =>({
    type: SET_SUPPLIER_COMPANY_ADDRESS,
    payload: address
})

export const setSupplierCompanyDeliveries = deliveries =>({
    type: SET_SUPPLIER_COMPANY_DELIVERIES,
    payload: deliveries
})

export const postSupplierCompanyAsync = (company, onSuccess, onError) => {
    return dispatch => {
        postSupplierCompany(company).then(onSuccess).catch(onError)
    }
}

export const putSupplierCompanyAsync = (id, company, onSuccess, onError) => {
    return dispatch => {
        putSupplierCompany(id, company).then(onSuccess).catch(onError)
    }
}

export const loadSupplierCompanyDeliveries = (companyId, onSuccess, params) => {
    params = params || {}
    const {status, locator, page} = params
    
    return dispatch => {
        getSupplierCompanyDeliveries(companyId, {status, locator, page}).then(deliveries => {

            dispatch(setSupplierCompanyDeliveries(deliveries.map(delivery => {
                delivery.statusLabel = getStateLabel(delivery.status)
                return delivery
            })))
            
            return deliveries
        }).then(onSuccess)
    }
}

export const createtSupplierCompanyAddress = (id, address, onSuccess, onError) => {
    return dispatch => {
        postSupplierCompanyAddress(id, address).then(onSuccess).catch(onError)
    }
}

export const editSupplierCompanyAddress = (supplierId, addressId, address, onSuccess, onError) => {
    return dispatch => {
        putSupplierCompanyAddress(supplierId, addressId, address).then(onSuccess).catch(onError)
    }
}

export const loadSupplierCompanyAddressesAsync = (id, onSuccess) => {
    return dispatch => {
        getSupplierCompanyAddresses(id).then(addresses => {
            dispatch(setSupplierCompanyAddresses(addresses))
            return addresses
        }).then(onSuccess)
    }
}

export const loadSupplierCompanyAddressAsync = (supplierId, addressId, onSuccess) => {
    return dispatch => {
        getSupplierCompanyAddress(supplierId, addressId).then(address => {
            dispatch(setSupplierCompanyAddress(address))
            return address
        }).then(onSuccess)
    }
}