import requests from '../requests'

export const getSupplierCompanyAddresses = (id) => {
    return requests.get(`/supplier_companies/${id}/addresses`).then(
        req => req.data
    )
}

export const getSupplierCompanyAddress = (supplierId, addressId) => {
    return requests.get(`/supplier_companies/${supplierId}/addresses/${addressId}`).then(
        req => req.data
    )
}

export const postSupplierCompanyAddress = (id, address) => {
    return requests.post(`/supplier_companies/${id}/addresses`, address).then(
        req => req.data
    )
}

export const putSupplierCompanyAddress = (supplierId, addressId, address) => {
    return requests.put(`/supplier_companies/${supplierId}/addresses/${addressId}`, address).then(
        req => req.data
    )
}

export const getSupplierCompanyDeliveries = (id, params) => {
    return requests.get(`/supplier_companies/${id}/deliveries`, params).then(
        resp => resp.data
    )
}

export const postSupplierCompany = async company => {
    const resp = await requests.post('/supplier_companies', company)
    return resp.data
}

export const putSupplierCompany = async (id, company) => {
    const resp = await requests.put('/supplier_companies/' + id, company)
    return resp.data
}